<img src="src/main/webapp/img/logo.png" alt="ResponsibleFood" />

<p>
    This Repository is student project <br/> 
    It is the end-of-semester project of the "Web technologies" course in the software engineering training given at ISEP. <br/> 
</p>

## Elevator pitch
<p style="text-align: justify"> 
    For customers who wish to limit their environmental impact by consuming more responsible food products, ResponsibleFood is a platform for bringing together farmers, associations, distributors and customers, offering short circuit food products or unsold products that are still consumable.
    Unlike grocery stores offering products imported from far away, the application promotes local products at fair prices for suppliers and customers alike, satisfies all stakeholders and encourages the non-wasteful use of unsold products.
    And unlike the Too Good to Go applications that work on a small scale, we target more players.
</p> 

## Prerequisites
* Java 14, your can download [here](https://jdk.java.net/14/)
* An IDE, in our case [IntelliJ IDEA](https://www.jetbrains.com/fr-fr/idea/)
* Apache Maven

## Installation
* In IntelliJ, go to File/Project Structure
    * In the section project, change the JDK 
    * In the section project, set project language level to at least 13
* Copy mongo.properties into src/main/resources/
* Copy mail.properties into src/main/resources/
* Copy mapbox.properties into src/main/resources/
* Install the LiveReload extension for your browser
* For IntelliJ Users : [enabling LiveReload](https://dzone.com/articles/spring-boot-application-live-reload-hot-swap-with)

## Run the application
To start the application, simply run the class :

```
src/main/java/com/llanfair/responsiblefood/ResponsibleFoodWebApplication.java
```

## Authors
* Laurent Yu → @lauyu10
* Mykola Choban -> @MykolaChoban
* Sébastien Viguier → @SebEdena
* Mathieu Valentin → @hmatico
* Pierre Verbe → @PierreVerbe 