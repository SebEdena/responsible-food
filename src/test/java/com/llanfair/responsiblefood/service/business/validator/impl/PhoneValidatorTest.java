package com.llanfair.responsiblefood.service.business.validator.impl;

import com.llanfair.responsiblefood.model.bean.dto.SignupDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.SpringConstraintValidatorFactory;
import org.springframework.web.context.WebApplicationContext;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@WebAppConfiguration
class PhoneValidatorTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private LocalValidatorFactoryBean validator;

    @BeforeEach
    public void setUp() {
        SpringConstraintValidatorFactory springConstraintValidatorFactory
                = new SpringConstraintValidatorFactory(webApplicationContext.getAutowireCapableBeanFactory());
        validator = new LocalValidatorFactoryBean();
        validator.setConstraintValidatorFactory(springConstraintValidatorFactory);
        validator.setApplicationContext(webApplicationContext);
        validator.afterPropertiesSet();
    }

    @Test
    public void testCorrectPhone() {
        SignupDTO signupDTO = new SignupDTO();
        signupDTO.setPhone("01 11 11 11 11");

        Set<ConstraintViolation<SignupDTO>> violations = validator.validateProperty(signupDTO, "phone");
        assertTrue(violations.isEmpty());
    }

    @ParameterizedTest
    @ValueSource(strings = {
        "0111111111",
        "01 11 11 11 aa"
    })
    public void testIncorrectPhones(String phone) {
        SignupDTO signupDTO = new SignupDTO();
        signupDTO.setPhone(phone);

        Set<ConstraintViolation<SignupDTO>> violations = validator.validateProperty(signupDTO, "phone");
        assertEquals(1, violations.size());
    }
}