package com.llanfair.responsiblefood.service.business.validator.impl;

import com.llanfair.responsiblefood.model.bean.dto.SignupDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.SpringConstraintValidatorFactory;
import org.springframework.web.context.WebApplicationContext;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@WebAppConfiguration
class RoleExistsValidatorTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private LocalValidatorFactoryBean validator;

    @BeforeEach
    public void setUp() {
        SpringConstraintValidatorFactory springConstraintValidatorFactory
                = new SpringConstraintValidatorFactory(webApplicationContext.getAutowireCapableBeanFactory());
        validator = new LocalValidatorFactoryBean();
        validator.setConstraintValidatorFactory(springConstraintValidatorFactory);
        validator.setApplicationContext(webApplicationContext);
        validator.afterPropertiesSet();
    }


    @Test
    public void correctRoleValidatorCase() {

        SignupDTO signupDTO = new SignupDTO();
        signupDTO.setRole("client");

        Set<ConstraintViolation<SignupDTO>> violations = validator.validateProperty(signupDTO, "role");
        assertTrue(violations.isEmpty());
    }

    @Test
    public void noRoleValidatorCase() {

        SignupDTO signupDTO = new SignupDTO();
        signupDTO.setRole("badRole");

        Set<ConstraintViolation<SignupDTO>> violations = validator.validateProperty(signupDTO, "role");
        assertEquals(violations.size(), 1);
    }
}