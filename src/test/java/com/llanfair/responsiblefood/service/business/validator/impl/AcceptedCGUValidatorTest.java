package com.llanfair.responsiblefood.service.business.validator.impl;

import com.llanfair.responsiblefood.model.bean.dto.SignupDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.SpringConstraintValidatorFactory;
import org.springframework.web.context.WebApplicationContext;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@WebAppConfiguration
class AcceptedCGUValidatorTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private LocalValidatorFactoryBean validator;

    @BeforeEach
    public void setUp() {
        SpringConstraintValidatorFactory springConstraintValidatorFactory
                = new SpringConstraintValidatorFactory(webApplicationContext.getAutowireCapableBeanFactory());
        validator = new LocalValidatorFactoryBean();
        validator.setConstraintValidatorFactory(springConstraintValidatorFactory);
        validator.setApplicationContext(webApplicationContext);
        validator.afterPropertiesSet();
    }

    @Test
    public void CGUAcceptedValidatorCase() {

        SignupDTO signupDTO = new SignupDTO();
        signupDTO.setCguAccepted(true);

        Set<ConstraintViolation<SignupDTO>> violations = validator.validateProperty(signupDTO, "cguAccepted");
        assertTrue(violations.isEmpty());
    }

    @Test
    public void CGUNotAcceptedValidatorCase() {

        SignupDTO signupDTO = new SignupDTO();
        signupDTO.setCguAccepted(false);

        Set<ConstraintViolation<SignupDTO>> violations = validator.validateProperty(signupDTO, "cguAccepted");
        assertEquals(violations.size(), 1);
    }

}