package com.llanfair.responsiblefood.service.business.validator.impl.password;

import com.llanfair.responsiblefood.model.bean.dto.SignupDTO;
import com.llanfair.responsiblefood.model.mongo.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.SpringConstraintValidatorFactory;
import org.springframework.web.context.WebApplicationContext;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@WebAppConfiguration
class PasswordsMatchValidatorTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private LocalValidatorFactoryBean validator;

    @MockBean
    private UserRepository userRepository;

    @BeforeEach
    public void setUp() {
        SpringConstraintValidatorFactory springConstraintValidatorFactory
                = new SpringConstraintValidatorFactory(webApplicationContext.getAutowireCapableBeanFactory());
        validator = new LocalValidatorFactoryBean();
        validator.setConstraintValidatorFactory(springConstraintValidatorFactory);
        validator.setApplicationContext(webApplicationContext);
        validator.afterPropertiesSet();

        when(userRepository.findByEmail(any())).thenReturn(null);
    }

    @Test
    public void testPasswordsMatch() {
        SignupDTO signupDTO = getUser();
        Set<ConstraintViolation<SignupDTO>> violations = validator.validate(signupDTO);
        assertTrue(violations.isEmpty());
    }

    @ParameterizedTest
    @CsvSource({
        "Blabla2!,Blabla1!",
        "Blabla1!,Blabla1"
    })
    public void testPasswordsDoNotMatch(String password, String confirmation) {
        SignupDTO signupDTO = getUser();
        signupDTO.setPassword(password);
        signupDTO.setPasswordConfirmation(confirmation);

        Set<ConstraintViolation<SignupDTO>> violations = validator.validate(signupDTO);
        assertFalse(violations.isEmpty());
    }

    private SignupDTO getUser() {
        SignupDTO signupDTO = new SignupDTO();
        signupDTO.setFirstName("aa");
        signupDTO.setLastName("bb");
        signupDTO.setPhone("01 11 11 11 11");
        signupDTO.setAddress("aaa");
        signupDTO.setZipCode("aaa");
        signupDTO.setCompany("corp");
        signupDTO.setRole("client");
        signupDTO.setCity("Paris");
        signupDTO.setCguAccepted(true);

        signupDTO.setEmail("aa@aa.aa");
        signupDTO.setPassword("Blabla!1");
        signupDTO.setPasswordConfirmation("Blabla!1");

        return signupDTO;
    }
}