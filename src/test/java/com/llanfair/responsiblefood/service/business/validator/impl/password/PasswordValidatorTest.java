package com.llanfair.responsiblefood.service.business.validator.impl.password;

import com.llanfair.responsiblefood.model.bean.dto.SignupDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.SpringConstraintValidatorFactory;
import org.springframework.web.context.WebApplicationContext;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@WebAppConfiguration
class PasswordValidatorTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private LocalValidatorFactoryBean validator;

    @BeforeEach
    public void setUp() {
        SpringConstraintValidatorFactory springConstraintValidatorFactory
                = new SpringConstraintValidatorFactory(webApplicationContext.getAutowireCapableBeanFactory());
        validator = new LocalValidatorFactoryBean();
        validator.setConstraintValidatorFactory(springConstraintValidatorFactory);
        validator.setApplicationContext(webApplicationContext);
        validator.afterPropertiesSet();
    }

    @Test
    public void testCorrectPassword() {
        SignupDTO signupDTO = new SignupDTO();
        signupDTO.setPassword("Blabla!1");

        Set<ConstraintViolation<SignupDTO>> violations = validator.validateProperty(signupDTO, "password");
        assertTrue(violations.isEmpty());
    }

    @ParameterizedTest
    @ValueSource(strings = {
        "Blabla!",
        "Blabla11",
        "blabla!1"
    })
    public void testIncorrectPassword(String password) {
        SignupDTO signupDTO = new SignupDTO();
        signupDTO.setPassword(password);

        Set<ConstraintViolation<SignupDTO>> violations = validator.validateProperty(signupDTO, "password");
        assertEquals(1, violations.size());
    }
}