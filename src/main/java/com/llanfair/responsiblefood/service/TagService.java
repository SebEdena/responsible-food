/**
 * This Service allows us to work with tag system
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service;

import com.llanfair.responsiblefood.model.mongo.collection.Tag;
import com.llanfair.responsiblefood.model.mongo.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagService {

    @Autowired
    private TagRepository tagRepository;

    /**
     * Used to get tags from a selected family
     * @param family
     * @return List<Tag>
     */
    public List<Tag> findTagByFamily(String family) {
        return tagRepository.findTagByFamily(family);
    }

    /**
     * Used to get all the tags stored in the database
     * @return List<Tag>
     */
    public List<Tag> findAllTags() {
        return tagRepository.findAll();
    }

    /**
     * Used to count the number of tags stored in the database
     * @return int
     */
    public int countAllTags(){ return tagRepository.countAllBy(); }

}
