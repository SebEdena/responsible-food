/**
 * This Service allows us to work with the user system
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service;

import com.llanfair.responsiblefood.model.bean.dto.PasswordChangeDTO;
import com.llanfair.responsiblefood.model.bean.dto.SignupDTO;
import com.llanfair.responsiblefood.model.bean.dto.UserDetailsDTO;
import com.llanfair.responsiblefood.model.bean.image.DefaultImageEnum;
import com.llanfair.responsiblefood.model.exception.user.UserNotFoundException;
import com.llanfair.responsiblefood.model.mongo.collection.Role;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import com.llanfair.responsiblefood.model.mongo.repository.RoleRepository;
import com.llanfair.responsiblefood.model.mongo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private ImageService imageService;

    /**
     * Used to register a new account
     * @param signupDTO
     * @return User
     */
    public User registerNewUserAccount(SignupDTO signupDTO) {
        User user = createUser(signupDTO);
        return userRepository.save(user);
    }

    /**
     * Used to create a new user
     * @param signupDTO
     * @return User
     */
    public User createUser(SignupDTO signupDTO) {
        User user = new User();
        user.setEmail(signupDTO.getEmail());
        user.setPassword(bCryptPasswordEncoder.encode(signupDTO.getPassword()));
        user.setFirstName(signupDTO.getFirstName());
        user.setLastName(signupDTO.getLastName());
        user.setPhone(signupDTO.getPhone());
        user.setCompany(signupDTO.getCompany());
        user.setAddress(signupDTO.getAddress());
        user.setZipCode(signupDTO.getZipCode());
        user.setCity(signupDTO.getCity());
        user.setEnabled(false);

        user.setImageId(null);

        Role userRole = roleRepository.findByRole(signupDTO.getRole());
        user.setRoles(Collections.singletonList(userRole));

        return user;
    }

    /**
     * Used to get all the user details of a user
     * @param email
     * @return UserDetails
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        User user = userRepository.findByEmail(email);
        if(user != null) {
            List<GrantedAuthority> authorities = getUserAuthority(user.getRoles());
            return buildUserForAuthentication(user, authorities);
        } else {
            throw new UsernameNotFoundException("username not found");
        }
    }

    /**
     * Used to get the user authority from a list of Role
     * @param userRoles
     * @return List<GrantedAuthority>
     */
    private List<GrantedAuthority> getUserAuthority(List<Role> userRoles) {
        Set<GrantedAuthority> roles = new HashSet<>();

        userRoles.forEach((role) -> roles.add(new SimpleGrantedAuthority(role.getRole())));

        return new ArrayList<>(roles);
    }

    /**
     * Used to build a User for Authentification
     * @param user
     * @param authorities
     * @return UserDetails
     */
    private UserDetails buildUserForAuthentication(User user, List<GrantedAuthority> authorities) {
        boolean enabled = user.isEnabled();
        boolean accountNotExpired = true;
        boolean credentialsNotExpired = true;
        boolean accountNotLocked = true;

        return new org.springframework.security.core.userdetails.User(
                user.getEmail(),
                user.getPassword(),
                enabled,
                accountNotExpired,
                credentialsNotExpired,
                accountNotLocked,
                authorities);
    }

    /**
     * Used to get all the Producer in the database
     * @return List<User>
     */
    public List<User> getListProducer() {
        Role producerRole = roleRepository.findByRole("farmer");
        return userRepository.findByRoles(producerRole);
    }

    /**
     * Used to change the password of a user
     * @param user
     * @param password
     * @param confirmedPassword
     * @return User
     */
    public User changePassword(User user, String password, String confirmedPassword) {
        user.setPassword(bCryptPasswordEncoder.encode(password));
        return userRepository.save(user);
    }

    /**
     * Used to get all the user stored in the database
     * @return List<User>
     */
    public List<User> getListUser(){
        return userRepository.findAll();
    }

    /**
     * Used to get a user from its id
     * @param id
     * @return User
     */
    public User getUserById(String id){
        if(userRepository.findById(id).isPresent()){
            return userRepository.findById(id).get();
        }else{
            return null;
        }
    }

    /**
     * Used to remove a user from its id
     * @param id
     */
    public void removeUser(String id){
        if(userRepository.findById(id).isPresent()) {
            userRepository.deleteById(id);
        }
    }

    /**
     * Used to build user details from an id
     * @param id
     * @return UserDetailsDTO
     */
    public UserDetailsDTO buildUserDetailsObject(String id) {
        Optional<User> userOptional = userRepository.findById(id);
        if(userOptional.isPresent()) {
            User user = userOptional.get();
            UserDetailsDTO userDetailsDTO = new UserDetailsDTO();
            userDetailsDTO.setEmail(user.getEmail());
            userDetailsDTO.setRole(user.getRoles().get(0).getRole());
            userDetailsDTO.setFirstName(user.getFirstName());
            userDetailsDTO.setLastName(user.getLastName());
            userDetailsDTO.setImageId(user.getImageId());
            userDetailsDTO.setPhone(user.getPhone());
            userDetailsDTO.setCompany(user.getCompany());
            userDetailsDTO.setAddress(user.getAddress());
            userDetailsDTO.setZipCode(user.getZipCode());
            userDetailsDTO.setCity(user.getCity());
            return userDetailsDTO;
        } else {
            return null;
        }
    }

    /**
     * Used to update the user detail of an account
     * @param email
     * @param userDetailsDTO
     * @return User
     * @throws IOException
     */
    public User updateUserDetails(String email, UserDetailsDTO userDetailsDTO) throws IOException {
        User user = userRepository.findByEmail(email);
        if(user != null) {
            user.setFirstName(userDetailsDTO.getFirstName());
            user.setLastName(userDetailsDTO.getLastName());
            user.setPhone(userDetailsDTO.getPhone());
            user.setCompany(userDetailsDTO.getCompany());
            user.setAddress(userDetailsDTO.getAddress());
            user.setZipCode(userDetailsDTO.getZipCode());
            user.setCity(userDetailsDTO.getCity());

            if(userDetailsDTO.getImage() != null && !userDetailsDTO.getImage().isEmpty()){
                String imageId = imageService.updateImage(DefaultImageEnum.PERSON, user.getId(), userDetailsDTO.getImage());
                user.setImageId(imageId);
            }

            return userRepository.save(user);

        } else {
            return null;
        }
    }

    /**
     * Used to build a password change object
     * @return PasswordChangeDTO
     */
    public PasswordChangeDTO buildPasswordChangeObject() {
        PasswordChangeDTO passwordChangeDTO = new PasswordChangeDTO();
        passwordChangeDTO.setCurrentPassword("");
        passwordChangeDTO.setNewPassword("");
        passwordChangeDTO.setNewPasswordConfirmation("");

        return passwordChangeDTO;
    }

    /**
     * Used to check id the password is correct
     * @param email
     * @param passwordChangeDTO
     * @return boolean
     * @throws UserNotFoundException
     */
    public boolean checkCorrectPassword(String email, PasswordChangeDTO passwordChangeDTO) throws UserNotFoundException {
        User user = userRepository.findByEmail(email);
        if(user != null) {
            return bCryptPasswordEncoder.matches(passwordChangeDTO.getCurrentPassword(), user.getPassword());
        } else {
            throw new UserNotFoundException("Cet utilisateur n'existe pas.");
        }
    }

    /**
     * Used to update a password from an account
     * @param email
     * @param passwordChangeDTO
     * @return User
     * @throws UserNotFoundException
     */
    public User updatePassword(String email, PasswordChangeDTO passwordChangeDTO)
            throws UserNotFoundException {

        User user = userRepository.findByEmail(email);
        if(user == null) {
            throw new UserNotFoundException("Cet utilisateur n'existe pas.");
        }
        user.setPassword(bCryptPasswordEncoder.encode(passwordChangeDTO.getNewPassword()));
        return userRepository.save(user);
    }

    /**
     * Used to get producers from a research
     * @param research
     * @return List<User>
     */
    public List<User> getProducersFromResearch(String research){
        List<User> list = userRepository.findUsers(research);
        list.removeIf(farmer -> !farmer.getRoles().get(0).getRole().equals("farmer"));
        return list;
    }

    /**
     * Used to count the number of account for each role
     * @return Map<String, Integer>
     */
    public  Map<String, Integer> countAllUser(){
        List<Role> listRole = roleRepository.findAll();
        Map<String, Integer> roleMap = new HashMap<>();

        for (Role role : listRole){
            int nbRole = userRepository.countAllByRoles(role);
            roleMap.put(role.getRole(), nbRole);
        }

        return roleMap;
    }
}
