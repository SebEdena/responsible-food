/**
 * This Service allows us to work with the transaction system
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service;

import com.llanfair.responsiblefood.model.mongo.collection.Offer;
import com.llanfair.responsiblefood.model.mongo.collection.Transaction;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import com.llanfair.responsiblefood.model.mongo.repository.OfferRepository;
import com.llanfair.responsiblefood.model.mongo.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private OfferRepository offerRepository;

    @Autowired
    private UserService userService;

    /**
     * Used to add a transaction for an offer by a user with a quantity in the database
     * @param offer
     * @param user
     * @param quantity
     */
    public void addTransaction(Offer offer, User user, int quantity) {
        offer.setAvailable(offer.getAvailable() - quantity);
        offerRepository.save(offer);
        Transaction transaction = new Transaction();
        transaction.setValidated(false);
        transaction.setOffer(offer);
        transaction.setBuyer(user);
        transaction.setQuantity(quantity);
        transaction.setDate(new Date());
        transactionRepository.save(transaction);
    }

    /**
     * Used to get all the transactions from a user in the database
     * @param user
     * @return List<Transaction>
     */
    public List<Transaction> getTransactionByBuyer(User user){
        return transactionRepository.findAllByBuyer(user);
    }

    /**
     * Used to remove a transaction in the database
     * @param transaction
     */
    public void removeTransaction(Transaction transaction){
        Transaction transactionCheck = transactionRepository.findTransactionById(transaction.getId());

        if (transactionCheck != null){
            transactionRepository.deleteById(transactionCheck.getId());
        }
    }

    /**
     * Used to get all reserved transaction from a producer
     * @param producer
     * @return List<Transaction>
     */
    public List<Transaction> getReservedTransaction(User producer){
        List<Offer> listOffer = offerRepository.findAllByAuthor(producer);
        List<Transaction> listTransactions = new ArrayList<>();
        for(Offer offer : listOffer){
            List<Transaction> test = transactionRepository.findAllByOffer(offer);
            if (!test.isEmpty()){
                listTransactions.addAll(test);
            }
        }
        return listTransactions;
    }

    /**
     * Used to cancel a transaction with its id and updates the number of available offer
     * @param transactionId
     * @param producer
     */
    public void cancelTransaction(String transactionId,User producer) {
        Transaction transaction = transactionRepository.findTransactionById(transactionId);
        Offer offer = transaction.getOffer();
         if (offer.getAuthor().getId().equals(producer.getId())){
            offer.setAvailable(offer.getAvailable()+transaction.getQuantity());
            offerRepository.save(offer);
            transactionRepository.delete(transaction);
        }
    }

    /**
     * Used to validate a transaction
     * @param transactionId
     * @param producer
     */
    public void validateTransaction(String transactionId,User producer) {
        Transaction transaction = transactionRepository.findTransactionById(transactionId);
        Offer offer = transaction.getOffer();
        if (offer.getAuthor().getId().equals(producer.getId())) {
            transaction.setValidated(true);
            transactionRepository.save(transaction);
        }
    }

    /**
     * Used to delete all transactions of an offer
     * @param offer
     */
    public void deleteAllTransactionByOffer(Offer offer){
        transactionRepository.deleteAllByOffer(offer);

    }
}
