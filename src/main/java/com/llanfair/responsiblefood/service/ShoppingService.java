/**
 * This Service allows us to work on the buying system
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service;

import com.llanfair.responsiblefood.model.bean.basket.BasketItem;
import com.llanfair.responsiblefood.model.exception.basket.ExpiredOfferException;
import com.llanfair.responsiblefood.model.exception.basket.NoOfferException;
import com.llanfair.responsiblefood.model.exception.basket.NotAvailableException;
import com.llanfair.responsiblefood.model.exception.basket.NothingInBasket;
import com.llanfair.responsiblefood.model.mongo.collection.Offer;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import com.llanfair.responsiblefood.model.mongo.repository.OfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ShoppingService {

    @Autowired
    private OfferRepository offerRepository;

    @Autowired
    private TransactionService transactionService;

    /**
     * This function allows us to put an item on the basket
     * @param id is the id of the offer
     * @param quantity is the quantity asked by the user
     * @param request instance that call the function
     * @return the update list of items in the basket
     * @throws NoOfferException the offer asked doesn't exist
     * @throws NotAvailableException the offer is no longer available
     * @throws ExpiredOfferException the offer has expired
     */
    public List<BasketItem> putOfferInBasket(String id, int quantity, HttpServletRequest request) throws NoOfferException, NotAvailableException, ExpiredOfferException {
        List<BasketItem> basket = !checkIfBasket(request) ? new ArrayList<>():
                (List<BasketItem>) request.getSession().getAttribute("basket");
        Offer offer = offerRepository.findOfferById(id);
        if(offer == null){
            throw new NoOfferException(id);
        }else{
            if(checkNotExpiredOffer(offer)){
                for(BasketItem basketItem : basket){
                    if(basketItem.getOffer().getId().equals(offer.getId())){
                        if(checkAvailableOffer(offer,basketItem.getQuantity() + quantity)){
                            basketItem.setQuantity(basketItem.getQuantity() + quantity);
                            return basket;
                        }else{
                            throw new NotAvailableException("Vous ne pouvez pas commander plus de "
                                    + offer.getAvailable()
                                    + " unités pour l'offre " + offer.getTitle());
                        }
                    }
                }
            }else{
                throw new ExpiredOfferException(offer.getTitle());
            }
            if(checkAvailableOffer(offer, quantity)){
                basket.add(new BasketItem(offer, quantity));
                return basket;
            }else{
                throw new NotAvailableException("Vous ne pouvez pas commander plus de "
                        + offer.getAvailable()
                        + " unités pour l'offre " + offer.getTitle());
            }
        }
    }

    public List<BasketItem> deleteOffer(String id, HttpServletRequest request) throws NothingInBasket {
        if(checkIfBasket(request)){
            List<BasketItem> basket = (List<BasketItem>) request.getSession().getAttribute("basket");
            for(BasketItem basketItem : basket){
                if(basketItem.getOffer().getId().equals(id)){
                    basket.remove(basketItem);
                    return basket;
                }
            }
        }
        throw new NothingInBasket();
    }

    /**
     * This funciton allows us to validate the basket
     * @param request instance that call the function
     * @param user is the user who validate the basket
     * @throws NothingInBasket there is nothing in the basket
     * @throws NotAvailableException one offer is no longer available
     * @throws ExpiredOfferException one offer has expired
     */
    public void validateBasket(HttpServletRequest request, User user) throws NothingInBasket, ExpiredOfferException, NotAvailableException {
        if(checkIfBasket(request)){
            List<BasketItem> basket = (List<BasketItem>) request.getSession().getAttribute("basket");
            for(BasketItem basketItem : basket){
                if(!checkNotExpiredOffer(basketItem.getOffer())){
                    throw new ExpiredOfferException(basketItem.getOffer().getTitle());
                }else{
                    if(!checkAvailableOffer(basketItem.getOffer(), basketItem.getQuantity())){
                        throw new NotAvailableException(
                                "Vous ne pouvez pas commander plus de "
                                + basketItem.getOffer().getAvailable()
                                + " unités pour l'offre " + basketItem.getOffer().getTitle());
                    }
                    transactionService.addTransaction(basketItem.getOffer(), user, basketItem.getQuantity());
                }
            }
        }else{
            throw new NothingInBasket();
        }
    }

    /**
     * This function allows us to verify if there is something in the basket
     * @param request instance that call the function
     * @return the state of the basket
     */
    private boolean checkIfBasket(HttpServletRequest request){
        return request.getSession().getAttribute("basket") != null;
    }

    /**
     * This function allows us to verify if there is no offer expired
     * @param offer is the tested offer
     * @return the state of the offer about the expiration date
     */
    private boolean checkNotExpiredOffer(Offer offer){
        return offer.getExpirationDate().after(new Date());
    }

    /**
     * This function allows us to verifiy if the offer is available or not
     * @param offer is the tested offer
     * @param quantity is the quantity asked by the user
     * @return the state of the offer
     */
    private boolean checkAvailableOffer(Offer offer, int quantity){
        return offer.getAvailable() >= quantity;
    }

    /**
     * This function allows us to clear the basket
     * @param request instance that call the function
     * @return an empty list in the basket
     * @throws NothingInBasket if there is nothing in the basket
     */
    public List<BasketItem> deleteAllOffers(HttpServletRequest request) throws NothingInBasket {
        if(checkIfBasket(request)){
            List<BasketItem> basket = (List<BasketItem>) request.getSession().getAttribute("basket");
            basket.clear();
            return basket;
        }else{
            throw new NothingInBasket();
        }
    }
}
