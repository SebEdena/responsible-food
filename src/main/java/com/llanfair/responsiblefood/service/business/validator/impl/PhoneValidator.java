/**
 * This Validator is for the Phone validator
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service.business.validator.impl;

import com.llanfair.responsiblefood.service.business.validator.annotation.ValidPhone;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class PhoneValidator implements ConstraintValidator<ValidPhone, String> {

    private Pattern pattern;

    private static final String PHONE_REGEX = "^0\\d( [\\d]{2}){4}$";

    /**
     * This function allows us to initialise the pattern for the phone validator
     * @param constraintAnnotation is the constraint
     */
    @Override
    public void initialize(ValidPhone constraintAnnotation) {
        pattern = Pattern.compile(PHONE_REGEX);
    }

    /**
     * This function allows us to check if the phone is valid
     * @param phone is the phone to check
     * @param constraintValidatorContext is the constraint
     * @return the status of the phone
     */
    @Override
    public boolean isValid(String phone, ConstraintValidatorContext constraintValidatorContext) {
        return pattern.matcher(phone).matches();
    }

}
