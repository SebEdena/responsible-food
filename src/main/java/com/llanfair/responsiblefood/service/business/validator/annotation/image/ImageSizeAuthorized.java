/**
 * This interface is for the Image Size Validator
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service.business.validator.annotation.image;

import com.llanfair.responsiblefood.service.business.validator.impl.image.ImageSizeAuthorizedValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = ImageSizeAuthorizedValidator.class)
@Documented
public @interface ImageSizeAuthorized {
    String message() default "La taille de l'image ne respecte pas la limite fixée.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}