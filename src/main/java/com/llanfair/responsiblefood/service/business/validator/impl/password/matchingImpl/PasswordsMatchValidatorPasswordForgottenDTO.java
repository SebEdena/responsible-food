/**
 * This Validator is for the Password validator for password forgotten
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service.business.validator.impl.password.matchingImpl;

import com.llanfair.responsiblefood.model.bean.dto.PasswordForgottenDTO;
import com.llanfair.responsiblefood.service.business.validator.impl.password.PasswordsMatchValidator;

import javax.validation.ConstraintValidatorContext;

public class PasswordsMatchValidatorPasswordForgottenDTO extends PasswordsMatchValidator<PasswordForgottenDTO> {

    /**
     * This function allows us to check if the password is ok or not
     * @param passwordForgottenDTO is the object that contains information of the password
     * @param context is the validator
     * @return the status of the password
     */
    @Override
    public boolean isValid(PasswordForgottenDTO passwordForgottenDTO, ConstraintValidatorContext context){
        return passwordForgottenDTO.getPassword().equals(passwordForgottenDTO.getPasswordConfirmation());
    }
}