/**
 * This Validator is for the Password validator for signup
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service.business.validator.impl.password.matchingImpl;

import com.llanfair.responsiblefood.model.bean.dto.SignupDTO;
import com.llanfair.responsiblefood.service.business.validator.impl.password.PasswordsMatchValidator;

import javax.validation.ConstraintValidatorContext;

public class PasswordsMatchValidatorUserDTO extends PasswordsMatchValidator<SignupDTO> {

    /**
     * This function allows us to check if the password is ok or not
     * @param signupDTO is the object that contains information of the password
     * @param context is the validator
     * @return the status of the password
     */
    @Override
    public boolean isValid(SignupDTO signupDTO, ConstraintValidatorContext context){
        return signupDTO.getPassword().equals(signupDTO.getPasswordConfirmation());
    }
}