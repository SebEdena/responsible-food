/**
 * This Validator is for the Email in Database
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */
package com.llanfair.responsiblefood.service.business.validator.impl.email;

import com.llanfair.responsiblefood.model.mongo.repository.UserRepository;
import com.llanfair.responsiblefood.service.business.validator.annotation.email.EmailInDb;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EmailInDbValidator implements ConstraintValidator<EmailInDb, String> {

    @Autowired
    private UserRepository userRepository;

    /**
     * This function allows us to check of the email exists in the database
     * @param email is the email to check
     * @param constraintValidatorContext is the validator
     * @return the status of the email
     */
    @Override
    public boolean isValid(String email, ConstraintValidatorContext constraintValidatorContext) {
        return userRepository.findByEmail(email) != null;
    }
}
