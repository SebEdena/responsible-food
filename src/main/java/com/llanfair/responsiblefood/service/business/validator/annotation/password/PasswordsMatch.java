/**
 * This interface is for the Password Matches Validator
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */
package com.llanfair.responsiblefood.service.business.validator.annotation.password;

import com.llanfair.responsiblefood.service.business.validator.impl.password.matchingImpl.PasswordsMatchValidatorPasswordChangeDTO;
import com.llanfair.responsiblefood.service.business.validator.impl.password.matchingImpl.PasswordsMatchValidatorPasswordForgottenDTO;
import com.llanfair.responsiblefood.service.business.validator.impl.password.matchingImpl.PasswordsMatchValidatorUserDTO;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy =
        {
        PasswordsMatchValidatorUserDTO.class,
        PasswordsMatchValidatorPasswordForgottenDTO.class,
        PasswordsMatchValidatorPasswordChangeDTO.class
        })
@Documented
public @interface PasswordsMatch {
    String message() default "Les mots de passe ne correspondent pas.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
