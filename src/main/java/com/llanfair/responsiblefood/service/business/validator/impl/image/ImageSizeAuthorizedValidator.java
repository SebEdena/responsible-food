/**
 * This Validator is for the Image Size Validator
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service.business.validator.impl.image;

import com.llanfair.responsiblefood.service.business.validator.annotation.image.ImageSizeAuthorized;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.io.IOException;
import java.util.Properties;

public class ImageSizeAuthorizedValidator implements ConstraintValidator<ImageSizeAuthorized, MultipartFile> {

    private Integer MAX_IMAGE_SIZE = null;

    /**
     * This function allows us to initialize the size
     * @param constraintAnnotation is the constraint
     */
    @Override
    public void initialize(ImageSizeAuthorized constraintAnnotation) {
        Properties properties = new Properties();
        try {
            properties.load(this.getClass().getClassLoader().getResourceAsStream("application.properties"));
            MAX_IMAGE_SIZE = Integer.parseInt(properties.get("responsiblefood.max-image-size").toString());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * This function allows us to check if the image is valid or not
     * @param multipartFile is the image file
     * @param constraintValidatorContext is the validator
     * @return the status of the email
     */
    @Override
    public boolean isValid(MultipartFile multipartFile, ConstraintValidatorContext constraintValidatorContext) {
        if(multipartFile != null && !multipartFile.isEmpty() ) {
            return MAX_IMAGE_SIZE != null && multipartFile.getSize() < MAX_IMAGE_SIZE;
        } else {
            return true;
        }
    }

}
