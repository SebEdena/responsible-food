/**
 * This Validator is for the Password validator
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service.business.validator.impl.password;

import com.llanfair.responsiblefood.service.business.validator.annotation.password.ValidPassword;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class PasswordValidator implements ConstraintValidator<ValidPassword, String> {

    private Pattern atLeastOneCapsLetter;
    private Pattern atLeastOneSpecialChar;

    private static final int PASSWORD_MIN_LENGTH = 8;
    private static final String CAPS_REGEX = "^.*[A-Z]+.*$";
    private static final String SPECIAL_CHAR_REGEX = "^.*\\W+.*$";

    /**
     * This function allows us to initialize the password validator
     * @param constraint isthe constraint for the password
     */
    @Override
    public void initialize(ValidPassword constraint) {
        atLeastOneCapsLetter = Pattern.compile(CAPS_REGEX);
        atLeastOneSpecialChar = Pattern.compile(SPECIAL_CHAR_REGEX);
    }

    /**
     * This function allows us to cehck if the password respects the constraint
     * @param password is the password to check
     * @param context is the constraint
     * @return the status of the password
     */
    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        return password.length() >= PASSWORD_MIN_LENGTH &&
                atLeastOneCapsLetter.matcher(password).matches() &&
                atLeastOneSpecialChar.matcher(password).matches();
    }
}
