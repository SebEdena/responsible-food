/**
 * This interface is for the Image format Validator
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service.business.validator.annotation.image;

import com.llanfair.responsiblefood.service.business.validator.impl.image.ImageFormatAuthorizedValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = ImageFormatAuthorizedValidator.class)
@Documented
public @interface ImageFormatAuthorized {
    String message() default "Le format de l'image n'est pas supporté.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}