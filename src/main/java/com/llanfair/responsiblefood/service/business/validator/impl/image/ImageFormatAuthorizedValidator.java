/**
 * This Validator is for the Image Format Validator
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service.business.validator.impl.image;

import com.llanfair.responsiblefood.service.business.validator.annotation.image.ImageFormatAuthorized;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class ImageFormatAuthorizedValidator implements ConstraintValidator<ImageFormatAuthorized, MultipartFile> {

    /**
     * This function allows us to check if the image is in the good format
     * @param multipartFile is the image
     * @param constraintValidatorContext is the validator
     * @return the status of the email
     */
    @Override
    public boolean isValid(MultipartFile multipartFile, ConstraintValidatorContext constraintValidatorContext) {
        if(multipartFile != null && !multipartFile.isEmpty()) {
            return List.of(MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE).contains(multipartFile.getContentType());
        } else {
            return true;
        }
    }

}
