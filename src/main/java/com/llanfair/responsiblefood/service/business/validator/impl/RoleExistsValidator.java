/**
 * This Validator is for the Role validator
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */
package com.llanfair.responsiblefood.service.business.validator.impl;

import com.llanfair.responsiblefood.model.mongo.repository.RoleRepository;
import com.llanfair.responsiblefood.service.business.validator.annotation.RoleExists;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RoleExistsValidator implements ConstraintValidator<RoleExists, String> {

    @Autowired
    private RoleRepository roleRepository;

    /**
     * This function allows us to check if the role exists
     * @param role is th role to check
     * @param constraintValidatorContext is the constraint
     * @return the status of the role
     */
    @Override
    public boolean isValid(String role, ConstraintValidatorContext constraintValidatorContext) {
        return roleRepository.findByRole(role) != null;
    }
}
