/**
 * This abstract class is for the password Match Vaidator
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */
package com.llanfair.responsiblefood.service.business.validator.impl.password;

import com.llanfair.responsiblefood.service.business.validator.annotation.password.PasswordsMatch;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public abstract class PasswordsMatchValidator<T> implements ConstraintValidator<PasswordsMatch, T> {

    @Override
    public abstract boolean isValid(T object, ConstraintValidatorContext context);

}
