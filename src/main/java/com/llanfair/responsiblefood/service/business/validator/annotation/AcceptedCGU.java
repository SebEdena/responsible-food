/**
 * This interface is for the Accepted GCU Validator
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service.business.validator.annotation;

import com.llanfair.responsiblefood.service.business.validator.impl.AcceptedCGUValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = AcceptedCGUValidator.class)
@Documented
public @interface AcceptedCGU {
    String message() default "Vous n'avez pas accepté les CGU.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
