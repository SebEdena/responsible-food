/**
 * This Validator is for the Email
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */
package com.llanfair.responsiblefood.service.business.validator.impl.email;

import com.llanfair.responsiblefood.service.business.validator.annotation.email.ValidEmail;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class EmailValidator implements ConstraintValidator<ValidEmail, String> {

    private Pattern pattern;

    private static final String EMAIL_REGEX =
            "^[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+" +
            "(.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*" +
            "@" +
            "[A-Za-z0-9-]+" +
            "(.[A-Za-z0-9]+)*" +
            "(.[A-Za-z]{2,})$";

    @Override
    public void initialize(ValidEmail constraintAnnotation) {
        pattern = Pattern.compile(EMAIL_REGEX);
    }

    /**
     * This function allows us to check if the email matches the pattern
     * @param email is the email to check
     * @param constraintValidatorContext is the validator
     * @return the status of the email
     */
    @Override
    public boolean isValid(String email, ConstraintValidatorContext constraintValidatorContext) {
        return pattern.matcher(email).matches();
    }
}
