package com.llanfair.responsiblefood.service.business.listener;

import com.llanfair.responsiblefood.model.event.OnRegistrationCompleteEvent;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import com.llanfair.responsiblefood.service.MailService;
import com.llanfair.responsiblefood.service.UserService;
import com.llanfair.responsiblefood.service.credentials.AccountActivationService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    @Autowired
    private UserService userService;

    @Autowired
    private MailService mailService;

    @Autowired
    private AccountActivationService accountActivationService;

    /**
     *
     * @param event
     */
    @SneakyThrows
    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    /**
     * Function that send a mail with the token for the confirmation of sign up
     * @param event is the event that comes from
     * @throws Exception if the mail is not sent
     */
    private void confirmRegistration(OnRegistrationCompleteEvent event) throws Exception {
        User user = event.getUser();
        String appUrl = event.getAppUrl();
        accountActivationService.sendAccountActivationToken(user, appUrl);
    }
}