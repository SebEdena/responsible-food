/**
 * This interface is for the Email in database Validator
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service.business.validator.annotation.email;

import com.llanfair.responsiblefood.service.business.validator.impl.email.EmailInDbValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = EmailInDbValidator.class)
@Documented
public @interface EmailInDb {
    String message() default "Aucun compte n'existe pour cet email.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
