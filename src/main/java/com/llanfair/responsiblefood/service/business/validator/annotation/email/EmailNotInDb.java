/**
 * This interface is for the Email not in database Validator
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service.business.validator.annotation.email;

import com.llanfair.responsiblefood.service.business.validator.impl.email.EmailNotInDbValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = EmailNotInDbValidator.class)
@Documented
public @interface EmailNotInDb {
    String message() default "Un compte existe déjà pour cet email.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
