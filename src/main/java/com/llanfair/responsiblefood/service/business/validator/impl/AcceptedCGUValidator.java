/**
 * This Validator is for the Accepted GCU
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service.business.validator.impl;

import com.llanfair.responsiblefood.service.business.validator.annotation.AcceptedCGU;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AcceptedCGUValidator implements ConstraintValidator<AcceptedCGU, Boolean> {

    /**
     * This function allows us to check if the user sees the GCU
     * @param cguAccepted boolean of the status of the GCU
     * @param constraintValidatorContext is the constraint
     * @return the status of the GCU
     */
    @Override
    public boolean isValid(Boolean cguAccepted, ConstraintValidatorContext constraintValidatorContext) {
        return cguAccepted;
    }

}
