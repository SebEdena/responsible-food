package com.llanfair.responsiblefood.service;

import com.llanfair.responsiblefood.model.bean.dto.OfferDTO;
import com.llanfair.responsiblefood.model.bean.image.DefaultImageEnum;
import com.llanfair.responsiblefood.model.mongo.collection.Image;
import com.llanfair.responsiblefood.model.mongo.collection.Offer;
import com.llanfair.responsiblefood.model.mongo.collection.Tag;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import com.llanfair.responsiblefood.model.mongo.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class OfferService {

    @Autowired
    private OfferRepository offerRepository;

    @Autowired
    private AddressService addressService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private UnitRepository unitRepository;

    @Autowired
    private TagService tagService;

    @Autowired
    private CurrencyRepository currencyRepository;

    /**
     * save in the database a new object offer using an offerDTO and the author
     * @param offerDTO
     * @param author
     * @throws ParseException
     * @throws IOException
     */
    public void registerNewOffer(OfferDTO offerDTO, User author) throws ParseException, IOException {
        Image image = null;
        if(offerDTO.getImage() != null && !offerDTO.getImage().isEmpty()) {
            image = imageService.addImage(offerDTO.getImage());
        }
        Offer offer = createOffer(offerDTO, author, image);
        offerRepository.save(offer);
    }

    /**
     * create an object Offer using OfferDTO to get almost all fields
     * ,the author and the image linked to the offer
     * @param offerDTO
     * @param user
     * @param image
     * @return
     * @throws ParseException
     */
    public Offer createOffer(OfferDTO offerDTO, User user, Image image) throws ParseException {
        Offer offer = new Offer();
        offer.setTimeStamp(new Date());
        offer.setAuthor(user);
        offer.setTitle(offerDTO.getTitle());
        offer.setImageId(image == null ? null : image.getId());
        offer.setDescription(offerDTO.getDescription());
        offer.setExpirationDate(new SimpleDateFormat("yyyy-MM-dd").parse(offerDTO.getExpirationDate()));
        offer.setTotal(offerDTO.getTotal());
        offer.setAvailable(offerDTO.getTotal());
        offer.setPricePerUnit(offerDTO.getPricePerUnit());
        offer.setQuantityPerUnit(offerDTO.getQuantityPerUnit());
        offer.setUnit(unitRepository.findBySymbol(offerDTO.getUnit()));
        offer.setCurrency(currencyRepository.findByName("euro"));
        offer.setTags(offerDTO.getTags());
        //offer.setBuyers();
        //offer.setMessages();
        offer.setAddress(offerDTO.getAddress());
        offer.setGeoxy(addressService.addressToPoint(offerDTO.getAddress()));//offer.setGeoxy(new Point(new Position(2.2511616,48.8865792)));//trouver une solution

        return offer;
    }

    /**
     * use offerRepository function in order to find all offers linked to a producer
     * @param producer
     * @return
     */
    public List<Offer> getProducerOffers(User producer){
        return offerRepository.findAllByAuthor(producer);
    }

    /**
     * use offerRepository function in order to get all offers from a producer sorted by the expiration date
     * @param producer
     * @return
     */
    public List<Offer> getProducerOffersForClient(User producer){

        return offerRepository.
                findAllByAuthorAndExpirationDateGreaterThanEqualAndAvailableGreaterThan(
                        producer, new Date(),0,
                        Sort.by(Sort.Direction.ASC, "expirationDate"));
    }

    /**
     *get all offers sorted by the date of creation from older to younger
     * @return
     */
    public List<Offer> getAllOffers(){
        List<Offer> offerList = offerRepository.findAll();
        // Sorting by date ASC
        offerList.sort((Offer a, Offer b) -> (int) (b.getTimeStamp().getTime()-a.getTimeStamp().getTime()));
        return offerList;
    }

    /**
     * user offerRepository function in order to get one order from its id
     * @param id
     * @return
     */
    public Offer getOfferById(String id){
        return offerRepository.findOfferById(id);
    }

    public List<Offer> getOffersByFamily(String family) {
        List<Tag> tags = tagService.findTagByFamily(family);
        List<String> tagsNames = new ArrayList<>();
        for (Tag tag : tags) {
            tagsNames.add(tag.getTag());
        }
        tagsNames.add(family);
        return offerRepository.
                findAllByTagsIsInAndExpirationDateGreaterThanEqualAndAvailableGreaterThan(
                        tagsNames, new Date(),0,
                        Sort.by(Sort.Direction.ASC, "expirationDate"));
    }

    /**
     * remove an offer from the data base using the offer id to find it and the user
     * in order to verify that the action is done by the good person
     * @param offerId
     * @param offerOwner
     */
    public void removeOffer(String offerId, User offerOwner){
        Offer offer = offerRepository.findOfferById(offerId);
        if (offer != null && offerOwner.getId().equals(offer.getAuthor().getId())){
            if(offer.getImageId() != null) {
                imageService.deleteImage(offer.getImageId());
            }

            offerRepository.deleteById(offerId);
        }
    }

    /**
     * used by the admin to remove an offer from the data base
     * @param offerId
     */
    public void removeOffer(String offerId){
        Offer offer = offerRepository.findOfferById(offerId);

        if (offer != null){
            if(offer.getImageId() != null) {
                imageService.deleteImage(offer.getImageId());
            }

            offerRepository.deleteById(offerId);
        }
    }

    /**
     * using the offerDTO instance to retrieve fields
     * to change in the offer retrieved from the data base using its id
     * Before we save it in the databse
     * we verify its creator using the author instance
     * @param offerDTO
     * @param offerId
     * @param author
     * @throws ParseException
     * @throws IOException
     */
    public void updateOffer(OfferDTO offerDTO,String offerId, User author) throws ParseException, IOException {
        Offer offer = offerRepository.findOfferById(offerId);
        offer.setTimeStamp(new Date());
        offer.setTitle(offerDTO.getTitle());
        //offer.setImage(offerDTO.getImage());
        offer.setDescription(offerDTO.getDescription());
        offer.setExpirationDate(new SimpleDateFormat("yyyy-MM-dd").parse(offerDTO.getExpirationDate()));
        offer.setTotal(offerDTO.getTotal());
        offer.setAvailable(offerDTO.getTotal());
        offer.setPricePerUnit(offerDTO.getPricePerUnit());
        offer.setQuantityPerUnit(offerDTO.getQuantityPerUnit());
        offer.setUnit(unitRepository.findBySymbol(offerDTO.getUnit()));
        //offer.setCurrency();
        offer.setTags(offerDTO.getTags());
        //offer.setMessages();
        offer.setAddress(offerDTO.getAddress());
        offer.setGeoxy(addressService.addressToPoint(offerDTO.getAddress()));
        //offer.setGeoxy(new Point(new Position(2.2511616,48.8865792)));//trouver une solution

        if (offer.getAuthor().getId().equals(author.getId())){
            offerRepository.save(offer);

            if(offerDTO.getImage() != null && !offerDTO.getImage().isEmpty()) {
                imageService.updateImage(DefaultImageEnum.OFFER, offer.getId(), offerDTO.getImage());
            }
        }
    }

    public List<Offer> getOffersByExpirationDate(Integer limit){
        List<Offer> offerList;

        if(limit == null) {
            offerList = offerRepository.
                    findOffersByExpirationDateGreaterThanEqualAndAvailableIsGreaterThan(
                            new Date(),0,
                            Sort.by(Sort.Direction.ASC, "expirationDate"));
        } else {
            offerList = offerRepository.findLimitedNumberMostRecentOffersWithMinStock(
                    new Date(),0,
                    PageRequest.of(0, limit, Sort.by(Sort.Direction.ASC, "expirationDate")));
        }

        return offerList;
    }

    public List<Offer> getOffersByTags(List<String> tagsNames){

        return offerRepository.
                findAllByTagsIsInAndExpirationDateGreaterThanEqualAndAvailableGreaterThan(
                        tagsNames, new Date(),0,
                        Sort.by(Sort.Direction.ASC, "expirationDate"));
    }

    public List<Offer> getOffersByTitleOrDescription(String research){

        return offerRepository.
                findAllByExpirationDateGreaterThanEqualAndAvailableIsGreaterThan(
                    research, new Date(), 0,
                    Sort.by(Sort.Direction.ASC, "expirationDate"));
    }

    public int countAllOffer(){
        return offerRepository.countAllBy();
    }

    public Map<String, Long> mapTagsOccurences(){
        List<Offer> offerList = offerRepository.findAll();

        List<String> tagsList = new ArrayList<>();
        for(Offer offer: offerList){
            List<String> innerTagsList = offer.getTags();
            tagsList.addAll(innerTagsList);
        }

        Map<String, Long> mapTagsOccurences = getSortedMap(tagsList);

        Map<String, Long> mapTagsTop3= new HashMap<>();

        for(int i = 0; i < 3; i++) {
            Map.Entry<String, Long> maxEntry = null;

            for (Map.Entry<String, Long> entry : mapTagsOccurences.entrySet()) {
                if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                    maxEntry = entry;
                }
            }

            mapTagsTop3.put(maxEntry.getKey(), maxEntry.getValue());
            mapTagsOccurences.remove(maxEntry.getKey());
        }

        return mapTagsTop3;
    }

    final Map<String, Long> getSortedMap(List<String> wordsList) {
        return wordsList
                .stream()
                .collect(Collectors.groupingBy(Function.identity(), TreeMap::new, Collectors.counting()));
    }

}
