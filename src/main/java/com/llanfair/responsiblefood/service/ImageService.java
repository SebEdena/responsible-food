/**
 * This Service allows us to work the image system
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service;

import com.llanfair.responsiblefood.model.bean.image.DefaultImageEnum;
import com.llanfair.responsiblefood.model.mongo.collection.Image;
import com.llanfair.responsiblefood.model.mongo.collection.Offer;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import com.llanfair.responsiblefood.model.mongo.repository.ImageRepository;
import com.llanfair.responsiblefood.model.mongo.repository.OfferRepository;
import com.llanfair.responsiblefood.model.mongo.repository.UserRepository;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@PropertySource("classpath:/application.properties")
public class ImageService {

    @Value("${responsiblefood.max-image-size}")
    private int MAX_IMAGE_SIZE;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OfferRepository offerRepository;

    /**
     * Used to Update an image stored in MongoDB
     * @param defaultImageEnum
     * @param id
     * @param multipartFile
     * @return String
     * @throws IOException
     */
    public String updateImage(DefaultImageEnum defaultImageEnum, String id, MultipartFile multipartFile) throws IOException {
        switch (defaultImageEnum) {
            case PERSON: {
                Optional<User> optionalUser = userRepository.findById(id);
                if(optionalUser.isPresent()) {
                    User user = optionalUser.get();
                    String prevId = user.getImageId();

                    Image image = addImage(multipartFile);
                    user.setImageId(image.getId());
                    userRepository.save(user);

                    if(prevId != null) deleteImage(prevId);
                    return image.getId();
                }
                break;
            }
            case OFFER: {
                Optional<Offer> optionalOffer = offerRepository.findById(id);
                if(optionalOffer.isPresent()) {
                    Offer offer = optionalOffer.get();
                    String prevId = offer.getImageId();

                    Image image = addImage(multipartFile);
                    offer.setImageId(image.getId());
                    offerRepository.save(offer);

                    if(prevId != null) deleteImage(prevId);
                    return image.getId();
                }
                break;
            }
            default: break;
        }
        return null;
    }

    /**
     * Used to delete an image stored in MongoDb with its id
     * @param id
     */
    public void deleteImage(String id) {
        imageRepository.deleteById(id);
    }

    /**
     * Used to add an image in MongoDB
     * @param multipartFile
     * @return
     * @throws IOException
     */
    public Image addImage( MultipartFile multipartFile) throws IOException {
        Image image = new Image();
        image.setUpdateDate(new Date());
        image.setImageData(new Binary(BsonBinarySubType.BINARY, multipartFile.getBytes()));
        return imageRepository.save(image);
    }

    /**
     * Used to get the image corresponding to the id or the default image stored in MongoDB
     * @param id
     * @param defaultImageEnum
     * @return byte[]
     */
    public byte[] getImageOrDefault(String id, DefaultImageEnum defaultImageEnum) {
        Optional<Image> imageOptional = imageRepository.findById(id);

        if(imageOptional.isPresent()) {
            return imageOptional.get().getImageData().getData();
        } else {
            return getDefaultImage(defaultImageEnum);
        }
    }

    /**
     * Used to get the default image stored in MongoDB
     * @param defaultImageEnum
     * @return byte[]
     */
    public byte[] getDefaultImage(DefaultImageEnum defaultImageEnum) {
        byte[] defaultImage = null;
        try {
            defaultImage = Objects.requireNonNull(
                    this.getClass().getResourceAsStream(defaultImageEnum.getPath())
            ).readAllBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defaultImage;
    }

    /**
     * Used to validate an image
     * @param file
     * @return String
     */
    public String validateFile(MultipartFile file) {
        if(file.isEmpty()) {
            return "Veuillez sélectionner un fichier.";
        }
        if(!List.of(MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE).contains(file.getContentType())) {
            return "Le fichier n'est pas à un format valide.";
        }
        if(file.getSize() > MAX_IMAGE_SIZE) {
            return "Le fichier est trop volumineux.";
        }
        return null;
    }
}
