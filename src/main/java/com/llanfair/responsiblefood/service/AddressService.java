/**
 * This Service allows us to work the address system
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service;

import com.llanfair.responsiblefood.model.bean.mapbox.FeatureCollection;
import com.mongodb.client.model.geojson.Point;
import com.mongodb.client.model.geojson.Position;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;

@Service
@PropertySource("classpath:/mapbox.properties")
public class AddressService {

    @Value("${mapbox.token}")
    private String mapboxToken;

    @Value("${mapbox.geocodingURL}")
    private String geocodingURL;

    @Value("${mapbox.baseURL}")
    private String baseURL;

    private WebClient mapboxClient;

    @PostConstruct
    public void initWebClient() {
        mapboxClient = WebClient.builder()
                .baseUrl(baseURL)
                .build();
    }

    /**
     * Used to found the point (latitude, longitude) corresponding to an address
     * @param address
     * @return Point
     */
    public Point addressToPoint(String address) {

        Mono<FeatureCollection> featureCollectionMono = mapboxClient
                .get()
                .uri(uriBuilder -> uriBuilder
                    .path(geocodingURL + "/${text}.json")
                    .queryParam("access_token", mapboxToken)
                    .queryParam("country", "fr")
                    .queryParam("language", "fr")
                    .queryParam("limit", 1)
                    .queryParam("types", "postcode","locality","neighborhood","address","poi")
                    .build(address))
                .retrieve()
                .bodyToMono(FeatureCollection.class);

        try {
            FeatureCollection featureCollection = featureCollectionMono.block();

            return new Point(new Position(
                    featureCollection
                            .getFeatures()
                            .get(0)
                            .getGeometry()
                            .getCoordinates())
            );
        } catch (NullPointerException e) {
            return null;
        }
    }

    /**
     * Used to get all the informations about an address
     * Future Release
     * @param address
     * @return FeatureCollection
     */
    public FeatureCollection getAutocompleteResult(String address) {
        Mono<FeatureCollection> featureCollectionMono = mapboxClient
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(geocodingURL + "/${text}.json")
                        .queryParam("access_token", mapboxToken)
                        .queryParam("country", "fr")
                        .queryParam("language", "fr")
                        .queryParam("limit", 5)
                        .queryParam("types", "postcode","locality","neighborhood","address","poi")
                        .build(address))
                .retrieve()
                .bodyToMono(FeatureCollection.class);

        try {
            return featureCollectionMono.block();
        } catch (Exception e) {
            return null;
        }
    }
}
