/**
 * This Service allows us to work the mail system
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;

@Service
@PropertySource("classpath:/mail.properties")
public class MailService {

    @Value("${mail.from.address}")
    private String senderAddress;

    @Value("${mail.from.display}")
    private String senderDisplayName;

    @Autowired
    private JavaMailSender javaMailSender;

    /**
     * Used to send an email to a user
     * @param subject
     * @param message
     * @param to
     * @param cc
     * @throws Exception
     */
    @Async
    public void sendMail(String subject, String message, List<String> to, List<String> cc) throws Exception {
        MimeMessage email = initEmail();

        email.setSubject(subject);
        email.setText(message);

        for (String address : to) {
            email.addRecipient(Message.RecipientType.TO, new InternetAddress(address));
        }

        if(cc != null) {
            for (String address : cc) {
                email.addRecipient(Message.RecipientType.CC, new InternetAddress(address));
            }
        }

        javaMailSender.send(email);
    }

    /**
     * Used to create a mail from the account responsiblefood@free.fr
     * @return
     * @throws Exception
     */
    private MimeMessage initEmail() throws Exception {
        MimeMessage email = javaMailSender.createMimeMessage();
        email.setFrom(new InternetAddress(senderAddress, senderDisplayName));
        return email;
    }

}
