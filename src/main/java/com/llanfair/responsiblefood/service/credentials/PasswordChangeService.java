/**
 * This Service is for the Password change
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service.credentials;

import com.llanfair.responsiblefood.model.bean.token.TokenTypeEnum;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import com.llanfair.responsiblefood.model.mongo.collection.VerificationToken;
import com.llanfair.responsiblefood.model.mongo.repository.UserRepository;
import com.llanfair.responsiblefood.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.UUID;

@Service
public class PasswordChangeService {

    private static final int EXPIRATION = 15;

    private final TokenTypeEnum tokenType = TokenTypeEnum.FORGOTTEN_PASSWORD;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VerificationTokenService tokenService;

    @Autowired
    private MailService mailService;

    public VerificationToken getVerificationToken(String verificationToken) {
        return tokenService.getVerificationToken(verificationToken, tokenType);
    }

    /**
     * This function allows us to delete all tokens created for the user
     * @param user is the user
     */
    public void deleteAllTokensWithUser(User user) {
        tokenService.deleteAllTokensWithUserAndType(user, tokenType);
    }

    /**
     * This function allows us to check if the token has expired
     * @param token is the token
     * @return the status of the token
     */
    public boolean isPasswordExpired(VerificationToken token) {
        return tokenService.isTokenExpired(token);
    }

    /**
     * This function allows us to send an email for the reset of the password
     * @param email the mail of the user
     * @throws Exception if the mail cannot be sent
     */
    public void tokenResetPassword(String email) throws Exception {
        User user = userRepository.findByEmail(email);
        if(user != null) {
            String uuid = UUID.randomUUID().toString();
            VerificationToken token = tokenService.createVerificationToken(user, uuid, EXPIRATION, tokenType);
            tokenService.saveVerificationToken(token);

            String recipientAddress = user.getEmail();
            String subject = "Réinitialisation de votre mot de passe";
            String message = "Pour réinitaliser votre mot de passe, cliquez sur le lien ci-dessous" +
                    "\r\n" +
                    "http://localhost:8080/auth/token-password?token=" + uuid;
            mailService.sendMail(subject, message, Collections.singletonList(recipientAddress), null);
        } else {
            throw new UsernameNotFoundException("username not found");
        }
    }
}
