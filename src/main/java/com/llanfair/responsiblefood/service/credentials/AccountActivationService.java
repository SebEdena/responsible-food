/**
 * This Service is for the validation account
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service.credentials;

import com.llanfair.responsiblefood.model.bean.token.TokenTypeEnum;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import com.llanfair.responsiblefood.model.mongo.collection.VerificationToken;
import com.llanfair.responsiblefood.model.mongo.repository.UserRepository;
import com.llanfair.responsiblefood.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.UUID;

@Service
public class AccountActivationService {

    private static final int EXPIRATION = 60*24;

    private final TokenTypeEnum tokenType = TokenTypeEnum.ACCOUNT_VALIDATION;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VerificationTokenService tokenService;

    @Autowired
    private MailService mailService;

    public VerificationToken getVerificationToken(String verificationToken) {
        return tokenService.getVerificationToken(verificationToken, tokenType);
    }

    /**
     * This function allows us to generate a new token for the account activation
     * @param token a unique identifier
     * @return a verification token
     */
    public VerificationToken generateNewAccountActivationToken(String token) {
        VerificationToken oldToken = tokenService.getVerificationToken(token, TokenTypeEnum.ACCOUNT_VALIDATION);

        try {
            if(oldToken.getUser().getEmail() == null) {
                return null;
            }
            return resendAccountActivationToken(oldToken.getUser());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * This function allows us to validate an account
     * @param user is the user who will be activated
     */
    public void enableUser(User user) {
        user.setEnabled(true);
        userRepository.save(user);
        deleteAllTokensWithUser(user);
    }

    /**
     * This function allows us to generate a new token for an account activation
     * If the previous one has expired
     * @param email a unique identifier
     * @return a verification token
     */
    public VerificationToken createNewAccountActivationTokenFromEmail(String email) {
        User user = userRepository.findByEmail(email);

        try {
            if(email == null) {
                return null;
            }
            return resendAccountActivationToken(user);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * This function allows us to delete all tokens that has been created after an activation
     * @param user is the user
     */
    public void deleteAllTokensWithUser(User user) {
        tokenService.deleteAllTokensWithUserAndType(user, tokenType);
    }

    /**
     * This function allows us to verify if the token for the password forgotten has expired
     * @param token
     * @return
     */
    public boolean isPasswordExpired(VerificationToken token) {
        return tokenService.isTokenExpired(token);
    }

    /**
     * This function allows us to send an activation account for a user
     * @param user is the user who needs this mail
     * @param appUrl is the URL of the application
     * @throws Exception if the mail cannot be sent
     */
    public void sendAccountActivationToken(User user, String appUrl) throws Exception {

        String tokenUUID = UUID.randomUUID().toString();
        VerificationToken token = tokenService.createVerificationToken(user, tokenUUID, EXPIRATION, tokenType);
        tokenService.saveVerificationToken(token);

        String recipientAddress = user.getEmail();
        String subject = "Votre compte a été créé.";
        String confirmationUrl = appUrl + "/auth/token-validate?token=" + tokenUUID;
        String message = "Pour activer votre compte, cliquez sur le lien ci-dessous" +
                "\r\n" +
                "http://localhost:8080" + confirmationUrl;

        mailService.sendMail(subject, message, Collections.singletonList(recipientAddress), null);
    }

    /**
     * This function allows us to resend an activation account for a user
     * @param user is the use who needs this mail
     * @return a verification token
     * @throws Exception if the mail cannot be sent
     */
    private VerificationToken resendAccountActivationToken(User user) throws Exception {
        String uuid = UUID.randomUUID().toString();
        VerificationToken newToken = tokenService.createVerificationToken(user, uuid, EXPIRATION, tokenType);

        tokenService.deleteAllTokensWithUserAndType(user, TokenTypeEnum.ACCOUNT_VALIDATION);
        tokenService.saveVerificationToken(newToken);

        String subject = "Renvoi de lien d'activation";
        String confirmationUrl = "/auth/token-validate?token=" + uuid;
        String message = "Vous avez demandé le renvoi d'un lien d'activation de compte. Cliquez sur le lien ci-dessous pour y parvenir." +
                "\r\n" +
                "http://localhost:8080" + confirmationUrl;

        mailService.sendMail(subject, message, Collections.singletonList(user.getEmail()), null);

        return newToken;
    }
}
