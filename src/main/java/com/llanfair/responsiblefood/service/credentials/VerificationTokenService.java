/**
 * This Service is for the Verification token
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.service.credentials;

import com.llanfair.responsiblefood.model.bean.token.TokenTypeEnum;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import com.llanfair.responsiblefood.model.mongo.collection.VerificationToken;
import com.llanfair.responsiblefood.model.mongo.repository.VerificationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class VerificationTokenService {

    @Autowired
    private VerificationTokenRepository tokenRepository;

    public VerificationToken getVerificationToken(String verificationToken, TokenTypeEnum tokenType) {
        return tokenRepository.findByTokenAndTokenType(verificationToken, tokenType);
    }

    /**
     * This function allows us to delete all tokens of a user
     * @param user is the user
     * @param tokenType is the type of the token
     */
    public void deleteAllTokensWithUserAndType(User user, TokenTypeEnum tokenType) {
        tokenRepository.deleteAllByUserAndTokenType(user, tokenType);
    }

    /**
     * This function allows us to delete all tokens of a user
     * @param user is the user
     */
    public void deleteAllTokensWithUser(User user) {
        tokenRepository.deleteAllByUser(user);
    }

    /**
     * This function allows us to check if the token has expired or not
     * @param token is the token to check
     * @return the status of the token
     */
    public boolean isTokenExpired(VerificationToken token) {
        Calendar cal = Calendar.getInstance();
        return (token.getExpirationDate().getTime() - cal.getTime().getTime()) <= 0;
    }

    /**
     * This function allows us to create a verification token
     * @param user is the user
     * @param token is the token
     * @param expirationTime is the duration of the expiration
     * @param tokenType is the type of token
     * @return the verification token
     */
    public VerificationToken createVerificationToken(User user, String token, int expirationTime, TokenTypeEnum tokenType) {
        VerificationToken myToken = new VerificationToken();
        myToken.setUser(user);
        myToken.setToken(token);
        myToken.setTokenType(tokenType);
        myToken.computeExpirationDate(expirationTime);
        return myToken;
    }

    /**
     * This function allows us to save the token on the database
     * @param verificationToken is the verification token
     */
    public void saveVerificationToken(VerificationToken verificationToken) {
        tokenRepository.save(verificationToken);
    }
}
