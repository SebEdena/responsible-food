package com.llanfair.responsiblefood;

import com.llanfair.responsiblefood.model.InitData;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

//@SpringBootApplication
public class InitDataApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ResponsibleFoodWebApplication.class, args);
        context.getBean(InitData.class).initData();
    }
}
