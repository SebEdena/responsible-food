/**
 * This Repository is for quering mongoDB User collection
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.model.mongo.repository;

import com.llanfair.responsiblefood.model.mongo.collection.Role;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    User findByEmail(String email);

    Optional<User> findById(String id);

    List<User> findByRoles(Role role);

    List<User> findAll();

    /**
     * Query counting the number of instance for a role in base
     * @param role role to count
     * @return number of instance
     */
    int countAllByRoles(Role role);

    /**
     * Query to find users with firstname, lastname, and company (for producers) using a string
     * @param text research string
     * @return list of user corresponding to the research
     */
    @Query("{'$text' : { '$search' : ?0 , '$diacriticSensitive' : true, '$language' : 'fr', '$caseSensitive' : false}}")
    List<User> findUsers(String text);

}