/**
 * This Repository allows us to deal with the collection Unit in MongoDB
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.model.mongo.repository;

import com.llanfair.responsiblefood.model.mongo.collection.Unit;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface UnitRepository extends MongoRepository<Unit, String>{

    /**
     * Used to find all the Units
     * @return ArrayList<Unit>
     */
    ArrayList<Unit> findAll();

    /**
     * Used to find a symbol from a string
     * @param symbol
     * @return Unit
     */
    Unit findBySymbol(String symbol);

}
