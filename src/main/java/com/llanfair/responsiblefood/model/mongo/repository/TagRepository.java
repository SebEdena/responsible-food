/**
 * This Repository allows us to deal with the collection Tag in MongoDB
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.model.mongo.repository;

import com.llanfair.responsiblefood.model.mongo.collection.Tag;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface TagRepository extends MongoRepository<Tag, String> {

    /**
     * Used to find all the tags
     * @return ArrayList<Tag>
     */
    ArrayList<Tag> findAll();

    /**
     * Used to find tags from a family
     * @param family
     * @return List<Tag>
     */
    List<Tag> findTagByFamily(String family);

    /**
     * Used to count the number of tags
     * @return int
     */
    int countAllBy();
}
