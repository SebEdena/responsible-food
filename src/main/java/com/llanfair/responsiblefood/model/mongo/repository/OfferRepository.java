/**
 * This Repository allows us to deal with the collection Offer in MongoDB
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.model.mongo.repository;

import com.llanfair.responsiblefood.model.mongo.collection.Offer;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface OfferRepository extends MongoRepository<Offer, String> {

    /**
     * Used to find all the offer of an author
     * @param author
     * @return List<Offer>
     */
    List<Offer> findAllByAuthor(User author);

    /**
     * Used to find offers of an author with the expiration date greater or equal than a value and available quantity greater than a value
     * @param author
     * @param date
     * @param minStock
     * @param sort
     * @return
     */
    List<Offer> findAllByAuthorAndExpirationDateGreaterThanEqualAndAvailableGreaterThan(User author, Date date, int minStock, Sort sort);

    /**
     * Used to find the offer corresponding to the id
     * @param id
     * @return Offer
     */
    Offer findOfferById(String id);

    /**
     * Used to find all the offer
     * @return List<Offer>
     */
    List<Offer> findAll();

    /**
     * Used to find offers corresponding to tags with the expiration date greater or equal than a value and the available quantity greater a value
     * @param tags
     * @param expirationDate
     * @param minStock
     * @param sort
     * @return List<Offer>
     */
    @Query("{'tags' : {'$in' : ?0}, 'expirationDate' : {'$gte' : ?1}, 'available' : {'$gt' : ?2}}")
    List<Offer> findAllByTagsIsInAndExpirationDateGreaterThanEqualAndAvailableGreaterThan(List<String> tags, Date expirationDate, int minStock, Sort sort);

    /** Used to find offers with expiration date greater or equal than a value and the available quantity greater than a value
     * Used to find
     * @param date
     * @param minStock
     * @param sort
     * @return List<Offer>
     */
    @Query("{'expirationDate' : {'$gte' : ?0}, 'available' : {'$gt' : ?1}}")
    List<Offer> findOffersByExpirationDateGreaterThanEqualAndAvailableIsGreaterThan(Date date, int minStock, Sort sort);

    /**
     * Used to find offers with expiration date greater or equal than a value and the available quatity greater than a value
     * @param text
     * @param date
     * @param minStock
     * @param sort
     * @return List<Offer>
     */
    @Query("{'$text' : { '$search' : ?0 , '$diacriticSensitive' : true, '$language' : 'fr', '$caseSensitive' : false}, 'expirationDate' : {'$gte' : ?1}, 'available' : {'$gt' : ?2}}")
    List<Offer> findAllByExpirationDateGreaterThanEqualAndAvailableIsGreaterThan(String text, Date date, int minStock, Sort sort);

    /**
     * Used to find most recent offers with a minimum of stock
     * @param date
     * @param minStock
     * @param pageable
     * @return List<Offer>
     */
    @Query("{'expirationDate' : {'$gte' : ?0}, 'available' : {'$gt' : ?1}}")
    List<Offer> findLimitedNumberMostRecentOffersWithMinStock(Date date, int minStock, Pageable pageable);

    /**
     * Used to count the number of offer
     * @return int
     */
    int countAllBy();
}
