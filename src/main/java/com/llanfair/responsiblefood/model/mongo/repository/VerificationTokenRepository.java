/**
 * This interface allows us to call mongodb for queries linked to VerificationToken
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */
package com.llanfair.responsiblefood.model.mongo.repository;

import com.llanfair.responsiblefood.model.bean.token.TokenTypeEnum;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import com.llanfair.responsiblefood.model.mongo.collection.VerificationToken;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VerificationTokenRepository extends MongoRepository<VerificationToken, Long> {

    /**
     * This function allows us to find the verification token by its token and the type
     * @param token is the searched token
     * @param tokenType is the needed type
     * @return the verification token
     */
    VerificationToken findByTokenAndTokenType(String token, TokenTypeEnum tokenType);

    /**
     * This function allows us to find the verification token by its user and the type
     * @param user is the user
     * @param tokenType is the needed type
     * @return the verification token
     */
    VerificationToken findByUserAndTokenType(User user, TokenTypeEnum tokenType);

    /**
     * This function allows us to delete all tokens of the defined type of the user
     * @param user is the user
     * @param tokenType is the type
     */
    void deleteAllByUserAndTokenType(User user, TokenTypeEnum tokenType);

    /**
     * This function allows us to delete all tokens of the user
     * @param user is the user
     */
    void deleteAllByUser(User user);
}

