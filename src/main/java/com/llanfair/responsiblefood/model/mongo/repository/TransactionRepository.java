/**
 * This Repository allows us to deal with the collection Transaction in MongoDB
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.model.mongo.repository;

import com.llanfair.responsiblefood.model.mongo.collection.Offer;
import com.llanfair.responsiblefood.model.mongo.collection.Transaction;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends MongoRepository<Transaction, String> {

    /**
     * Used to find all the transactions of a user
     * @param user
     * @return List<Transaction>
     */
    List<Transaction> findAllByBuyer(User user);

    /**
     * Used to find the transaction from its id
     * @param id
     * @return Transaction
     */
    Transaction findTransactionById(String id);

    /**
     * Used to find all the transactions from an author
     * @param user
     * @return List<Transaction>
     */
    List<Transaction> findAllTransactionByOffer_Author(User user);

    /**
     * Used to find all the transactions of an offer
     * @param offer
     * @return List<Transaction>
     */
    List<Transaction> findAllByOffer(Offer offer);

    /**
     * Used to delete all the transactions of an offer
     * @param offer
     */
    void deleteAllByOffer(Offer offer);

}
