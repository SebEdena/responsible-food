/**
 * This class is a document in mongodb and describes a verificationToken
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.model.mongo.collection;

import com.llanfair.responsiblefood.model.bean.token.TokenTypeEnum;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Calendar;
import java.util.Date;

@Document("token")
public class VerificationToken {

    @Id
    private String id;

    @DBRef
    private User user;

    private String token;

    private TokenTypeEnum tokenType;

    private Date expirationDate;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public TokenTypeEnum getTokenType() {
        return tokenType;
    }

    public void setTokenType(TokenTypeEnum tokenType) {
        this.tokenType = tokenType;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    /**
     * This function allows us to
     * @param expirationDateInterval is the duration of the expiration
     */
    public void computeExpirationDate(int expirationDateInterval) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(cal.getTime());
        cal.add(Calendar.MINUTE, expirationDateInterval);
        expirationDate = Date.from(cal.toInstant());
    }
}
