/**
 * This interface allows us to call mongodb for queries linked to Currency
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.model.mongo.repository;

import com.llanfair.responsiblefood.model.mongo.collection.Currency;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CurrencyRepository extends MongoRepository<Currency, String> {

    /**
     * This function allows us to find a currency by its is
     * @param id is the id of the currency object
     * @return The currency
     */
    Optional<Currency> findById(String id);

    /**
     * This function allows us to find the currency by its code
     * @param id the id of the currency object
     * @return th currency
     */
    Currency findByCode(String id);

    /**
     * This function allows us to find the currency by its name
     * @param id the id of the currency object
     * @return th currency
     */
    Currency findByName(String id);
}
