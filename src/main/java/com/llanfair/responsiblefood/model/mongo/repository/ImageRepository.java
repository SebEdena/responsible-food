/**
 * This interface allows us to call mongodb for queries linked to ImageRepository
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.model.mongo.repository;

import com.llanfair.responsiblefood.model.mongo.collection.Image;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ImageRepository extends MongoRepository<Image, String> {

    /**
     * This function allows us to find the Image by the id
     * @param id is the id of the image
     * @return the image
     */
    Optional<Image> findById(String id);

}
