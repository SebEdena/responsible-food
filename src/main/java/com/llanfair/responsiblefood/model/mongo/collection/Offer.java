/**
 * This class is a document in mongodb and describes a offer
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.model.mongo.collection;

import com.mongodb.client.model.geojson.Point;
import com.mongodb.client.model.geojson.Position;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document(collection = "offer")
public class Offer {

    @Id
    private String id;

    private Date timeStamp;

    @DBRef
    private User author;

    private String title;

    private String imageId;

    private String description;

    private Date expirationDate;

    private Integer total;

    private Integer available;

    private Double pricePerUnit;

    private Double quantityPerUnit;

    @DBRef
    private Unit unit;

    @DBRef
    private Currency currency;

    private List<String> tags;

    private List<Object> messages;

    private String address;

    private Point geoxy;
    // https://mongodb.github.io/mongo-java-driver/3.6/javadoc/?com/mongodb/client/model/geojson/Point.html
    // https://mongodb.github.io/mongo-java-driver/3.6/javadoc/com/mongodb/client/model/geojson/Position.html
    // !chercher des points proches https://mongodb.github.io/mongo-java-driver/3.5/driver/tutorials/geospatial-search/


    public Offer(){
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    public Double getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(Double pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Object getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<Object> getMessages() {
        return messages;
    }

    public void setMessages(List<Object> messages) {
        this.messages = messages;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Point getGeoxy() {
        return geoxy;
    }

    public void setGeoxy(Point geoxy) {
        this.geoxy = geoxy;
    }

    public Double getQuantityPerUnit() {
        return quantityPerUnit;
    }

    public void setQuantityPerUnit(Double quantityPerUnit) {
        this.quantityPerUnit = quantityPerUnit;
    }
}