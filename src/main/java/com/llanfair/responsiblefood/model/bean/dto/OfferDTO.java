/**
 * This DTO is for the Offer forgotten
 * If the object Offer contains values that does not match with the validator an error will be thrown
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.model.bean.dto;

import com.llanfair.responsiblefood.model.mongo.collection.Offer;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import com.llanfair.responsiblefood.service.business.validator.annotation.image.ImageFormatAuthorized;
import com.llanfair.responsiblefood.service.business.validator.annotation.image.ImageSizeAuthorized;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class OfferDTO {

    private String id;

    private Date timeStamp;

    private User author;

    @NotNull
    @NotEmpty
    private String title;

    private String imageId;

    @ImageFormatAuthorized
    @ImageSizeAuthorized
    private MultipartFile image;

    private String description;

    @NotNull
    @NotEmpty
    private String expirationDate;

    @NotNull
    private Integer total;

    private Integer available;

    @NotNull
    private Double pricePerUnit;

    @NotNull
    private Double quantityPerUnit;

    @NotNull
    @NotEmpty
    private String unit;

    private Object currency;

    @NotNull
    @NotEmpty
    private List<String> tags;

    private List<Object> buyers;

    private List<Object> messages;

    @NotNull
    @NotEmpty
    private String address;

    public OfferDTO(){
    }

    public OfferDTO(Offer offer){
        this.id = offer.getId();
        this.timeStamp = new Date();
        this.author = offer.getAuthor();
        this.title = offer.getTitle();
        this.imageId = offer.getImageId();
        this.description = offer.getDescription();
        if (offer.getExpirationDate() == null){
            int nbMilliSecondInADay = 1000 * 60 * 60 * 24;
            this.expirationDate = new SimpleDateFormat("yyyy-MM-dd").format((new Date()).getTime() + nbMilliSecondInADay);
        }else{
            this.expirationDate = new SimpleDateFormat("yyyy-MM-dd").format(offer.getExpirationDate());
        }
        this.total = offer.getTotal();
        this.available = offer.getAvailable();
        this.pricePerUnit = offer.getPricePerUnit();
        this.quantityPerUnit = offer.getQuantityPerUnit();
        this.unit = offer.getUnit().getSymbol();
        this.tags = offer.getTags();
        this.address = offer.getAddress();
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    public Double getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(Double pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Object getCurrency() {
        return currency;
    }

    public void setCurrency(Object currency) {
        this.currency = currency;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<Object> getBuyers() {
        return buyers;
    }

    public void setBuyers(List<Object> buyers) {
        this.buyers = buyers;
    }

    public List<Object> getMessages() {
        return messages;
    }

    public void setMessages(List<Object> messages) {
        this.messages = messages;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getQuantityPerUnit() {
        return quantityPerUnit;
    }

    public void setQuantityPerUnit(Double quantityPerUnit) {
        this.quantityPerUnit = quantityPerUnit;
    }

    @Override
    public String toString() {
        return "OfferDTO{" +
                "id='" + id + '\'' +
                ", timeStamp=" + timeStamp +
                ", author=" + author +
                ", title='" + title + '\'' +
                ", imageId=" + imageId +
                ", description='" + description + '\'' +
                ", expirationDate='" + expirationDate + '\'' +
                ", total=" + total +
                ", available=" + available +
                ", pricePerUnit=" + pricePerUnit +
                ", quantityPerUnit=" + quantityPerUnit +
                ", unit='" + unit + '\'' +
                ", currency=" + currency +
                ", tags=" + tags +
                ", buyers=" + buyers +
                ", messages=" + messages +
                ", address='" + address + '\'' +
                '}';
    }
}
