/**
 * This class represents a part of the answer from MapBox named Properties
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */
package com.llanfair.responsiblefood.model.bean.mapbox;

import lombok.Data;

@Data
public class Properties {

    private String address;
            
    private String category;

    private String maki;
            
    private boolean landmark;
            
    private String wikidata;
            
    private String short_code;

}
