/**
 * This DTO is for the Email forgotten
 * If the object Email contains values that does not match with the validator an error will be thrown
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */
package com.llanfair.responsiblefood.model.bean.dto;

import com.llanfair.responsiblefood.service.business.validator.annotation.email.EmailInDb;
import com.llanfair.responsiblefood.service.business.validator.annotation.email.ValidEmail;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class EmailDTO {

    @NotNull
    @NotEmpty
    @ValidEmail
    @EmailInDb
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
