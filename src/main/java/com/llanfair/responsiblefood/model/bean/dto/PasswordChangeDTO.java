/**
 * This DTO is for the PasswordChange forgotten
 * If the object Password contains values that does not match with the validator an error will be thrown
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.model.bean.dto;

import com.llanfair.responsiblefood.service.business.validator.annotation.password.PasswordsMatch;
import com.llanfair.responsiblefood.service.business.validator.annotation.password.ValidPassword;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@PasswordsMatch
public class PasswordChangeDTO {

    @NotNull
    @NotEmpty
    private String currentPassword;

    @NotNull
    @NotEmpty
    @ValidPassword
    private String newPassword;

    @NotNull
    @NotEmpty
    private String newPasswordConfirmation;

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPasswordConfirmation() {
        return newPasswordConfirmation;
    }

    public void setNewPasswordConfirmation(String newPasswordConfirmation) {
        this.newPasswordConfirmation = newPasswordConfirmation;
    }
}
