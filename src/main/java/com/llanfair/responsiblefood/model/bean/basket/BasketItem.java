/**
 * This class allows us to have one item of the basket
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.model.bean.basket;

import com.llanfair.responsiblefood.model.mongo.collection.Offer;
import lombok.Data;

@Data
public class BasketItem {
    private Offer offer;
    private int quantity;

    public BasketItem(Offer offer, int quantity){
        this.offer = offer;
        this.quantity = quantity;
    }
}
