/**
 * This class represents a part of the answer from MapBox named Feature
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.model.bean.mapbox;

import lombok.Data;

import java.util.List;

@Data
public class Feature {

    private String id;

    private String type;

    private List<String> place_type;

    private int relevance;

    private String address;

    private Properties properties;

    private String text;

    private String language;

    private String place_name;

    private String matching_text;

    private String matching_place_name;

    private List<Double> bbox;

    private List<Double> center;

    private Geometry geometry;

    private List<Feature> context;

}
