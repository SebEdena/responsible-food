/**
 * This class represents a part of the answer from MapBox named FeatureCollection
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.model.bean.mapbox;

import lombok.Data;

import java.util.List;

@Data
public class FeatureCollection {

    private String type;

    private List<String> query;

    private List<Feature> features;

    private String attribution;
}
