/**
 * This Enum gives a type for the token
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */
package com.llanfair.responsiblefood.model.bean.token;

public enum TokenTypeEnum {

    ACCOUNT_VALIDATION("account-validation"),
    FORGOTTEN_PASSWORD("forgotten-password");

    private final String type;

    TokenTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
