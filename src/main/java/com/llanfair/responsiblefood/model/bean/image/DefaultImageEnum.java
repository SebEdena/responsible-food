/**
 * This Enum gives a default image for a specific field
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */
package com.llanfair.responsiblefood.model.bean.image;

import java.util.Arrays;

public enum DefaultImageEnum {

    OFFER("/image/offer.png", "offer"),
    PERSON("/image/person.png", "person"),
    DEFAULT("/image/default.png", "default");

    private final String path;
    private final String name;

    DefaultImageEnum(String path, String name) {
        this.path = path;
        this.name = name;
    }

    public static DefaultImageEnum getByNameOrDefault(String name) {
        return Arrays.stream(DefaultImageEnum.values())
                .filter(defaultImageEnum -> defaultImageEnum.getName().equals(name))
                .findFirst()
                .orElse(DEFAULT);
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }
}
