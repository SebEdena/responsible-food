/**
 * This class represents a part of the answer from MapBox named Geometry
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */
package com.llanfair.responsiblefood.model.bean.mapbox;

import lombok.Data;

import java.util.List;

@Data
public class Geometry {

    private List<Double> coordinates;

    private String type;

}
