/**
 * This Exception is thrown when the offer has expired
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.model.exception.basket;

public class ExpiredOfferException extends Exception{
    public ExpiredOfferException(String nomOffre){
        super("L'offre : " + nomOffre + " a expiré");
    }
}
