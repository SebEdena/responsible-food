/**
 * This Exception is thrown when the offer is not available
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.model.exception.basket;

public class NotAvailableException extends Exception{
    public NotAvailableException(String message){
        super(message);
    }
}
