/**
 * This Exception is thrown when there is nothing in basket
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.model.exception.basket;

public class NothingInBasket extends Exception{
    public NothingInBasket(){
        super("Il n'y a rien dans votre panier");
    }
}
