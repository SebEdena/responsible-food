/**
 * This Exception is thrown when the offer doesn't exist
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.model.exception.basket;

public class NoOfferException extends Exception{
    public NoOfferException(String idOffer){
        super("L'offre : " + idOffer + " n'existe pas");
    }
}
