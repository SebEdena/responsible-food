/**
 * This initData can initialize the database
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */
package com.llanfair.responsiblefood.model;

import com.llanfair.responsiblefood.model.mongo.collection.Role;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import com.llanfair.responsiblefood.model.mongo.repository.RoleRepository;
import com.llanfair.responsiblefood.model.mongo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public class InitData {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    /**
     * set up a new default role collection and create the admin user in the dataBase
     */
    public void initData() {

        List<Role> roles = Arrays.asList(
                new Role("farmer"),
                new Role("association"),
                new Role("client"),
                new Role("retailer"),
                new Role("admin")
        );

        User admin = new User();
        admin.setEmail("responsiblefood@free.fr");
        admin.setPassword(bCryptPasswordEncoder.encode("Foodfood!"));
        Role userRole = roleRepository.findByRole("admin");
        admin.setRoles(Collections.singletonList(userRole));
        admin.setImageId(null);
        admin.setFirstName("Responsible");
        admin.setLastName("Food");
        admin.setPhone("0123456789");
        admin.setCompany("ResponsibleFood");
        admin.setAddress("28 Rue Notre Dame des Champs");
        admin.setZipCode("75006");
        admin.setCity("Paris");
        admin.setEnabled(true);

        MongoOperations ops = mongoTemplate;

        ops.dropCollection("role");

        ops.insertAll(roles);

        userRepository.save(admin);
    }
}
