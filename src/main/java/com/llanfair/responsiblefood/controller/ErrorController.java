/**
 * This Controller is for the Error
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;

@Controller("error")
@ControllerAdvice
@Slf4j
public class ErrorController {

    /**
     * This function allows us to handle an Unknown Exception
     * @param request instance that call the function
     * @param e is the exception thrown
     * @return the view that needs to be displayed
     */
    @ExceptionHandler(Exception.class)
    public ModelAndView handleException(HttpServletRequest request, Exception e) {
        ModelAndView mv = new ModelAndView("/error");
        mv.addObject("exception", e.getMessage());
        mv.addObject("url", request.getRequestURL());
        return mv;
    }

    /**
     * This function allows us to handle the no hadlerException
     * @param request instance that call the function
     * @param e is the exception thrown
     * @return the view that needs to be displayed
     */
    @ExceptionHandler(value = NoHandlerFoundException.class)
    public ModelAndView handleNoHandlerException(HttpServletRequest request, Exception e) {
        ModelAndView mv = new ModelAndView("/error/unknownPage");
        log.error("No handler found for " + request.getRequestURL());
        mv.addObject("exception", e.getMessage());
        mv.addObject("url", request.getRequestURL());
        return mv;
    }

    /**
     * This function allows us to handle the AccessDeniedException
     * @param request instance that call the function
     * @param ex is the exception thrown
     * @return the view that needs to be displayed
     */
    @ExceptionHandler(value = AccessDeniedException.class)
    public ModelAndView commence(HttpServletRequest request, AccessDeniedException ex ) {
        ModelAndView mv = new ModelAndView("/error/accessDenied");
        mv.addObject("exception", ex.getMessage());
        mv.addObject("url", request.getRequestURL());
        mv.setStatus(HttpStatus.FORBIDDEN);
        return mv;
    }

    /**
     * This function allows us to handle the MultipartException
     * @param request instance that call the function
     * @param e is the exception thrown
     * @return the view that needs to be displayed
     */
    @ExceptionHandler(MultipartException.class)
    public ModelAndView handleMultipartException(HttpServletRequest request, MultipartException e) {
        ModelAndView mv = new ModelAndView("/error/badRequest");
        mv.addObject("exception", e.getMessage());
        mv.addObject("url", request.getRequestURL());
        mv.setStatus(HttpStatus.BAD_REQUEST);
        return mv;
    }

    /**
     * This function allows us to handle the ValidationException
     * @param request instance that call the function
     * @param e is the exception thrown
     * @return the view that needs to be displayed
     */
    @ExceptionHandler(ValidationException.class)
    public ModelAndView handleValidationException(HttpServletRequest request, MultipartException e) {
        ModelAndView mv = new ModelAndView("/error");
        mv.addObject("exception", e.getMessage());
        mv.addObject("url", request.getRequestURL());
        return mv;
    }
}
