/**
 * This Controller is for the Basket
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.controller;

import com.llanfair.responsiblefood.config.cookie.CookieUtils;
import com.llanfair.responsiblefood.model.exception.basket.ExpiredOfferException;
import com.llanfair.responsiblefood.model.exception.basket.NoOfferException;
import com.llanfair.responsiblefood.model.exception.basket.NotAvailableException;
import com.llanfair.responsiblefood.model.exception.basket.NothingInBasket;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import com.llanfair.responsiblefood.service.ShoppingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Slf4j
@Controller
@RequestMapping(value = "/shop")
@PreAuthorize("hasRole('client')")
@SessionAttributes({"basket",  "user"})
public class ShoppingController {

    @Autowired
    private ShoppingService shoppingService;

    @Autowired
    private CookieUtils cookieUtils;

    /**
     * This function allows us to add an item on the basket
     * @param quantity is the quantity asked by the user
     * @param id is the id of the offer
     * @param request instance that call the function
     * @param redirectAttributes allows us to have variables stored shortly
     * @return is the path of the view that the application will display
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String addToBasket(@RequestParam("qte") int quantity, @PathVariable() String id, HttpServletRequest request, RedirectAttributes redirectAttributes){
        try{
            request.getSession().setAttribute("basket", shoppingService.putOfferInBasket(id, quantity, request));
            redirectAttributes.addFlashAttribute("okMessage", "L'offre a été ajoutée dans votre panier");
            return returnPath(request);
        }catch (NoOfferException | NotAvailableException | ExpiredOfferException e){
            redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
            return returnPath(request);
        }
    }

    /**
     * This function allows us to delete an item from the basket
     * @param id is the id of the offer
     * @param request instance that call the function
     * @param redirectAttributes allows us to have variables stored shortly
     * @return is the path of the view that the application will display
     */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String deleteFromBasket (@PathVariable() String id, HttpServletRequest request, RedirectAttributes redirectAttributes){
        try{
            request.getSession().setAttribute("basket", shoppingService.deleteOffer(id, request));
            redirectAttributes.addFlashAttribute("okMessage", "L'offre a été supprimée de votre panier");
            return returnPath(request);
        }catch (NothingInBasket e){
            redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
            return returnPath(request);
        }
    }

    /**
     * This function allows us to delete all offers that are in the basket
     * @param request instance that call the function
     * @param redirectAttributes allows us to have variables stored shortly
     * @return is the path of the view that the application will display
     */
    @RequestMapping(value = "/deleteAll", method = RequestMethod.GET)
    public String deleteAllFromBasket(HttpServletRequest request, RedirectAttributes redirectAttributes){
        try{
            request.getSession().setAttribute("basket", shoppingService.deleteAllOffers(request));
            redirectAttributes.addFlashAttribute("okMessage", "Le panier a été vidé");
            return returnPath(request);
        }catch(NothingInBasket e){
            redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
            return returnPath(request);
        }

    }

    /**
     * This function allows us to go to the confirmation page
     * @param request instance that call the function
     * @return is the path of the view that the application will display
     */
    @RequestMapping(value = "/confirmation", method = RequestMethod.GET)
    public String confirmationBasket(HttpServletRequest request){
        return "/basket/confirmation";
    }

    /**
     * This function allows us to validate the basket
     * @param request instance that call the function
     * @param user is the user who validate the basket
     * @param redirectAttributes allows us to have variables stored shortly
     * @return is the path of the view that the application will display
     */
    @RequestMapping(value = "/validation", method = RequestMethod.GET)
    public String validationBasket(HttpServletRequest request, @ModelAttribute User user, RedirectAttributes redirectAttributes) {
        try{
            shoppingService.validateBasket(request, user);
            redirectAttributes.addFlashAttribute("basket", shoppingService.deleteAllOffers(request));
            return "/basket/validation";
        }catch(NothingInBasket | ExpiredOfferException | NotAvailableException e){
            redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
            return "/basket/confirmation";
        }

    }

    /**
     * This function returns the previous path of view
     * @param request instance that call the function
     * @return is the path of the view that the application will display
     */
    private String returnPath(HttpServletRequest request){
        Optional<Cookie> cookieOptional = cookieUtils.getCookieOptional(request, "RF_PREV_PAGE");
        return cookieOptional.map(cookie -> "redirect:" + cookie.getValue()).orElse("/");
    }
}
