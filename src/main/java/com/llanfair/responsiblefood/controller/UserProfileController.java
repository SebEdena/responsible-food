/**
 * This Controller is for the display and edition of user profiles
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.controller;

import com.llanfair.responsiblefood.config.cookie.CookieUtils;
import com.llanfair.responsiblefood.model.bean.dto.PasswordChangeDTO;
import com.llanfair.responsiblefood.model.bean.dto.UserDetailsDTO;
import com.llanfair.responsiblefood.model.exception.user.UserNotFoundException;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import com.llanfair.responsiblefood.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/user")
public class UserProfileController {

    @Autowired
    private UserService userService;

    @Autowired
    private CookieUtils cookieUtils;

    /**
     * Retrieves the details of a user
     * @param id The user id
     * @param update if an update has been done
     * @param request The http request
     * @param model The model map
     * @return The user details view
     */
    @RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
    public String showUserDetails (
            @PathVariable("id") String id,
            @ModelAttribute("update") Object update,
            HttpServletRequest request,
            ModelMap model)
    {
        boolean editable = (
            request.getSession(false).getAttribute("user") != null &&
            ((User) request.getSession(false).getAttribute("user")).getId().equals(id)
        );
        UserDetailsDTO details = userService.buildUserDetailsObject(id);
        if(details == null) {
            Optional<Cookie> cookieOptional = cookieUtils.getCookieOptional(request, "RF_PREV_PAGE");
            return cookieOptional.map(cookie -> "redirect:" + cookie.getValue()).orElse("redirect:/");
        } else {
            model.addAttribute("details", details);
            model.addAttribute("editable", editable);
            return "personalData/personalData";
        }
    }

    /**
     * Retrieves the user details from the user in the session
     * @param request The http request
     * @return The user details
     */
    @PreAuthorize("hasAnyRole('farmer','retailer', 'client', 'association')")
    @RequestMapping(value = "/details", method = RequestMethod.GET)
    public String showUserDetailNoId(HttpServletRequest request)
    {
        Optional<User> userOptional = Optional.ofNullable((User) request.getSession(false).getAttribute("user"));
        return userOptional.map(user -> "redirect:/user/details/" + user.getId()).orElse("redirect:/");
    }

    /**
     * Retrieves the user details edition page
     * @param request The http requeset
     * @param model The model map
     * @param redirectAttributes The redirect attributes data
     * @return The User details editor view
     */
    @PreAuthorize("hasAnyRole('farmer','retailer', 'client', 'association')")
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String showUserEditDataForm(
            HttpServletRequest request,
            Model model,
            RedirectAttributes redirectAttributes)
    {
        Optional<User> userOptional = Optional.ofNullable((User) request.getSession(false).getAttribute("user"));
        if(userOptional.isPresent()) {
            if(!model.containsAttribute("user"))
                model.addAttribute("user", userService.buildUserDetailsObject(userOptional.get().getId()));

            if(!model.containsAttribute("passwordChange") ||
                    (model.containsAttribute("update") && ((Boolean) model.getAttribute("update")))) {
                model.addAttribute("passwordChange", userService.buildPasswordChangeObject());
            }

            return "personalData/editPersonalData";
        } else {
            Optional<Cookie> cookieOptional = cookieUtils.getCookieOptional(request, "RF_PREV_PAGE");
            return cookieOptional.map(cookie -> "redirect:" + cookie.getValue()).orElse("redirect:/");
        }
    }

    /**
     * Update user details
     * @param request The request
     * @param userDetailsDTO The form data
     * @param bindingResult The form errors
     * @param model The model map
     * @param redirectAttributes The redirect attributes data
     * @return The confirmation or error view
     * @throws IOException
     */
    @PreAuthorize("hasAnyRole('farmer','retailer', 'client', 'association')")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public RedirectView updateUserData(
            HttpServletRequest request,
            @ModelAttribute("user") @Valid UserDetailsDTO userDetailsDTO,
            BindingResult bindingResult,
            Model model,
            RedirectAttributes redirectAttributes)
            throws IOException
    {
        Optional<User> userOptional = Optional.ofNullable((User) request.getSession(false).getAttribute("user"));

        if(bindingResult.hasErrors()) {
            return getEditProfileModelView(userOptional.orElse(null), model, userDetailsDTO, null, false, redirectAttributes);
        } else {
            boolean update = false;
            UserDetailsDTO nextUserDetailsDTO = null;
            if(userOptional.isPresent()) {
                String email = userOptional.get().getEmail();
                User user = userService.updateUserDetails(email, userDetailsDTO);
                if(user != null){
                    request.getSession(false).setAttribute("user", user);
                    nextUserDetailsDTO = userService.buildUserDetailsObject(user.getId());
                    update = true;
                }
            }

            return getEditProfileModelView(userOptional.orElse(null), model, nextUserDetailsDTO, null, update, redirectAttributes);
        }
    }

    /**
     * Update the password of a user
     * @param request The request
     * @param passwordChangeDTO The password form data
     * @param bindingResult The form errors
     * @param model The model map
     * @param redirectAttributes The redirect attributes data
     * @return The confirmation or error view
     */
    @PreAuthorize("hasAnyRole('farmer','retailer', 'client', 'association')")
    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    public RedirectView changePasswordData(
            HttpServletRequest request,
            @ModelAttribute("passwordChange") @Valid PasswordChangeDTO passwordChangeDTO,
            BindingResult bindingResult,
            Model model,
            RedirectAttributes redirectAttributes)
    {
        Optional<User> userOptional = Optional.ofNullable((User) request.getSession(false).getAttribute("user"));

        if(userOptional.isPresent()) {
            User user = userOptional.get();
            String email = user.getEmail();

            try {

                if(!userService.checkCorrectPassword(email, passwordChangeDTO)) {
                    bindingResult.rejectValue("currentPassword", "error.currentPassword", "Mot de passe incorrect");
                }

                if(bindingResult.hasErrors()) {
                    return getEditProfileModelView(user, model, null, passwordChangeDTO, false, redirectAttributes);
                }

                if(!userService.checkCorrectPassword(email, passwordChangeDTO)) {
                    bindingResult.rejectValue("currentPassword", "error.currentPassword", "Mot de passe incorrect");
                }

                user = userService.updatePassword(email, passwordChangeDTO);

                if(user != null){
                    request.getSession(false).setAttribute("user", user);
                    return getEditProfileModelView(user, model, null, passwordChangeDTO, true, redirectAttributes);
                } else {
                    return getEditProfileModelView(null, model, null, passwordChangeDTO, false, redirectAttributes);
                }
            } catch (UserNotFoundException e) {
                return getEditProfileModelView(null, model, null, passwordChangeDTO, false, redirectAttributes);
            }
        } else {
            return getEditProfileModelView(null, model, null, passwordChangeDTO, false, redirectAttributes);
        }
    }

    /**
     * Select the correct view to display depending on the context
     * @param user The user to consider
     * @param model The model data
     * @param userDetailsDTO The user details form
     * @param passwordChangeDTO The password change form
     * @param update If it is an update case
     * @param redirectAttributes The redirect attributes data
     * @return The correct view to display
     */
    private RedirectView getEditProfileModelView(
            User user,
            Model model,
            UserDetailsDTO userDetailsDTO,
            PasswordChangeDTO passwordChangeDTO,
            boolean update,
            RedirectAttributes redirectAttributes)
    {
        if(user == null) {
            return new RedirectView("/auth/login", true);
        }

        if((model.containsAttribute("user") &&
                model.getAttribute("user") == null) ||
                !model.containsAttribute("user")) {
            if(userDetailsDTO == null) {
                redirectAttributes.addFlashAttribute("user", userService.buildUserDetailsObject(user.getId()));
            } else {
                redirectAttributes.addFlashAttribute("user", userDetailsDTO);
            }
        } else {
            redirectAttributes.addFlashAttribute("user", model.getAttribute("user"));
        }

        if((model.containsAttribute("passwordChange") &&
                model.getAttribute("passwordChange") == null) ||
                !model.containsAttribute("passwordChange")) {
            if(passwordChangeDTO == null) {
                redirectAttributes.addFlashAttribute("passwordChange", userService.buildPasswordChangeObject());
            } else {
                redirectAttributes.addFlashAttribute("passwordChange", passwordChangeDTO);
            }
        } else {
            redirectAttributes.addFlashAttribute("passwordChange", model.getAttribute("passwordChange"));
        }

        redirectAttributes.addFlashAttribute("update", update);

        Map<String, Object> modelAsMap = model.asMap();

        for(String key: modelAsMap.keySet()) {
            if (!Arrays.asList("passwordChange", "user").contains(key)) {
                redirectAttributes.addFlashAttribute(key, modelAsMap.get(key));
            }
        }

        return new RedirectView("/user/edit", true);
    }
}
