/**
 * This Controller allows us with admin account to manage offers and accounts
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.controller;

import com.llanfair.responsiblefood.model.mongo.collection.Offer;
import com.llanfair.responsiblefood.model.mongo.collection.Role;
import com.llanfair.responsiblefood.model.mongo.collection.Transaction;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import com.llanfair.responsiblefood.service.OfferService;
import com.llanfair.responsiblefood.service.TagService;
import com.llanfair.responsiblefood.service.TransactionService;
import com.llanfair.responsiblefood.service.UserService;
import com.llanfair.responsiblefood.service.credentials.VerificationTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
@PreAuthorize("hasRole('admin')")
@SessionAttributes("name")
public class AdminController {

    @Autowired
    private UserService userService;

    @Autowired
    private OfferService offerService;

    @Autowired
    private VerificationTokenService verificationTokenService;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private TagService tagService;

    /**
     * Serve to show the admin home page with all the metrics
     * @param model
     * @return The page /admin/home
     */
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String home(ModelMap model){
        model.addAttribute("name", getLoggedInUserName());

        Map<String, Integer> mapUser = userService.countAllUser();
        model.addAttribute("mapUser", mapUser);

        int nbOffer = offerService.countAllOffer();
        model.addAttribute("nbOffer", nbOffer);

        int nbTag = tagService.countAllTags();
        model.addAttribute("nbTag", nbTag);

        //Map<String, Long> mostUsedTags = offerService.mapTagsOccurences();
        //model.addAttribute("mostUsedTags", mostUsedTags);

        return "/admin/home";
    }

    /**
     * Get the Username of the admin logged in
     * @return String
     */
    private String getLoggedInUserName() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();

        if (principal instanceof UserDetails)
            return ((UserDetails) principal).getUsername();

        return principal.toString();
    }

    /**
     * Serve to show the admin board page
     * @param model
     * @return The page /admin/board
     */
    @RequestMapping(value = "/board-admin", method = RequestMethod.GET)
    public String board(ModelMap model){
        return "/admin/board";
    }

    /**
     * Serve to show the admin manageOffers page, to manage the offers
     * @param model
     * @return The page /admin/manageOffers
     */
    @RequestMapping(value = "/board-admin/manageOffers", method = RequestMethod.GET)
    public String adminManageOffers(ModelMap model){
        model.addAttribute("focusedButton","manageOffers");

        List<Offer> arrayListOffer = offerService.getAllOffers();
        //List<Offer> arrayListOffer = new ArrayList<Offer>(); //Try with empty List
        model.addAttribute("arrayListOffer",arrayListOffer);

        return "/admin/manageOffers";
    }

    /**
     * Serve to show the admin manageAccounts page, to manage the accounts
     * @param model
     * @return The page /admin/manageAccounts
     */
    @RequestMapping(value = "/board-admin/manageAccounts", method = RequestMethod.GET)
    public String adminManageAccounts(ModelMap model){
        model.addAttribute("focusedButton","manageAccounts");

        List<User> arrayListUser = userService.getListUser();
        //List<User> arrayListUser = new ArrayList<User>(); //Try with empty List
        model.addAttribute("arrayListUser",arrayListUser);

        return "/admin/manageAccounts";
    }

    /**
     * Serve to delete an offer and its transactions associated
     * @param request
     * @return The page /admin/manageOffers
     */
    @RequestMapping(value = "/board-admin/manageOffers/removeOffer", method = RequestMethod.GET)
    public String producerDeleteOffer(HttpServletRequest request){
        String offer = request.getParameter("offerId");
        System.out.println("Offer : " + offer);

        if (offer == null){
            return "redirect:/board-admin/manageOffers";
        }
        transactionService.deleteAllTransactionByOffer(offerService.getOfferById(offer));
        offerService.removeOffer(offer);
        return "redirect:/board-admin/manageOffers/";
    }

    /**
     * Serve to delete an account, its offers, its transactions, its tokens
     * @param request
     * @return The page /admin/manageAccounts
     */
    @RequestMapping(value = "/board-admin/manageAccounts/removeAccount", method = RequestMethod.GET)
    public String producerDeleteAccount(HttpServletRequest request){
        String account = request.getParameter("AccountId");
        System.out.println("Account : " + account);

        if (account == null){
            return "redirect:/board-admin/manageAccounts";
        }

        User user = userService.getUserById(account);
        Role roleUser = user.getRoles().get(0);

        //Search if he has offers
        if (roleUser.getRole().equals("farmer")){
            List<Offer> offersAccount = offerService.getProducerOffers(user);

            for (Offer offer : offersAccount) {
                offerService.removeOffer(offer.getId()); //delete offer
            }
        }

        List<Transaction> transactionList = transactionService.getTransactionByBuyer(user);

        for (Transaction transaction : transactionList) {
            transactionService.removeTransaction(transaction);
        }

        //if the account has tokens
        verificationTokenService.deleteAllTokensWithUser(user);

        //delete account
        userService.removeUser(account);

        return "redirect:/board-admin/manageAccounts/";
    }

}
