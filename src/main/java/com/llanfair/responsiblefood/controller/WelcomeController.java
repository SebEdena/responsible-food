/**
 * This Controller is for the welcome page
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.llanfair.responsiblefood.model.mongo.repository.TagRepository;
import com.llanfair.responsiblefood.service.OfferService;
import com.llanfair.responsiblefood.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Controller
@PreAuthorize("permitAll()")
@SessionAttributes("name")
@Slf4j
public class WelcomeController {

    @Autowired
    private UserService userService;

    @Autowired
    private OfferService offerService;

    @Autowired
    private TagRepository tagRepository;

    /**
     * Function to go to the welcome page
     * @param model attributes storage for the view
     * @return welcome view
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showLoginPage(ModelMap model) {
        model.addAttribute("arrayListTags",tagRepository.findAll());
        model.addAttribute("name", getLoggedInUserName());
        model.addAttribute("offers", offerService.getOffersByExpirationDate(10));
        return "welcome";
    }

    /**
     * Getting the user session (not connected or specific role)
     * @return user session
     */
    private String getLoggedInUserName() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();

        if (principal instanceof UserDetails)
            return ((UserDetails) principal).getUsername();

        return principal.toString();
    }

    /**
     * Function to display General Condition of User (GCU)
     * @return content of GCU
     * @throws IOException
     */
    @RequestMapping(value = "/cgu", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getCGU() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(this.getClass().getResourceAsStream("/cgu/cgu.json").readAllBytes());
        return jsonNode.toString();
    }

    /**
     * Function to display Frequently Asked Questions (FAQ)
     * @return content of FAQ
     * @throws IOException
     */
    @RequestMapping(value = "/faq", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getFAQ() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(this.getClass().getResourceAsStream("/faq/faq.json").readAllBytes());
        return jsonNode.toString();
    }

    /**
     * Function to retreive producer list
     * @param model attributes storage for the view
     * @return view containing producers list
     */
    @RequestMapping(value = "/producers", method = RequestMethod.GET)
    public String getProducerList(ModelMap model){
        model.put("producers",userService.getListProducer());
        model.addAttribute("research","producteurs");
        model.addAttribute("name", getLoggedInUserName());
        return "results/listProducer";
    }
}
