/**
 * This Controller is for the Credentials
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.controller;

import com.llanfair.responsiblefood.model.bean.dto.EmailDTO;
import com.llanfair.responsiblefood.model.bean.dto.PasswordForgottenDTO;
import com.llanfair.responsiblefood.model.bean.dto.SignupDTO;
import com.llanfair.responsiblefood.model.event.OnRegistrationCompleteEvent;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import com.llanfair.responsiblefood.model.mongo.collection.VerificationToken;
import com.llanfair.responsiblefood.service.UserService;
import com.llanfair.responsiblefood.service.credentials.AccountActivationService;
import com.llanfair.responsiblefood.service.credentials.PasswordChangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(value = "/auth")
public class CredentialsController {

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private AccountActivationService accountActivationService;

    @Autowired
    private PasswordChangeService passwordChangeService;

    /**
     * This function allows us to return the login page
     * @param request instance that call the function
     * @param response instance that we return to the next view
     * @return is the path of the view that the application will display
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(HttpServletRequest request,
                         HttpServletResponse response) {
        if(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken) {
            return "auth/login";
        } else {
            return "redirect:/";
        }
    }

    /**
     * This function allows us to return the signup page
     * @param modelMap is the object that contains all values in the session
     * @return is the path of the view that the application will display
     */
    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String signup(ModelMap modelMap) {
        SignupDTO signupDTO = new SignupDTO();
        modelMap.addAttribute("user", signupDTO);
        return "auth/signup/signup";
    }

    /**
     * This function allows us to register a new user
     * @param request instance that call the function
     * @param signupDTO is the object that has all parameters for the sign up of a user
     * @param bindingResult is the object that will bind with a validator (check if all values are ok)
     * @param modelMap is the object that contains all values in the session
     * @return is the path of the view that the application will display
     */
    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ModelAndView registerNewUser(
            HttpServletRequest request,
            @ModelAttribute("user") @Valid SignupDTO signupDTO,
            BindingResult bindingResult,
            ModelMap modelMap) {

        if(bindingResult.hasErrors()) {
            return new ModelAndView("/auth/signup/signup");
        } else {
            User user = userService.registerNewUserAccount(signupDTO);

            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(user,
                    request.getLocale(), request.getContextPath()));

            modelMap.addAttribute("email", user.getEmail());
            return new ModelAndView("redirect:/auth/signup-confirm", modelMap);
        }
    }

    /**
     * This function allows us to confirm the signup
     * @param email is the email used by the user
     * @param resend if resended
     * @param modelMap is the object that contains all values in the session
     * @return is the path of the view that the application will display
     */
    @RequestMapping(value = "/signup-confirm", method = RequestMethod.GET)
    public String signupConfirm(@ModelAttribute("email") String email, @ModelAttribute("resend") String resend, ModelMap modelMap) {
        modelMap.addAttribute("email", email);
        if(resend != null) modelMap.addAttribute("resend", resend);
        return "auth/signup/signup-confirm";
    }

    /**
     * This function allows us to return a error view for the signup
     * @param modelMap is the object that contains all values in the session
     * @param message is the message to display to the user
     * @return is the path of the view that the application will display
     */
    @RequestMapping(value = "/signup-error", method = RequestMethod.GET)
    public String signupError(ModelMap modelMap, @ModelAttribute("message") String message) {
        modelMap.addAttribute("message", message);
        return "auth/signup/signup-error";
    }

    /**
     * This function will return the view that the account is enabled
     * @return is the path of the view that the application will display
     */
    @RequestMapping(value = "/signup-enabled-account", method = RequestMethod.GET)
    public String signupEnabledAccount() {
        return "auth/signup/signup-enabled-account";
    }

    /**
     * This function allows us to control if the user has validate an token for the password forgotten
     * @param token is the token used
     * @param redirectAttributes allows us to have variables stored shortly
     * @return is the path of the view that the application will display
     */
    @RequestMapping(value = "/token-validate", method = RequestMethod.GET)
    public RedirectView validateToken(
            @RequestParam("token") String token,
            RedirectAttributes redirectAttributes) {

        VerificationToken verificationToken = accountActivationService.getVerificationToken(token);
        if (verificationToken == null) {
            redirectAttributes.addFlashAttribute("message", "Le lien choisi est invalide.");

            return new RedirectView("/auth/signup-error");
        }

        User user = verificationToken.getUser();
        if (accountActivationService.isPasswordExpired(verificationToken)) {
            redirectAttributes.addFlashAttribute("message", "Le lien de confirmation a expiré.");
            redirectAttributes.addFlashAttribute("token", verificationToken.getToken());

            return new RedirectView("/auth/signup-error");
        }

        accountActivationService.enableUser(user);
        return new RedirectView("/auth/signup-enabled-account");
    }

    /**
     * This function allows us to resend a token for a user
     * @param model is the object that contains all values in the session
     * @param token is the token
     * @param redirectAttributes allows us to have variables stored shortly
     * @return is the path of the view that the application will display
     */
    @RequestMapping(value = "/resend-token", method = RequestMethod.GET)
    public RedirectView resendToken (
            ModelMap model,
            @RequestParam("token") Optional<String> token,
            RedirectAttributes redirectAttributes) {

        if(token.isEmpty()) {
            return new RedirectView("/auth/resend-token-email");
        } else {
            VerificationToken verificationToken = accountActivationService.generateNewAccountActivationToken(token.get());

            if(verificationToken != null) {
                model.addAttribute("email", verificationToken.getUser().getEmail());
                model.addAttribute("resend", "true");
                return new RedirectView("/auth/signup-confirm");
            } else {
                redirectAttributes.addFlashAttribute("message", "Ce token n'existe pas");
                return new RedirectView("/auth/signup-error");
            }
        }
    }

    /**
     * This function allows us to get the mail
     * @param model is the object that contains all values in the session
     * @return is the path of the view that the application will display
     */
    @RequestMapping(value = "/resend-token-email", method = RequestMethod.GET)
    public String resendLinkGetEmail (ModelMap model) {

        EmailDTO emailDTO = new EmailDTO();
        model.addAttribute("email", emailDTO);
        return "auth/signup/signup-resend-token-email";
    }

    /**
     * This function allows us to resend a token for a user
     * @param model is the object that contains all values in the session
     * @return is the path of the view that the application will display
     */
    @RequestMapping(value = "/resend-token-email", method = RequestMethod.POST)
    public ModelAndView resendLinkSendWithEmail (
            ModelMap model,
            @Valid @ModelAttribute("email") EmailDTO emailDTO,
            BindingResult bindingResult) {

        if(bindingResult.hasErrors()) {
            return new ModelAndView("/auth/resend-token-email", model);
        } else {
            VerificationToken token = accountActivationService.createNewAccountActivationTokenFromEmail(emailDTO.getEmail());
            if(token == null) {
                model.addAttribute("message", "Unable to send the new token.");
                return new ModelAndView("redirect:/auth/signup-error", model);
            } else {
                model.addAttribute("email", emailDTO.getEmail());
                model.addAttribute("resend", true);
                return new ModelAndView("redirect:/auth/signup-confirm", model);
            }
        }
    }

    /**
     * This function allows us to logout the user
     * @param request instance that call the function
     * @param response instance that we return to the next view
     * @return is the path of the view that the application will display
     */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request,
                         HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/";
    }

    /**
     * This function allows us to control the forgotten password request
     * @param mail is the mail used to retrieve the password
     * @param model is the object that contains all values in the session
     * @return is the path of the view that the application will display
     */
    @RequestMapping(value = "/forgotPassword", method = RequestMethod.POST)
    public String forgotPassword(@RequestParam("mailForgotten") String mail, ModelMap model) {
        try{
            passwordChangeService.tokenResetPassword(mail);
            model.addAttribute("email", mail);
            return "auth/forgotPassword/forgot-password-sended";
        }catch (Exception u){
            model.addAttribute("message", "Utilisateur inconnu pour : " + mail);
            return "auth/forgotPassword/forgot-password-rejected";
        }
    }

    /**
     * This function allows us to control the token used for the forgotten password request
     * @param token is the token used
     * @param model is the object that contains all values in the session
     * @return is the path of the view that the application will display
     */
    @RequestMapping(value = "/token-password", method = RequestMethod.GET)
    public String checkingTokenForgottenPassword(@RequestParam("token") String token, ModelMap model){
        VerificationToken verificationToken = passwordChangeService.getVerificationToken(token);
        if(verificationToken != null && !passwordChangeService.isPasswordExpired(verificationToken)){
            PasswordForgottenDTO passwordForgottenDTO = new PasswordForgottenDTO();
            model.addAttribute("token", token);
            model.addAttribute("passwordForgottenDTO", passwordForgottenDTO);
            return "auth/forgotPassword/renew-password";
        }else{
            model.addAttribute("message", "Le lien du token est mauvais ou périmé : " + token);
            return "auth/forgotPassword/forgot-password-rejected";
        }
    }

    /**
     * This function allows us to control the change of the password
     * @param token is the token used for the password forgotten
     * @param passwordForgottenDTO is the object used for the forgotten password
     * @param bindingResult is the object that will bind with a validator (check if all values are ok)
     * @param model is the object that contains all values in the session
     * @return is the path of the view that the application will display
     */
    @RequestMapping(value = "/renew-password", method = RequestMethod.POST)
    public ModelAndView renewPassword(
            @RequestParam("token") String token,
            @Valid @ModelAttribute("passwordForgottenDTO") PasswordForgottenDTO passwordForgottenDTO,
            BindingResult bindingResult,
            ModelMap model)
    {
        if(!bindingResult.hasErrors()){
            VerificationToken verificationToken = passwordChangeService.getVerificationToken(token);
            if(verificationToken != null) {
                User user = verificationToken.getUser();
                passwordChangeService.deleteAllTokensWithUser(user);
                userService.changePassword(user, passwordForgottenDTO.getPassword(), passwordForgottenDTO.getPasswordConfirmation());
                return new ModelAndView("auth/forgotPassword/validate-new-password", model);
            }else {
                model.addAttribute("message", "Token invalide");
                return new ModelAndView( "auth/forgotPassword/forgot-password-rejected", model);
            }
        }else{
            model.addAttribute("token", token);
            model.addAttribute("passwordDTO", passwordForgottenDTO);
            return new ModelAndView( "auth/forgotPassword/renew-password", model);
        }
    }
}
