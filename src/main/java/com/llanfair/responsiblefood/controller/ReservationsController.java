/**
 * This Controller is for Reservation
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.controller;

import com.llanfair.responsiblefood.model.mongo.collection.User;
import com.llanfair.responsiblefood.service.OfferService;
import com.llanfair.responsiblefood.service.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller
@PreAuthorize("hasRole('client')")
public class ReservationsController {

    @Autowired
    private OfferService offerService;

    @Autowired
    private TransactionService transactionService;

    /**
     * This function allows us to get the reservation of the client
     * @param request instance that call the function
     * @param response instance that we return to the next view
     * @param modelMap is the object that contains all values in the session
     * @return is the path of the view that the application will display
     */
    @RequestMapping(value = "/reservations", method = RequestMethod.GET)
    public String reservations(HttpServletRequest request,
                               HttpServletResponse response,
                               ModelMap modelMap) {
        modelMap.addAttribute("arrayListTransaction",transactionService.getTransactionByBuyer((User) request.getSession(false).getAttribute("user")));
        return "reservationsForClient";
    }

}
