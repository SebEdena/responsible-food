package com.llanfair.responsiblefood.controller;

import com.llanfair.responsiblefood.model.bean.dto.OfferDTO;
import com.llanfair.responsiblefood.model.mongo.collection.Offer;
import com.llanfair.responsiblefood.model.mongo.collection.Transaction;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import com.llanfair.responsiblefood.model.mongo.repository.TagRepository;
import com.llanfair.responsiblefood.model.mongo.repository.UnitRepository;
import com.llanfair.responsiblefood.service.OfferService;
import com.llanfair.responsiblefood.service.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Slf4j
@Controller
@PreAuthorize("hasRole('farmer')")
public class MyBoardController {

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private UnitRepository unitRepository;

    @Autowired
    private OfferService offerService;

    @Autowired
    private TransactionService transactionService;

    /**
     * used to redirect when getting on /myBoard
     * @return
     */
    @RequestMapping(value = "/myBoard", method = RequestMethod.GET)
    public String producerDefaultBoard() {
        return "redirect:/myBoard/myOffers";
    }

    /**
     * used to set up all data needed in order to create an offer
     * @param request
     * @param response
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/myBoard/createOffer", method = RequestMethod.GET)
    public String createOfferView(HttpServletRequest request,
                                      HttpServletResponse response,
                                      ModelMap modelMap) {
        modelMap.addAttribute("focusedButton","createOffer");
        OfferDTO offerDTO = new OfferDTO();
        int nbMilliSecondInADay = 1000 * 60 * 60 * 24;
        Date tomorrow = new Date((new Date()).getTime() + nbMilliSecondInADay);

        User user = (User) request.getSession(false).getAttribute("user");
        String fullAddress = user.getAddress()+" "+user.getZipCode()+" "+user.getCity();
        offerDTO.setAddress(fullAddress);
        modelMap.addAttribute("tomorrowDate",new SimpleDateFormat("yyyy-MM-dd").format(tomorrow)); //haven't worked: LocalDate futureDate = LocalDate.now().plusMonths(1);
        modelMap.addAttribute("offerDTO", offerDTO);
        modelMap.addAttribute("arrayListUnit",unitRepository.findAll());
        modelMap.addAttribute("arrayListTags",tagRepository.findAll());
        return "createOffer";
    }

    /**
     * used to retrieve all informations needed to create a new offer and save it in the data base
     * @param request
     * @param response
     * @param offerDTO
     * @param bindingResult
     * @param modelMap
     * @return
     * @throws ParseException
     * @throws IOException
     */
    @RequestMapping(value = "/myBoard/createOffer", method = RequestMethod.POST)
    public String createOfferPost(HttpServletRequest request,
                                    HttpServletResponse response,
                                    @Valid @ModelAttribute("offerDTO") OfferDTO offerDTO,
                                    BindingResult bindingResult,
                                    ModelMap modelMap
    ) throws ParseException, IOException {
        if(bindingResult.hasErrors()) {
            modelMap.addAttribute("focusedButton","createOffer");
            modelMap.addAttribute("arrayListUnit",unitRepository.findAll());
            modelMap.addAttribute("arrayListTags",tagRepository.findAll());
            return "createOffer";
        } else {
            User user = (User) request.getSession(false).getAttribute("user");
            offerService.registerNewOffer(offerDTO,user);
            return "redirect:/myBoard/myOffers";
        }
    }

    /**
     * used to set up all the information regarding the offers of the connected producer
     * @param request
     * @param response
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/myBoard/myOffers", method = RequestMethod.GET)
    public String myOffersView(HttpServletRequest request,
                                   HttpServletResponse response,
                                   ModelMap modelMap) {
        modelMap.addAttribute("focusedButton","myOffers");

        List<Offer> arrayListOffer = offerService.getProducerOffers((User) request.getSession(false).getAttribute("user"));
        Collections.reverse(arrayListOffer);
        modelMap.addAttribute("todayDate",new Date());
        modelMap.addAttribute("arrayListOffer",arrayListOffer );
        return "myOffers";
    }

    /**
     * used to remove an offer using id in the parameter and delete all transactions using this offer
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/myBoard/removeMyOffer", method = RequestMethod.GET)
    public String producerDeleteMyOffer(HttpServletRequest request,
                                        HttpServletResponse response){
        if (request.getParameter("offerId")==null){
            return "redirect:/myBoard/myOffers";
        }
        User user = (User) request.getSession(false).getAttribute("user");
        String offerId= request.getParameter("offerId");

        transactionService.deleteAllTransactionByOffer(offerService.getOfferById(offerId));
        offerService.removeOffer(offerId,user);
        return "redirect:/myBoard/myOffers";
    }

    /**
     * set up all data needed to update an offer
     * @param request
     * @param response
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/myBoard/updateOffer", method = RequestMethod.GET)
    public String producerUpdateOfferView(HttpServletRequest request,
                                      HttpServletResponse response,
                                      ModelMap modelMap) {
        if (request.getParameter("offerId")==null){
            return "redirect:/myBoard/myOffers";
        }
        String offerId = request.getParameter("offerId");
        OfferDTO offerDTO = new OfferDTO(offerService.getOfferById(offerId));
        int nbMilliSecondInADay = 1000 * 60 * 60 * 24;
        Date tomorrow = new Date((new Date()).getTime() + nbMilliSecondInADay);
        modelMap.addAttribute("focusedButton","myOffers");
        modelMap.addAttribute("tomorrowDate",new SimpleDateFormat("yyyy-MM-dd").format(tomorrow)); //haven't worked: LocalDate futureDate = LocalDate.now().plusMonths(1);
        modelMap.addAttribute("offerDTO",offerDTO);
        modelMap.addAttribute("arrayListUnit",unitRepository.findAll());
        modelMap.addAttribute("arrayListTags",tagRepository.findAll());
        modelMap.addAttribute("offerId",offerId);
        return "updateOffer";
    }

    /**
     * used to retrieve all the informations needed in order to update an offer and save it in the data base
     * @param request
     * @param response
     * @param offerDTO
     * @param bindingResult
     * @param modelMap
     * @return
     * @throws ParseException
     * @throws IOException
     */
    @RequestMapping(value = "/myBoard/updateOffer", method = RequestMethod.POST)
    public String producerUpdateOfferPost(HttpServletRequest request,
                                          HttpServletResponse response,
                                          @Valid @ModelAttribute("offerDTO") OfferDTO offerDTO,
                                          BindingResult bindingResult,
                                          ModelMap modelMap
    ) throws ParseException, IOException {
        if(bindingResult.hasErrors()) {
            int nbMilliSecondInADay = 1000 * 60 * 60 * 24;
            Date tomorrow = new Date((new Date()).getTime() + nbMilliSecondInADay);
            String offerId = request.getParameter("offerId");
            modelMap.addAttribute("focusedButton","myOffers");
            modelMap.addAttribute("tomorrowDate",new SimpleDateFormat("yyyy-MM-dd").format(tomorrow)); //haven't worked: LocalDate futureDate = LocalDate.now().plusMonths(1);
            modelMap.addAttribute("offerDTO",offerDTO);
            modelMap.addAttribute("arrayListUnit",unitRepository.findAll());
            modelMap.addAttribute("arrayListTags",tagRepository.findAll());
            modelMap.addAttribute("offerId",offerId);
            return "updateOffer";
        } else {
            if (request.getParameter("offerId")==null){
                return "redirect:/myBoard/myOffers";
            }
            offerService.updateOffer(offerDTO,
                    request.getParameter("offerId"),
                    (User) request.getSession(false).getAttribute("user") );
            return "redirect:/myBoard/myOffers";
        }
    }

    /**
     * set up all the information needed to see the current transactions in which the owner is involved
     * @param request
     * @param response
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/myBoard/reservations", method = RequestMethod.GET)
    public String producerReservations(HttpServletRequest request,
                                   HttpServletResponse response,
                                   ModelMap modelMap) {
        modelMap.addAttribute("focusedButton","reservations");

        User user =(User) request.getSession(false).getAttribute("user");
        List<Transaction> arrayListTransaction = transactionService.getReservedTransaction(user);

        modelMap.addAttribute("arrayListTransaction",arrayListTransaction );
        return "reservationsForProducers";
    }

    /**
     * used to unvalidate and remove from the data base a transaction reserved by a client
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/myBoard/cancelReservation", method = RequestMethod.GET)
    public String producerCancelReservations(HttpServletRequest request,
                                       HttpServletResponse response) {
        transactionService.cancelTransaction(request.getParameter("transactionId"),
                (User) request.getSession(false).getAttribute("user"));
        return "redirect:/myBoard/reservations";
    }

    /**
     * used to validate and update in the data base a transaction reserved by a client
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/myBoard/validReservation", method = RequestMethod.GET)
    public String producerValidReservations(HttpServletRequest request,
                                             HttpServletResponse response) {
        transactionService.validateTransaction(request.getParameter("transactionId"),
                (User) request.getSession(false).getAttribute("user"));

        return "redirect:/myBoard/reservations";
    }



}
