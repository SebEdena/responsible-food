/**
 * This Controller is for image handling
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.controller;

import com.llanfair.responsiblefood.model.bean.image.DefaultImageEnum;
import com.llanfair.responsiblefood.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/images")
public class ImageController {

    @Autowired
    private ImageService imageService;

    /**
     * Get the image associated to its type and id
     * @param type The type of image used for default image if not found
     * @param id The id of the image
     * @return The image data as bytes
     */
    @GetMapping(
        value = "/{type}/{id}",
        produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE}
    )
    @ResponseBody
    public byte[] getImage (
        @PathVariable("type") String type,
        @PathVariable("id") String id
    ) {
        DefaultImageEnum defaultImage = DefaultImageEnum.getByNameOrDefault(type);
        return imageService.getImageOrDefault(id, defaultImage);
    }

    /**
     * Return the default image for the specified type
     * @param type person, offer or default
     * @return The image data as bytes
     */
    @GetMapping(
        value = "/{type}",
        produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE}
    )
    @ResponseBody
    public byte[] getDefaultImage (
        @PathVariable("type") String type
    ) {
        DefaultImageEnum defaultImageEnum = DefaultImageEnum.getByNameOrDefault(type);
        return imageService.getDefaultImage(defaultImageEnum);
    }

/*   /**
     * Store an image
     * @param type The type of image
     * @param id The id of the previous image
     * @param image The image data
     * @return A confirmation json object as string
     * @throws IOException
     *//*
    @PostMapping(
        value = "/add/{type}/{id}",
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = { MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE },
        headers=("content-type=multipart/form-data")
    )
    public ResponseEntity<String> addImage(
        @PathVariable("type") String type,
        @PathVariable("id") String id,
        @RequestParam(value = "image") MultipartFile image
    ) throws IOException {
        String error = imageService.validateFile(image);

        if(error != null) {
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        } else {
            DefaultImageEnum defaultImageEnum = DefaultImageEnum.getByNameOrDefault(type);
            imageService.updateImage(defaultImageEnum, id, image);
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }*/

}
