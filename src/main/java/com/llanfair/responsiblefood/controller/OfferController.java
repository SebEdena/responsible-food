/**
 * This Controller is for offers
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.controller;

import com.llanfair.responsiblefood.service.OfferService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Controller
@PreAuthorize("permitAll()")
@SessionAttributes({"basket", "name"})
public class OfferController {

    @Autowired
    private OfferService offerService;

    /**
     * Getting the user session (not connected or specific role)
     * @return user session
     */
    private String getLoggedInUserName() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();

        if (principal instanceof UserDetails)
            return ((UserDetails) principal).getUsername();

        return principal.toString();
    }

    /**
     * Function to display a specific offer details
     * @param id id of the offer
     * @param modelMap attributes storage for the view
     * @return offer view containing additional details
     */
    @RequestMapping(value = "/offers/{id}", method = RequestMethod.GET)
    public String getOfferDetailbyId(@PathVariable() String id, ModelMap modelMap){
        // preventing null for the id -> redirect to the welcom page
        if(id == null){
            return "redirect:/";
        } else {
            modelMap.put("offer", offerService.getOfferById(id));
            modelMap.addAttribute("name", getLoggedInUserName());
            return "detailedOffer";
        }
    }
}
