/**
 * This Controller is for the research
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.controller;

import com.llanfair.responsiblefood.model.mongo.collection.Offer;
import com.llanfair.responsiblefood.service.OfferService;
import com.llanfair.responsiblefood.service.TagService;
import com.llanfair.responsiblefood.service.UserService;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@PreAuthorize("permitAll()")
@SessionAttributes("name")
@RequestMapping(value="/search")
public class SearchController {

    @Autowired
    private OfferService offerService;

    @Autowired
    private UserService userService;

    @Autowired
    private TagService tagService;

    /**
     * Getting the user session (not connected or specific role)
     * @return user session
     */
    private String getLoggedInUserName() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();

        if (principal instanceof UserDetails)
            return ((UserDetails) principal).getUsername();

        return principal.toString();
    }

    /**
     * Function to retreive all available offers
     * @param modelMap attributes storage for the view
     * @return view containing all offers available
     */
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public String getAllOffers(ModelMap modelMap){
        modelMap.put("offers", offerService.getOffersByExpirationDate(null));
        modelMap.addAttribute("arrayListTags",tagService.findAllTags());
        modelMap.addAttribute("name", getLoggedInUserName());
        modelMap.put("research","offres");
        return "results/resultOffers";
    }

    /**
     * Function to retreive all available offers of a specific product family
     * @param family family tag of offers
     * @param modelMap attributes storage for the view
     * @return view containing all available offers of the product family
     */
    @RequestMapping(value = "/family/{family}", method = RequestMethod.GET)
    public String getOffersByFamily(@PathVariable() String family, ModelMap modelMap){
        modelMap.put("offers", offerService.getOffersByFamily(family));
        modelMap.addAttribute("arrayListTags",tagService.findAllTags());
        modelMap.addAttribute("name", getLoggedInUserName());
        modelMap.put("research",family);
        return "results/resultOffers";
    }

    /**
     * Function to retreive result frm the search bar
     * @param type type of the research (by tags, products, producers, producer offer)
     * @param tags list of tags (not required)
     * @param params string of the research (not required)
     * @param modelMap attributes storage for the view
     * @return adequate view depending on the research
     */
    @RequestMapping(value = "/bar", method = RequestMethod.GET)
    public String getResultFromSearchBar(@RequestParam(value="type", required = false) String type,
                                         @RequestParam(value="tags",required = false) List<String> tags,
                                         @RequestParam(value="rsh", required = false) String params,
                                         ModelMap modelMap){
        modelMap.addAttribute("name", getLoggedInUserName());
        // Preventing null for type -> return to the welcom page
        if(type == null)
            type = "";
        switch (type){
            // research by tags
            case "tags":
                modelMap.addAttribute("arrayListTags",tagService.findAllTags());
                modelMap.addAttribute("offers", offerService.getOffersByTags(tags));
                // Standarized research text in string
                String research = tags.toString()
                        .replace(",", ", ")
                        .replace("[","")
                        .replace("]","");
                modelMap.addAttribute("research",research);
                return "results/resultOffers";
            // research producer's offer (search bar for producer research)
            case "byProducersFromProd":
                modelMap.addAttribute("arrayListTags",tagService.findAllTags());
                List<User> prods = userService.getProducersFromResearch(params);
                // for each producer, get its offers
                Map<User, List<Offer>> offersByProd = new HashMap<>();
                for (User p : prods) {
                    offersByProd.put(p,offerService.getProducerOffersForClient(p));
                }
                modelMap.addAttribute("offersProd",offersByProd);
                modelMap.addAttribute("research",params);
                return "results/resultOffersProd";
            // research producer's offer (search bar for offer research)
            case "byProducersFromOffers":
                modelMap.addAttribute("arrayListTags",tagService.findAllTags());
                List<User> prodsFromOffer = userService.getProducersFromResearch(params);
                Map<User, List<Offer>> offersByProdFromOffer = new HashMap<>();
                // for each producer, get its offers
                for (User p : prodsFromOffer) {
                    offersByProdFromOffer.put(p,offerService.getProducerOffersForClient(p));
                }
                modelMap.addAttribute("offersProd",offersByProdFromOffer);
                modelMap.addAttribute("research",params);
                return "results/resultOffersProducer";
            // research producers
            case "prod":
                modelMap.addAttribute("producers",userService.getProducersFromResearch(params));
                modelMap.addAttribute("research",params);
                return "results/listProducer";
            // research offers by title or description
            case "offers" :
                modelMap.addAttribute("arrayListTags",tagService.findAllTags());
                modelMap.addAttribute("offers", offerService.getOffersByTitleOrDescription(params));
                modelMap.addAttribute("research",params);
                return "results/resultOffers";
            default: return "redirect:/";
        }
    }
}
