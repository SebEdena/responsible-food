/**
 * Mongo configuration class to interact with the database
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.config.mongodb;

import com.mongodb.*;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = "com.llanfair.responsiblefood")
@PropertySource("classpath:/mongo.properties")
public class MongoConfig extends AbstractMongoClientConfiguration {

    @Value("${mongodb.db}")
    private String db;

    @Value("${mongodb.username}")
    private String user;

    @Value("${mongodb.password}")
    private String password;

    @Value("${mongodb.host}")
    private String host;

    /**
     * Get the mongo client from the settings
     * @return The mongo client
     */
    @Override
    public MongoClient mongoClient() {
        return MongoClients.create(getSettings());
    }

    /**
     * Get the database name from the settings
     * @return The database name
     */
    @Override
    protected String getDatabaseName() {
        return db;
    }

    /**
     * Disable automatic index creation
     * @return The boolean value, false in this case
     */
    @Override
    protected boolean autoIndexCreation() {
        return false;
    }

    /**
     * Get the settings object for the configuration of the Mongo Client
     * @return The settings object
     */
    private MongoClientSettings getSettings() {
        return MongoClientSettings
                .builder()
                .applyConnectionString(
                        new ConnectionString(
                                String.format("mongodb+srv://%s:%s@%s/%s",
                                        user,
                                        password,
                                        host,
                                        getDatabaseName()
                                )
                        )
                )
                .retryWrites(true)
                .writeConcern(WriteConcern.MAJORITY)
                .build();
    }

}
