/**
 * Cookie utilitary class
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.config.cookie;

import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Optional;

@Component
public class CookieUtils {

    /**
     * Get a cookie from a request
     * @param request The request to get the cookie from
     * @param cookieName The name of the cookie
     * @return An optional object containing the cookie or nothing if not found
     */
    public Optional<Cookie> getCookieOptional(HttpServletRequest request, String cookieName) {
        if(request.getCookies() == null) return Optional.empty();

        return Arrays.stream(request.getCookies())
                     .filter(cookie -> cookie.getName().equals(cookieName))
                     .findFirst();
    }

    /**
     * Add a cookie to a response object
     * @param response The response where you put the cookie
     * @param cookieName The name of the cookie
     * @param cookieValue Its value
     */
    public void addCookie(HttpServletResponse response, String cookieName, String cookieValue) {
        Cookie c = new Cookie(cookieName, cookieValue);
        c.setSecure(true);
        c.setHttpOnly(true);
        c.setPath("/");
        response.addCookie(c);
    }
}
