/**
 * Class to check if the request is valid for storing into a cookie the previous page
 * Because you dont want to go back to static content or login form / logout
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.config.security;

import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

@Component
public class AcceptedRequestChecker {

    private final String[] excludedPaths = {
            "/auth/",
            "/webjars/",
            "/images/",
            "favicon",
            "/js/",
            "/css/",
            "/img/",
            "/cgu",
            "/faq"
    };

    /**
     * Checks if the request can be store in the previous page cookie
     * @param httpServletRequest The request to check
     * @return true if the request is valid for storage
     */
    public boolean isAcceptedRequest(HttpServletRequest httpServletRequest) {
        boolean isGet = httpServletRequest.getMethod().equalsIgnoreCase("get");
        boolean rejected = Arrays.stream(excludedPaths)
                .map(s -> httpServletRequest.getRequestURL().toString().contains(s))
                .reduce(false, (a, b) -> a || b);

        return !rejected && isGet;
    }
}
