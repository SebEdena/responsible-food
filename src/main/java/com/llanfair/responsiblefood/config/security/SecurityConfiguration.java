/**
 * Configure Spring Security
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.config.security;

import com.llanfair.responsiblefood.config.cookie.CookieUtils;
import com.llanfair.responsiblefood.config.security.handler.*;
import com.llanfair.responsiblefood.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private AuthenticationSuccessHandlerImpl authenticationSuccessHandlerImpl;

    @Autowired
    private AuthenticationFailureHandler authenticationFailureHandler;

    @Autowired
    private RenewSessionLogoutHandler renewSessionLogout;

    @Autowired
    private InvalidSessionStrategyHandler invalidSessionStrategyHandler;

    @Autowired
    private CookieUtils cookieUtils;

    /**
     * Get the service that will return the user details
     * @return The user details service
     * @see UserService
     */
    @Bean
    public UserDetailsService mongoUserDetails() {
        return new UserService();
    }

    /**
     * Configure the role mapping between Mongo and Spring Security
     * @return GrantedAuthorityDefaults object
     */
    @Bean
    public GrantedAuthorityDefaults grantedAuthorityDefaults() {
        return new GrantedAuthorityDefaults("");
    }

    /**
     * Configure the authentication side
     * @param auth AuthenticationManagerBuilder object
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        UserDetailsService userDetailsService = mongoUserDetails();
        auth
                .userDetailsService(userDetailsService)
                .passwordEncoder(bCryptPasswordEncoder);
    }

    /**
     * Configure the web security side
     * @param web The WebSecurity object
     */
    @Override
    public void configure(WebSecurity web) {
        web
                .ignoring()
                .antMatchers("/webjars/**", "/css/**", "/js/**", "/img/**");
    }

    /**
     * Configure HttpSecurity object
     * @param http The HttpSecurity object
     * @throws Exception
     */
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
                    .authorizeRequests()
                .and()
                    .formLogin()
                    .loginPage("/auth/login")
                    .usernameParameter("username")
                    .passwordParameter("password")
                    .failureHandler(authenticationFailureHandler)
                    .successHandler(authenticationSuccessHandlerImpl)
                    .permitAll()
                .and()
                    .logout()
                    .logoutRequestMatcher(new AntPathRequestMatcher("/auth/logout"))
                    .addLogoutHandler(renewSessionLogout)
                    .logoutSuccessUrl("/")
                    .invalidateHttpSession(false)
                    .permitAll()
                .and()
                    .csrf()
                    .disable();

        http        .sessionManagement()
                    .sessionFixation()
                    .migrateSession()
                    .maximumSessions(1)
                .and()
                    .invalidSessionStrategy(invalidSessionStrategyHandler);
    }
}