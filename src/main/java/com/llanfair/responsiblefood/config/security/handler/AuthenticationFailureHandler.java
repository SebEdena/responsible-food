/**
 * Authentication handler class triggered after a failed login
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.config.security.handler;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Component
public class AuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    /**
     * The method that is executed after a failed login
     * @param request The request object
     * @param response The response object
     * @param exception Tht exception related to the failure
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        HttpSession session = request.getSession();
        setDefaultFailureUrl("/auth/login?status=loginError");
        super.onAuthenticationFailure(request, response, exception);

        String errorMessage = "Adresse email ou mot de passe invalide.";

        if (exception.getMessage().equalsIgnoreCase("User is disabled")) {
            errorMessage = "Le compte n'est pas validé.";
        } else if (exception.getMessage().equalsIgnoreCase("User account has expired")) {
            errorMessage = "Le compte n'est pas validé.";
        }

        session.setAttribute(WebAttributes.AUTHENTICATION_EXCEPTION, errorMessage);
    }
}
