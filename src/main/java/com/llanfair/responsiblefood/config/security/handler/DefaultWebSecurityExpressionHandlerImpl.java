/**
 * Class to configure the mapping of the roles between Mongo and Spring Security
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.config.security.handler;

import org.springframework.security.access.expression.SecurityExpressionOperations;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.access.expression.WebSecurityExpressionRoot;
import org.springframework.stereotype.Component;

@Component
public class DefaultWebSecurityExpressionHandlerImpl extends DefaultWebSecurityExpressionHandler {

    /**
     * Returns the object used for role-based access control
     * @param authentication The authentication object
     * @param fi The filter invocation context
     * @return The control object
     */
    @Override
    protected SecurityExpressionOperations createSecurityExpressionRoot(Authentication authentication, FilterInvocation fi) {
        WebSecurityExpressionRoot root = (WebSecurityExpressionRoot) super.createSecurityExpressionRoot(authentication, fi);
        root.setDefaultRolePrefix("");
        return root;
    }

}
