/**
 * Logout handler class
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.config.security.handler;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Optional;

@Component
public class RenewSessionLogoutHandler implements LogoutHandler {

    /**
     * Logout handler
     * @param httpServletRequest The request
     * @param httpServletResponse The response
     * @param authentication The authentication object
     */
    @Override
    public void logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) {
        Optional<HttpSession> sessionOptional = Optional.ofNullable(httpServletRequest.getSession(false));

        if(sessionOptional.isPresent()) {
            HttpSession session = sessionOptional.get();
            session.removeAttribute("email");
            session.removeAttribute("roles");
            session.invalidate();
        }

        httpServletRequest.getSession(true);
    }

}
