/**
 * Authentication handler class triggered after a successful login
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.config.security.handler;

import com.llanfair.responsiblefood.config.cookie.CookieUtils;
import com.llanfair.responsiblefood.model.mongo.collection.User;
import com.llanfair.responsiblefood.model.mongo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Component
public class AuthenticationSuccessHandlerImpl
        extends SavedRequestAwareAuthenticationSuccessHandler {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CookieUtils cookieUtils;

    /**
     * Disable the use of the referer header
     */
    @PostConstruct
    public void useReferer() {
        setAlwaysUseDefaultTargetUrl(false);
    }

    /**
     * The method that is executed after a successful login
     * @param request The request object
     * @param response The response object
     * @param authentication The authentication object containing user details
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response, Authentication authentication)
            throws ServletException, IOException {

        Optional<Cookie> cookieOptional = cookieUtils.getCookieOptional(request, "RF_PREV_PAGE");
        cookieUtils.addCookie(response, "anonymous", "false");
        request.getSession(false).setAttribute("email", getLoggedInEmail());
        request.getSession(false).setAttribute("roles", getLoggedInRole());

        User user = getUser(getLoggedInEmail());
        request.getSession(false).setAttribute("user", user);
        request.getSession(false).setAttribute("userName",  user.getLastName() + " " + user.getFirstName());

        if (getLoggedInRole().equals("admin")) {
            getRedirectStrategy().sendRedirect(request, response, "/admin");
        }

        else {
            if (cookieOptional.isPresent()) {
                getRedirectStrategy().sendRedirect(request, response, cookieOptional.get().getValue());
            } else {
                super.onAuthenticationSuccess(request, response, authentication);
            }
        }
    }

    /**
     * Get the email from the logged in user
     * @return The email
     */
    private String getLoggedInEmail() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();

        if (principal instanceof UserDetails)
            return ((UserDetails) principal).getUsername();

        return principal.toString();
    }

    /**
     * Get the role from the logged in user
     * @return The role
     */
    private String getLoggedInRole() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();

        if (principal instanceof UserDetails)
            return ((UserDetails) principal).getAuthorities().toArray()[0].toString();

        return principal.toString();
    }

    /**
     * Get the user from the email
     * @param email The email to look for
     * @return The user object
     */
    private User getUser(String email) throws UsernameNotFoundException{
        User user = userRepository.findByEmail(email);
        if(user != null) {
            return user;
        } else {
            throw new UsernameNotFoundException("username not found");
        }
    }
}
