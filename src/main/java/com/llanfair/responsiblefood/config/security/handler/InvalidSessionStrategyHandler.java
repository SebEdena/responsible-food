/**
 * Invalid session handler class triggered after the detection of an invalid session
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.config.security.handler;

import com.llanfair.responsiblefood.config.cookie.CookieUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.session.InvalidSessionStrategy;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Component
public class InvalidSessionStrategyHandler implements InvalidSessionStrategy {

    @Autowired
    private CookieUtils cookieUtils;

    /**
     * The handler for invalid session
     * @param httpServletRequest The request object
     * @param httpServletResponse The response object
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void onInvalidSessionDetected(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, ServletException {
        Optional<Cookie> cookieOptional = cookieUtils.getCookieOptional(httpServletRequest, "anonymous");
        boolean isAnonymous = cookieOptional.isEmpty() || Boolean.parseBoolean(cookieOptional.get().getValue());
        String path;

        if(isAnonymous) {
            httpServletRequest.getSession(true);
            path = httpServletRequest.getRequestURI().substring(httpServletRequest.getContextPath().length());
        } else {
            path = "/auth/login?status=sessionExpired";
        }
        httpServletRequest.getRequestDispatcher(path).forward(httpServletRequest, httpServletResponse);
    }
}
