/**
 * Filter to handle the anonymous cookie
 * Used for redirecting after logout
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.config.filter;

import com.llanfair.responsiblefood.config.cookie.CookieUtils;
import com.llanfair.responsiblefood.config.security.AcceptedRequestChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(3)
public class AnonymousCheckFilter implements Filter {

    @Autowired
    private CookieUtils cookieUtils;

    @Autowired
    private AcceptedRequestChecker acceptedRequestChecker;

    /**
     * Performs the anonymous cookie handling in the filter chain of the server
     * @param servletRequest The associated request
     * @param servletResponse The associated response
     * @param filterChain The filter chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

        if(acceptedRequestChecker.isAcceptedRequest(httpServletRequest))
            cookieUtils.addCookie(httpServletResponse, "anonymous", ""+isAnonymous());

        filterChain.doFilter(servletRequest, httpServletResponse);
    }

    /**
     * Checks if the current user is anonymous (not authenticated)
     * @return True if the user is anonymous, false otherwise
     */
    private boolean isAnonymous() {
        return SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken;
    }

}
