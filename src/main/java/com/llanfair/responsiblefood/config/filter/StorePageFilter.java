/**
 * Filter to handle the previous page cookie
 * Used for the redirections after login / logout
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.config.filter;

import com.llanfair.responsiblefood.config.cookie.CookieUtils;
import com.llanfair.responsiblefood.config.security.AcceptedRequestChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(2)
public class StorePageFilter implements Filter {

    @Autowired
    private CookieUtils cookieUtils;

    @Autowired
    private AcceptedRequestChecker acceptedRequestChecker;

    /**
     * Performs the previous page cookie handling in the filter chain of the server
     * @param servletRequest The associated request
     * @param servletResponse The associated response
     * @param filterChain The filter chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

        Cookie cookie = cookieUtils.getCookieOptional(httpServletRequest, "RF_PREV_PAGE").orElse(null);

        if(acceptedRequestChecker.isAcceptedRequest(httpServletRequest)) {

            if(cookie == null) {
                cookieUtils.addCookie(httpServletResponse, "RF_PREV_PAGE", "/");
            } else {
                String path = httpServletRequest.getRequestURI().substring(httpServletRequest.getContextPath().length());
                cookieUtils.addCookie(httpServletResponse, "RF_PREV_PAGE", path);
            }
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
