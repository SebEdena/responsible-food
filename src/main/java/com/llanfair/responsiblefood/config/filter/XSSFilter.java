/**
 * Filter to wrap the incoming request into a XSS-Proof wrapper
 * To prevent Cross-Site Scripting
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 * @author Mykola Choban
 */

package com.llanfair.responsiblefood.config.filter;

import com.llanfair.responsiblefood.config.security.XSSRequestWrapper;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
@Order(1)
public class XSSFilter implements Filter {

    /**
     * Performs the XSS Wrapping in the filter chain of the server
     * @param request The associated request
     * @param response The associated response
     * @param filterChain The filter chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        filterChain.doFilter(new XSSRequestWrapper((HttpServletRequest) request), response);
    }

}
