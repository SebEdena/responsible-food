<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<head>
    <jsp:directive.include file = "fragments/head.jspf"></jsp:directive.include>
    <title>Mise à jour de l'offre</title>
    <link href="/css/myBoard.css" rel="stylesheet">
    <script type="text/javascript" src="/js/imageUpload.js"></script>
    <script type="text/javascript" src="/js/createOffer.js"></script>
    <script type="text/javascript">
        $(window).ready(() => {
            initImageUpload($('#form-update-offer'), false);
        })
    </script>
</head>

<body>
<div id="rf-wrapper">
    <jsp:directive.include file = "fragments/navigation.jspf"></jsp:directive.include>
    <div id="rf-content">
        <jsp:directive.include file = "fragments/producerNavBoard.jspf"></jsp:directive.include>
        <div class="myboard-container">
            <div id="divAction">
                <form:form class="ui form" id="form-update-offer" method="POST" modelAttribute="offerDTO" enctype="multipart/form-data" action="/myBoard/updateOffer?offerId=${fn:escapeXml(offerId)}">
                    <div class="ui two required fields">
                        <div class="ui field">
                            <form:label path="title" class="ui pointing below label">Le titre de votre offre</form:label>
                            <form:input path="title" class="ui input" autofocus="true" required="true" ></form:input>
                            <form:errors path="title" cssClass="field-error"></form:errors>
                        </div>
                        <div class="ui field">
                            <form:label path="tags" class="ui pointing below label">Les tags augmentent la visibilité de votre offre</form:label>
                            <form:select path="tags" id="tags" class="ui fluid search dropdown" multiple="true" >
                                <c:forEach items="${arrayListTags}" var="tag" >
                                    <form:option value="${fn:escapeXml(tag.getTag())}"><c:out value="${tag.getTag()}"/></form:option>
                                </c:forEach>
                            </form:select>
                            <form:errors path="tags" cssClass="field-error"></form:errors>
                        </div>
                    </div>
                    <div class="ui two fields">
                        <div class="ui thirteen wide field">
                            <div class="field field-textarea">
                                <form:label path="description" class="ui pointing below label">La description de votre offre</form:label>
                                <form:textarea path="description" class="ui stacked segment" rows="2"></form:textarea>
                            </div>
                            <div class="ui two required fields">
                                <div class="field" >
                                    <form:label path="expirationDate" class="ui pointing below label">Date d'expiration de votre offre</form:label>
                                    <div class="ui left icon input">
                                        <i class="hourglass end icon"></i>
                                        <form:input path="expirationDate" type="date" min="${tomorrowDate}" id="expirationDate" required="true"></form:input>
                                        <form:errors path="expirationDate" cssClass="field-error"></form:errors>
                                    </div>
                                </div>
                                <div class="field" >
                                    <form:label path="address" class="ui pointing below label">L'adresse de récupération de votre offre</form:label>
                                    <div class="ui input">
                                        <form:input path="address" required="true" ></form:input >
                                        <form:errors path="address" cssClass="field-error"></form:errors>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ui three wide field img-container">
                            <div class="md-img">
                                <img class="img-upload-preview" src="/images/offer/${fn:escapeXml(offerDTO.imageId)}">
                            </div>
                            <form:input type="file" path="image" class="ui input img-upload-input" accept="image/jpeg,image/png" style="display:none" ></form:input>
                            <form:errors path="image" cssClass="field-error"></form:errors>
                            <button class="ui fluid button img-upload-proxy-button">Modifier l'image</button>
                            <!--<input class="ui  img-upload-reset" value="Annuler" disabled>-->
                            <div style="color:red; display: none" class="img-upload-error">Erreur</div>
                        </div>
                    </div>
                    <div class="ui three required fields">
                        <div class="field">
                            <form:label path="unit" class="ui label">Quantité par unité</form:label>
                            <div class="ui action input">
                                <form:input path="quantityPerUnit" type="number" step="1" required="true" ></form:input>
                                <form:errors path="quantityPerUnit" cssClass="field-error"></form:errors>
                                <form:select path="unit" id="unit" class="ui dropdown" required="true">
                                    <c:forEach items="${arrayListUnit}" var="unit" >
                                        <form:option value="${fn:escapeXml(unit.getSymbol())}"><c:out value="${unit.getSymbol()}"/></form:option>
                                    </c:forEach>
                                </form:select>
                                <form:errors path="unit" cssClass="field-error"></form:errors>
                            </div>
                        </div>
                        <div class=" field">
                            <form:label path="total" class="ui pointing below label">Nombre d'unité disponible</form:label>
                            <div class="ui left icon input">
                                <form:input path="total" type="number" min="1" required="true"></form:input>
                                <form:errors path="total" cssClass="field-error"></form:errors>
                                <i class="shopping bag icon"></i>
                            </div>
                        </div>
                        <div class=" field">
                            <form:label path="pricePerUnit" class="ui pointing below  label">Prix par unité:</form:label>
                            <div class="ui right icon input">
                                <form:input path="pricePerUnit" type="number" min="0" step="any" required="true"></form:input>
                                <form:errors path="pricePerUnit" cssClass="field-error"></form:errors>
                                <i class="euro icon"></i>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="ui fluid large green submit button" value="Mettre à jour"/>
                </form:form>
            </div>
        </div>
    </div>
    <jsp:directive.include file = "fragments/footer.jspf"></jsp:directive.include>
</div>
</body>