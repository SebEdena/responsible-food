<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<head>
    <jsp:directive.include file = "fragments/head.jspf"></jsp:directive.include>
    <title>Mes Offres</title>
    <link href="/css/myBoard.css" rel="stylesheet">
    <script type="text/javascript" src="/js/myOffers.js"></script>
</head>

<body>
<div id="rf-wrapper">
    <jsp:directive.include file = "fragments/navigation.jspf"></jsp:directive.include>
    <div id="rf-content">
        <jsp:directive.include file = "fragments/producerNavBoard.jspf"></jsp:directive.include>
        <div class="myboard-container">
            <div id="divAction" class="ui centered link cards">
                <c:if test="${empty arrayListOffer}">
                    <div class="ui message">
                        <div class="header">
                            Vous n'avez pas encore créé d'offre!
                        </div>
                        <p>Vous pouvez créer une annonce via le bouton "créer une offre". </p>
                        <p>Si vous n'arrivez pas à en créer une, vous pouvez nous contacter via le lien "Contacter le support" en bas de page</p>
                    </div>
                </c:if>
                <c:forEach items="${arrayListOffer}" var="offer" >
                    <div class="ui green card" >
                        <div class="content">
                            <img class="ui right floated tiny image" src="/images/offer/${fn:escapeXml(offer.getImageId())}">
                            <div class="header">
                                <p><c:out value="${offer.getTitle()}"></c:out></p>
                            </div>
                            <div class="meta">
                                <p>
                                    L'offre : <c:out value="${offer.getQuantityPerUnit()}"></c:out> <c:out value="${offer.getUnit().getSymbol()}"></c:out><br>
                                    Prix : <fmt:formatNumber type="number" value="${fn:escapeXml(offer.getPricePerUnit())}" pattern=".00"></fmt:formatNumber> €<br>
                                    En réserve : <c:out value="${offer.getAvailable()}"></c:out><br>
                                    Tags : <c:out value="${offer.getTags().toString()}"></c:out>
                                </p>
                            </div>
                            <div class="description">
                                <p><c:out value="${offer.getDescription()}"></c:out></p>
                            </div>
                            <div class="meta">
                                <p style="<c:if test="${offer.expirationDate lt todayDate}">color:red;</c:if>">
                                    Date d'expiration : <fmt:formatDate pattern="dd/MM/yyyy" value="${offer.expirationDate}"/>
                                </p>
                            </div>
                        </div>
                        <div class="extra content">
                            <div class="ui two buttons">
                                <a href="/myBoard/updateOffer?offerId=${fn:escapeXml(offer.getId())}" class="ui basic orange button">Mettre à jour</a>
                                <a href="/myBoard/removeMyOffer?offerId=${fn:escapeXml(offer.getId())}" class="ui basic red button" id="del-offer-btn">Supprimer</a>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
    <jsp:directive.include file = "fragments/footer.jspf"></jsp:directive.include>
</div>
</body>