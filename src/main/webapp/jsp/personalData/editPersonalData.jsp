<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head>
    <jsp:directive.include file = "../fragments/head.jspf"></jsp:directive.include>
    <title>Mes données personnelles</title>
    <link href="/css/user.css" rel="stylesheet">
    <script type="text/javascript" src="/js/imageUpload.js"></script>
    <script src="/js/user.js"></script>
</head>
<body>
<div id="rf-wrapper">
    <jsp:directive.include file = "../fragments/navigation.jspf"></jsp:directive.include>
    <div id="rf-content">
        <jsp:directive.include file = "../fragments/basket/informationBasket.jspf"></jsp:directive.include>
        <div class="ui container rf-edit-data">
            <c:if test="${update == true}">
                <div class="ui success message">
                    <i class="close icon"></i>
                    <div class="header">
                        Votre profil a été mis à jour.
                    </div>
                </div>
            </c:if>
            <h1 class="ui green header">Mes informations</h1>
            <div class="ui segment">
                <form:form class="ui form" id="form-user-details" method="POST" modelAttribute="user" action="/user/update" enctype="multipart/form-data">
                    <h2 class="ui green header">
                        Données personnelles
                    </h2>
                    <div class="ui stackable grid">
                        <div class="row">
                            <div class="four wide column img-container">
                                <div class="md-img">
                                    <img class="img-upload-preview" src="/images/person/${fn:escapeXml(sessionScope.user.imageId)}">
                                </div>
                                <div>
                                    <form:input type="file" path="image" class="ui input img-upload-input" accept="image/jpeg,image/png" style="display:none" ></form:input>
                                    <button class="ui button img-upload-proxy-button">Modifier l'image</button>
                                </div>
                                <div>
                                    <div style="color:red; display: none" class="img-upload-error">Erreur</div>
                                    <form:errors path="image" cssClass="field-error"></form:errors>
                                </div>
                            </div>
                            <div class="twelve wide column">
                                <div class="two fields">
                                    <div class="field">
                                        <label>Prénom *</label>
                                        <form:input type="text" path="firstName" placeholder="Prénom *" required="required"></form:input>
                                        <form:errors path="firstName" cssClass="field-error"></form:errors>
                                    </div>
                                    <div class="field">
                                        <label>Nom *</label>
                                        <form:input type="text" path="lastName" placeholder="Nom *" required="required"></form:input>
                                        <form:errors path="lastName" cssClass="field-error"></form:errors>
                                    </div>
                                </div>
                                <sec:authorize access="isAuthenticated() and hasAnyRole('farmer', 'association', 'retailer')">
                                    <div class="field" id="company">
                                        <label>Entreprise ou Association</label>
                                        <form:input type="text" path="company" placeholder="Nom de l'entreprise ou association *"></form:input>
                                        <form:errors path="company" cssClass="field-error"></form:errors>
                                    </div>
                                </sec:authorize>
                                <div class="field">
                                    <label>Adresse *</label>
                                    <form:input type="text" path="address" placeholder="Adresse postale *" required="required"></form:input>
                                    <form:errors path="address" cssClass="field-error"></form:errors>
                                </div>
                                <div class="two fields">
                                    <div class="field">
                                        <label>Code Postal *</label>
                                        <form:input type="text" path="zipCode" placeholder="Code Postal *" required="required"></form:input>
                                    </div>
                                    <div class="field">
                                        <label>Ville *</label>
                                        <form:input type="text" path="city" placeholder="Ville *" required="required"></form:input>
                                    </div>
                                    <form:errors path="zipCode" cssClass="field-error"></form:errors>
                                    <form:errors path="city" cssClass="field-error"></form:errors>
                                </div>
                                <div class="field">
                                    <label>Téléphone *</label>
                                    <div class="ui right labeled icon input">
                                        <form:input cssClass="phone" type="tel" maxlength="14" path="phone" placeholder="Téléphone *" required="required"
                                                    pattern="0[0-9]( [0-9]{2}){4}"></form:input>
                                        <div class="ui icon button label" data-tooltip="Le numéro de téléphone doit respecter le format : 0X&nbsp;&nbsp;XX&nbsp;&nbsp;XX&nbsp;&nbsp;XX&nbsp;&nbsp;XX">
                                            <i class="question circle icon"></i>
                                        </div>
                                    <form:errors path="phone" cssClass="field-error"></form:errors>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="btn-right-group">
                                        <a href="/user/edit" class="ui red button">Annuler</a>
                                        <input type="submit" class="ui green submit button" value="Valider"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form:form>
            </div>
            <div class="ui segment">
                <form:form class="ui form" id="form-user-details" method="POST" modelAttribute="passwordChange" action="/user/changePassword">
                    <h2 class="ui green header">
                        Changer de mot de passe
                    </h2>
                    <div class="ui stackable grid">
                        <div class="row">
                            <div class="sixteen wide column">
                                <div class="field">
                                    <label>Mot de passe actuel *</label>
                                    <form:input type="password" path="currentPassword" placeholder="Mot de passe actuel *" required="required"></form:input>
                                </div>
                                <div class="field">
                                    <form:errors cssClass="field-error" path="currentPassword"></form:errors>
                                </div>
                                <div class="field">
                                    <label>
                                        Nouveau mot de passe *
                                    </label>
                                    <div class="ui right labeled icon input">
                                        <form:input type="password" path="newPassword" placeholder="Nouveau mot de passe *" required="required"></form:input>
                                        <div class="ui icon button label" data-tooltip="Le mot de passe doit contenir au moins 8 caractères, dont une majuscule et un caractère spécial.">
                                            <i class="question circle icon"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <form:errors path="newPassword" cssClass="field-error"></form:errors>
                                </div>
                                <div class="field">
                                    <label>Confirmer le mot de passe *</label>
                                    <form:input type="password" path="newPasswordConfirmation" placeholder="Confirmer le mot de passe *" required="required"></form:input>
                                    <form:errors path="newPasswordConfirmation" cssClass="field-error"></form:errors>
                                </div>
                                <div class="field">
                                    <form:errors path="newPasswordConfirmation" cssClass="field-error"></form:errors>
                                </div>
                                <div class="field">
                                    <form:errors path="" cssClass="field-error"></form:errors>
                                </div>
                                <div class="row">
                                    <div class="btn-right-group">
                                        <a href="/user/edit" class="ui red button">Annuler</a>
                                        <input type="submit" class="ui green submit button" value="Valider"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
    <jsp:directive.include file = "../fragments/footer.jspf"></jsp:directive.include>
</div>
</body>
