<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<head>
    <jsp:directive.include file = "../fragments/head.jspf"></jsp:directive.include>
    <title>Mes données personnelles</title>
    <link href="/css/user.css" rel="stylesheet">
</head>
<body>
    <div id="rf-wrapper">
        <jsp:directive.include file = "../fragments/navigation.jspf"></jsp:directive.include>
        <div id="rf-content">
            <jsp:directive.include file = "../fragments/basket/informationBasket.jspf"></jsp:directive.include>
            <div class="ui grid container">
                <c:if test="${editable == true}">
                    <div class="edit-btn">
                        <div>
                            <a href="/user/edit" class="ui button">Editer</a>
                        </div>
                    </div>
                </c:if>
                <div class="row centered">
                    <div class="profile-head">
                        <div class="md-img profile-image">
                            <img src="/images/person/${fn:escapeXml(details.imageId)}">
                        </div>
                        <div>
                            <h1><c:out value="${details.firstName} ${details.lastName}"></c:out></h1>
                        </div>
                        <c:if test="${not empty details.company}">
                            <div>
                                <h2><c:out value="${details.company}"></c:out></h2>
                            </div>
                        </c:if>
                        <div>
                            <c:choose>
                                <c:when test="${details.role.equals('farmer')}">
                                    <h3>Agriculteur</h3>
                                </c:when>
                                <c:when test="${details.role.equals('association')}">
                                    <h3>Association</h3>
                                </c:when>
                                <c:when test="${details.role.equals('retailer')}">
                                    <h3>Distributeur</h3>
                                </c:when>
                                <c:when test="${details.role.equals('client')}">
                                    <h3>Client</h3>
                                </c:when>
                            </c:choose>
                        </div>
                    </div>
                </div>
                <div class="ui divider"></div>
                <div class="sixteen wide centered row">
                    <h3>Adresse</h3>
                </div>
                <div class="centered row profile-data">
                    <table>
                        <tr>
                            <td><h4>Adresse</h4></td>
                            <td>
                                <c:out value="${details.address}"></c:out>
                            </td>
                        </tr>
                        <tr>
                            <td><h4>Code Postal</h4></td>
                            <td>
                                <c:out value="${details.zipCode}"></c:out>
                            </td>
                        </tr>
                        <tr>
                            <td><h4>Ville</h4></td>
                            <td>
                                <c:out value="${details.city}"></c:out>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="ui divider"></div>
                <div class="sixteen wide centered row">
                    <h3>Contact</h3>
                </div>
                <div class="centered row profile-data">
                    <table>
                        <tr>
                            <td><h4>Téléphone</h4></td>
                            <td>
                                <c:out value="${details.phone}"></c:out>
                            </td>
                        </tr>
                        <tr>
                            <td><h4>Email</h4></td>
                            <td>
                                <c:out value="${details.email}"></c:out>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <jsp:directive.include file = "../fragments/footer.jspf"></jsp:directive.include>
    </div>
</body>
