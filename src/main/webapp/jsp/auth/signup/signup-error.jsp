<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<jsp:directive.include file = "../../fragments/auth/cred_header_pre_title.jspf"></jsp:directive.include>
<title>Erreur</title>
<jsp:directive.include file = "../../fragments/auth/cred_header_post_title.jspf"></jsp:directive.include>
    <div class="ui segment fluid">
        <h2 class="ui green header">
            Une erreur est survenue dans l'activation du compte.
        </h2>
        <p><c:out value="${message}"></c:out></p>
        <c:choose>
            <c:when test="${token != null}">
                <p><a href="/auth/resend-token?token=${fn:escapeXml(token)}">Renvoyer un lien de confirmation</a></p>
            </c:when>
            <c:otherwise>
                <p><a href="/auth/resend-token">Renvoyer un lien de confirmation</a></p>
            </c:otherwise>
        </c:choose>
        <a href="/">Accueil</a>
    </div>
<jsp:directive.include file = "../../fragments/auth/cred_footer.jspf"></jsp:directive.include>