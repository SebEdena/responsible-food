<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:directive.include file = "../../fragments/auth/cred_header_pre_title.jspf"></jsp:directive.include>
<title>Lien de validation</title>
<jsp:directive.include file = "../../fragments/auth/cred_header_post_title.jspf"></jsp:directive.include>
    <div class="ui segment fluid">
        <h2 class="ui green header">
            Ré-envoyer un lien de validation
        </h2>
        <form:form cssClass="ui form" modelAttribute="email" method="post" action="/auth/resend-token-email">
            <div class="ui field fluid">
                <form:input path="email" cssClass="ui input fluid" placeholder="Email"></form:input>
                <form:errors path="email" cssClass="field-error"></form:errors>
            </div>
            <input type="submit" class="ui green submit button" value="Valider"/>
        </form:form>
        <a href="/">Accueil</a> - <a href="/auth/login">Connexion</a>
    </div>
<jsp:directive.include file = "../../fragments/auth/cred_footer.jspf"></jsp:directive.include>