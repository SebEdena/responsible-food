<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<jsp:directive.include file = "../../fragments/auth/cred_header_pre_title.jspf"></jsp:directive.include>
<title>Inscription</title>
<jsp:directive.include file = "../../fragments/auth/cred_header_post_title.jspf"></jsp:directive.include>
<form:form class="ui form" method="POST" modelAttribute="user" action="/auth/signup">
    <div class="ui segment fluid">
        <h2 class="ui green header">
            Formulaire d'inscription
        </h2>
        <div class="inline fields">
            <div class="ui large label">Vous êtes : *</div>
            <form:select class="ui dropdown grey" id="rule" onchange="ruleChange()" path="role">
                <form:option value="farmer">Agriculteur</form:option>
                <form:option value="client">Client</form:option>
                <form:option value="association">Association</form:option>
                <form:option value="retailer">Distributeur</form:option>
            </form:select>
        </div>
        <div class="field" id="company">
            <div class="ui left icon input">
                <i class="industry icon"></i>
                <form:input type="text" path="company" placeholder="Nom de l'entreprise ou association *"></form:input>
            </div>
            <form:errors path="company" cssClass="field-error"></form:errors>
        </div>
        <div class="two fields">
            <div class="field">
                <div class="ui left icon input">
                    <i class="user icon"></i>
                    <form:input type="text" path="firstName" placeholder="Prénom *" required="required"></form:input>
                </div>
            </div>
            <div class="field">
                <div class="ui left icon input">
                    <i class="user icon"></i>
                    <form:input type="text" path="lastName" placeholder="Nom *" required="required"></form:input>
                </div>
            </div>
            <form:errors path="firstName" cssClass="field-error"></form:errors>
            <form:errors path="lastName" cssClass="field-error"></form:errors>
        </div>
        <div class="field">
            <div class="ui left icon input">
                <i class="address card icon"></i>
                <form:input type="text" path="address" placeholder="Adresse postale *" required="required"></form:input>
            </div>
            <form:errors path="address" cssClass="field-error"></form:errors>
        </div>
        <div class="two fields">
            <div class="field">
                <div class="ui left icon input">
                    <i class="envelope icon"></i>
                    <form:input type="text" path="zipCode" placeholder="Code Postal *" required="required"></form:input>
                </div>
            </div>
            <div class="field">
                <div class="ui left icon input">
                    <i class="envelope icon"></i>
                    <form:input type="text" path="city" placeholder="Ville *" required="required"></form:input>
                </div>
            </div>
            <form:errors path="zipCode" cssClass="field-error"></form:errors>
            <form:errors path="city" cssClass="field-error"></form:errors>
        </div>
        <div class="field">
            <div class="ui right labeled left icon input">
                <i class="phone icon"></i>
                <form:input cssClass="phone" type="tel" maxlength="14" path="phone" placeholder="Téléphone *" required="required"
                pattern="0[0-9]( [0-9]{2}){4}"></form:input>
                <div class="ui icon button label" data-tooltip="Le numéro de téléphone doit respecter le format : 0X&nbsp;&nbsp;XX&nbsp;&nbsp;XX&nbsp;&nbsp;XX&nbsp;&nbsp;XX">
                    <i class="question circle icon"></i>
                </div>
            </div>
            <form:errors path="phone" cssClass="field-error"></form:errors>
        </div>
        <div class="field">
            <div class="ui left icon input">
                <i class="at icon"></i>
                <form:input type="text" path="email" placeholder="Adresse mail *" required="required"></form:input>
            </div>
            <form:errors path="email" cssClass="field-error"></form:errors>
        </div>
        <div class="field">
            <div class="ui right labeled left icon input">
                <i class="lock icon"></i>
                <form:input type="password" path="password" placeholder="Mot de passe *" required="required"></form:input>
                <div class="ui icon button label" data-tooltip="Le mot de passe doit contenir au moins 8 caractères, dont une majuscule et un caractère spécial.">
                    <i class="question circle icon"></i>
                </div>
            </div>
            <form:errors path="password" cssClass="field-error"></form:errors>
        </div>
        <div class="field">
            <div class="ui left icon input">
                <i class="lock icon"></i>
                <form:input type="password" path="passwordConfirmation" placeholder="Confirmez votre mot de passe *" required="required"></form:input>
            </div>
            <form:errors path="passwordConfirmation" cssClass="field-error"></form:errors>
            <form:errors cssClass="field-error"></form:errors>
        </div>
        <div class="field">
            <div class="ui checkbox">
                <form:checkbox path="cguAccepted" required="required"></form:checkbox>
                <label>En créant mon compte, j'ai lu et j'approuve les <a onclick="displayCGU()">CGU</a> du site</label>
            </div>
            <form:errors path="cguAccepted" cssClass="field-error"></form:errors>
        </div>
        <input type="submit" class="ui fluid large green submit button" value="Valider"/>
    </div>
</form:form>
<div class="ui bottom small blue message">Les champs requis sont notés par *</div>
<div class="ui bottom small olive message">Déjà inscrit ? Connectez-vous <a href="/auth/login">ici</a>.</div>
<jsp:directive.include file = "../../fragments/auth/cred_footer.jspf"></jsp:directive.include>