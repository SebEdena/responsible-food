<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:directive.include file = "../../fragments/auth/cred_header_pre_title.jspf"></jsp:directive.include>
<title>Compte créé</title>
<jsp:directive.include file = "../../fragments/auth/cred_header_post_title.jspf"></jsp:directive.include>
    <div class="ui segment fluid">
        <c:choose>
            <c:when test="${resend != null}">
                <h2 class="ui green header">
                    Un lien de confirmation a été renvoyé à l'adresse <c:out value="${email}"></c:out>
                </h2>
            </c:when>
            <c:otherwise>
                <h2 class="ui green header">
                    Compte créé pour <c:out value="${email}"></c:out>
                </h2>
            </c:otherwise>
        </c:choose>
        <p>Relevez votre boite mail pour confirmer votre inscription.</p>
        <a href="/">Accueil</a> - <a href="/auth/login">Connexion</a>
    </div>
<jsp:directive.include file = "../../fragments/auth/cred_footer.jspf"></jsp:directive.include>