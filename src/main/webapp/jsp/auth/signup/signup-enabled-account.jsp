<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:directive.include file = "../../fragments/auth/cred_header_pre_title.jspf"></jsp:directive.include>
<title>Compte activé</title>
<jsp:directive.include file = "../../fragments/auth/cred_header_post_title.jspf"></jsp:directive.include>
    <div class="ui segment fluid">
        <h2 class="ui green header">
            Compte activé !
        </h2>
        <a href="/">Accueil</a> - <a href="/auth/login">Connexion</a>
    </div>
<jsp:directive.include file = "../../fragments/auth/cred_footer.jspf"></jsp:directive.include>