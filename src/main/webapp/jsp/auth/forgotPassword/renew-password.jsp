<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<jsp:directive.include file = "../../fragments/auth/cred_header_pre_title.jspf"></jsp:directive.include>
<title>Réinitialisation de mot de passe</title>
<jsp:directive.include file = "../../fragments/auth/cred_header_post_title.jspf"></jsp:directive.include>
<div class="ui segment fluid">
    <h2 class="ui green header">
        <c:choose>
            <c:when test="${message != null}">
                Les mots de passes sont différents, veuillez resaisir votre mot de passe
            </c:when>
            <c:otherwise>
                Token validé !
            </c:otherwise>
        </c:choose>
    </h2>
    <label>Veuillez saisir votre nouveau mot de passe</label>
    <form:form class="ui form" action="/auth/renew-password" method='POST' modelAttribute="passwordForgottenDTO">
        <input type="hidden" name="token" value="${fn:escapeXml(token)}">
        <div class="field">
            <div class="ui right labeled left icon input">
                <i class="lock icon"></i>
                <form:input type="password" path="password" name="password" placeholder="Mot de passe" required="required"></form:input>
                <div class="ui icon button label" data-tooltip="Le mot de passe doit contenir au moins 8 caractères, dont une majuscule et un caractère spécial.">
                    <i class="question circle icon"></i>
                </div>
            </div>
            <form:errors path="password" cssClass="field-error"></form:errors>
        </div>
        <div class="field">
            <div class="ui left icon input">
                <i class="lock icon"></i>
                <form:input type="password" path="passwordConfirmation" placeholder="Confirmez votre mot de passe *" required="required"></form:input>
            </div>

            <form:errors path="passwordConfirmation" cssClass="field-error"></form:errors>
            <form:errors cssClass="field-error"></form:errors>
        </div>
        <input type="submit" class="ui fluid small green submit button" value="Valider"/>
    </form:form>
    <a href="/">Accueil</a> - <a href="/auth/login">Connexion</a>
</div>
<jsp:directive.include file = "../../fragments/auth/cred_footer.jspf"></jsp:directive.include>