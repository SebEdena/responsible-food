<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<jsp:directive.include file = "../fragments/auth/cred_header_pre_title.jspf"></jsp:directive.include>
<title>Connexion</title>
<jsp:directive.include file = "../fragments/auth/cred_header_post_title.jspf"></jsp:directive.include>
    <form class="ui form" action="/auth/login" method='POST'>
        <div class="ui segment fluid">
        <h2 class="ui green header">
            Formulaire de connexion
        </h2>
        <div class="field">
            <div class="ui left icon input">
                <i class="user icon"></i>
                <input type="text" name="username" placeholder="Adresse mail">
            </div>
        </div>
        <div class="field">
            <div class="ui left icon input">
                <i class="lock icon"></i>
                <input type="password" name="password" placeholder="Mot de passe">
            </div>
        </div>
        <input type="submit" class="ui fluid large green submit button" value="Valider"/>
        </div>
    </form>
<c:choose>
    <c:when test="${param['status'] == \"loginError\"}">
        <div class="ui error red message">
            <c:set var = "springError" value = "${sessionScope.SPRING_SECURITY_LAST_EXCEPTION}"/>
            <c:out value="${springError}"/>
        </div>
    </c:when>
    <c:when test="${param['status'] == \"sessionExpired\"}">
        <div class="ui error orange message">
            La session a expiré. Veuillez vous reconnecter
        </div>
    </c:when>
</c:choose>
    <div class="ui bottom small yellow message">Mot de passe oublié ? <a onclick="modalForgottenPwd()">Cliquez ici</a>.</div>
    <div class="ui bottom small olive message">Pas encore de compte ? Inscrivez-vous <a href="/auth/signup">ici</a>.</div>

<div class="ui modal" id="modalForgottenPwd">
        <div class="header">
            <h1 class="column">Mot de passe oublié ? </h1>
        </div>
        <div class="content">
            <label>Veuillez saisir votre adresse mail </label>
            <form class="ui form" action="/auth/forgotPassword" method='POST'>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="at icon"></i>
                        <input type="text" name="mailForgotten" placeholder="Adresse mail">
                    </div>
                </div>
                <input type="submit" class="ui fluid small green submit button" value="Valider"/>
            </form>
        </div>
        <i class="close icon"></i>
    </div>
<jsp:directive.include file = "../fragments/auth/cred_footer.jspf"></jsp:directive.include>