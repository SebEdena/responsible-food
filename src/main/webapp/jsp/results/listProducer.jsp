<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<html>
    <head>
        <title>Nos producteurs</title>
        <jsp:directive.include file = "../fragments/head.jspf"></jsp:directive.include>
        <link href="/css/prod.css" rel="stylesheet">
        <link href="/css/search.css" rel="stylesheet">
        <script type="text/javascript" src="/js/search.js"></script>
    </head>
    <body>
        <div id="rf-wrapper">
            <jsp:directive.include file = "../fragments/navigation.jspf"></jsp:directive.include>
            <div id="rf-content">
                <jsp:directive.include file = "../fragments/basket/informationBasket.jspf"></jsp:directive.include>

                <div class="ui divider hidden"></div>

                <jsp:directive.include file = "../fragments/searchBarProd.jspf"></jsp:directive.include>

                <div class="ui divider hidden"></div>

                <c:choose>
                <c:when test="${producers.size() == 0}">
                    <div class="search_text">
                        Oups... Nous n'avons pas de producteurs correspondant à : <b>${research.toLowerCase()}</b>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="search_text">
                        Nous avons <b><c:out value="${producers.size()}"/></b> résultat(s) pour : <b>${research.toLowerCase()}</b>
                    </div>

                    <div class="ui divider hidden"></div>

                    <div class="ui container">
                        <div class="ui divided list">
                            <c:forEach items="${producers}" var="p">
                                <div class="item">
                                    <div class="prod-container">
                                        <div class="md-img">
                                            <img src="/images/person/${fn:escapeXml(p.imageId)}">
                                        </div>
                                        <div class="prod-info-container ui grid">
                                            <div class="prod-header sixteen wide column">
                                                <h2 class="ui header">
                                                    <a href="/user/details/${fn:escapeXml(p.id)}">
                                                        <c:out value="${p.firstName} ${p.lastName}"></c:out>
                                                    </a>
                                                </h2>
                                                <h3 class="ui header"><c:out value="${p.company}"></c:out></h3>
                                            </div>
                                            <div class="prod-infos-contact-container row">
                                                <div class="eight wide column prod-infos">
                                                    <h4 class="ui header">Adresse :</h4>
                                                    <div class="largeContent"><c:out value="${p.address}"></c:out></div>
                                                    <div class="content"><c:out value="${p.zipCode} ${p.city}"></c:out></div>
                                                </div>
                                                <div class="eight wide column prod-infos">
                                                    <h4 class="ui header">Contact :</h4>
                                                    <div class="content"><c:out value="${p.phone}"></c:out></div>
                                                    <div class="largeContent"><c:out value="${p.email}"></c:out></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <a class="lienVers ui button green" href="/user/details/${fn:escapeXml(p.id)}">Profil</a>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="ui divider hidden"></div>
                </c:otherwise>
            </c:choose>
            </div>
            <jsp:directive.include file = "../fragments/footer.jspf"></jsp:directive.include>
        </div>
    </body>
</html>