<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<html>
<head>
    <title>Résultats</title>
    <jsp:directive.include file = "../fragments/head.jspf"></jsp:directive.include>
    <link href="/css/offer.css" rel="stylesheet">
    <link href="/css/search.css" rel="stylesheet">
    <script type="text/javascript" src="/js/search.js"></script>
</head>
<body>
<div id="rf-wrapper">
    <jsp:directive.include file = "../fragments/navigation.jspf"></jsp:directive.include>
    <div id="rf-content">

        <div class="ui divider hidden"></div>

        <!-- Navigation Bar !-->
        <jsp:directive.include file = "../fragments/searchBar.jspf"></jsp:directive.include>

        <c:choose>
            <c:when test="${offers.size() == 0}">
                <div class="search_categories">
                    Oups... Nous n'avons pas d'offres correspondant à : <b>${research.toLowerCase()}</b>
                </div>
            </c:when>
            <c:otherwise>
                <div class="search_categories">
                    Nous avons <b><c:out value="${offers.size()}"/></b> résultat(s) pour : <b>${research.toLowerCase()}</b>
                </div>

                <div class="ui divider hidden"></div>

                <!-- List of the ten more recent offers !-->
                <div class="offers-container">
                    <c:forEach items="${offers}" var="offer" varStatus="loop">
                        <a class="lien offer-margin" href="/offers/<c:out value="${offer.id}"></c:out>">
                            <div class="offer-container">
                                <div class="sm-img offer-img">
                                    <img src="/images/offer/<c:out value="${offer.imageId}"></c:out>">
                                </div>
                                <div class="offer-text-context">
                                    <h3><c:out value="${offer.title}"></c:out></h3>
                                    <c:choose>
                                        <c:when test="${offer.description != ''}">
                                            <c:choose>
                                                <c:when test="${offer.description.length() > 80}">
                                                    <div><b>"</b><c:out value="${offer.description.substring(0, 77)}..."></c:out><b>"</b></div>
                                                </c:when>
                                                <c:otherwise>
                                                    <div><b>"</b><c:out value="${offer.description}"></c:out><b>"</b></div>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:when>
                                        <c:otherwise>
                                            <div><div class="ui divider hidden"></div></div>
                                        </c:otherwise>
                                    </c:choose>
                                    <div><b>Stock : </b><c:out value="${offer.available}"></c:out></div>
                                    <div><b>Quantité par stock : </b>
                                        <c:out value="${offer.quantityPerUnit}"></c:out> <c:out value="${offer.unit == null ? '': fn:escapeXml(offer.unit.symbol)}"></c:out>
                                    </div>
                                    <div><b>Prix : </b>
                                        <fmt:formatNumber type="number" value="${fn:escapeXml(offer.getPricePerUnit())}" pattern=".00"></fmt:formatNumber> €
                                    </div>
                                    <div><b>Expire le : </b>
                                        <fmt:formatDate pattern="dd/MM/yyyy" value="${offer.expirationDate}"/>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </c:forEach>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
    <jsp:directive.include file = "../fragments/footer.jspf"></jsp:directive.include>
</div>
</body>
</html>