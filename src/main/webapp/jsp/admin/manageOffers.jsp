<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>Welcome</title>
    <jsp:directive.include file = "../fragments/head.jspf"></jsp:directive.include>
    <link href="/css/admin/adminBoard.css" rel="stylesheet">
    <script type="text/javascript" src="/js/admin/removeOfferAccount.js"></script>
</head>
<body>
<div id="rf-wrapper">
    <jsp:directive.include file = "../fragments/admin/navigation.jspf"></jsp:directive.include>
    <div id="rf-content">

        <jsp:directive.include file = "../fragments/admin/adminNavBoard.jspf"></jsp:directive.include>

        <div id="divAction" class="ui centered link cards">
            <c:if test="${empty arrayListOffer}">
                <div class="ui message">
                    <div class="header">
                        Il n'y a pas encore d'offres !
                    </div>
                    <p>Patientez un peu que des utilisateurs créent des offres.</p>
                    <p>Si ce problème persiste, vous pouvez nous contacter via le lien "Contacter le support" en bas de page</p>
                </div>
            </c:if>

            <div class="ui divided items">
                <c:forEach items="${arrayListOffer}" var="offer" >
                    <div class="item">
                        <div class="image">
                            <img src="/images/offer/${fn:escapeXml(offer.imageId)}">
                        </div>
                        <div class="content">
                            <a class="header"><c:out value="${offer.getTitle()}"></c:out></a>
                            <div class="meta">
                                <span class="cinema">ID : <c:out value="${offer.getId()}"></c:out></span>
                            </div>
                            <div class="description">
                                <p>
                                    Auteur : <c:out value="${offer.getAuthor().getFirstName()} ${offer.getAuthor().getLastName()}"></c:out> <br>
                                    Entreprise : <c:out value="${offer.getAuthor().getCompany()}"></c:out> <br>

                                    Date : <fmt:formatDate pattern="dd/MM/yyyy" value="${offer.timeStamp}"/> <br>
                                    Date expiration : <fmt:formatDate pattern="dd/MM/yyyy" value="${offer.expirationDate}"/> <br>
                                    Description : <c:out value="${offer.getDescription()}"></c:out> <br>
                                    Adresse : <c:out value="${offer.getAddress()}"></c:out> <br>
                                    Prix : <c:out value="${offer.getPricePerUnit()} €/${offer.getUnit().getSymbol()}"></c:out>

                                    <!--
                                     Total : <br>
                                     Available <br>
                                     Price per Unit : <br>
                                     quantityPerUnit : <br>
                                     UNit :<br>
                                     -->

                                </p>
                            </div>
                            <div class="extra">
                                <div class="row">
                                    <c:forEach items="${offer.getTags()}" var="tags" >
                                        <div class="ui label"><c:out value="${tags.toString()}"></c:out></div>
                                    </c:forEach>
                                </div>
                                <div class="row">
                                    <a href="/board-admin/manageOffers/removeOffer?offerId=${fn:escapeXml(offer.id)}" class="ui red right floated button" id="remove-offer">Supprimer
                                        <i class="right chevron icon"></i>
                                    </a>
                                    <a href="/board-admin/manageOffers" class="ui yellow right floated button">Détail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
    <jsp:directive.include file = "../fragments/footer.jspf"></jsp:directive.include>
</div>
</body>
</html>