<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Welcome</title>
    <jsp:directive.include file = "../fragments/head.jspf"></jsp:directive.include>
    <link href="/css/admin/adminBoard.css" rel="stylesheet">
</head>
<body>
<div id="rf-wrapper">
    <jsp:directive.include file = "../fragments/admin/navigation.jspf"></jsp:directive.include>
    <div id="rf-content">
        <div>
            <sec:authorize access="isAuthenticated()">
                <c:set var="rolesLogged" value="${sessionScope.roles}"/>
                <h1>Bienvenue dans l'administration du site ResponsibleFood</h1>

                <div class="ui divider hidden"></div>

                <div class="ui grid">
                    <div class="eight wide computer sixteen wide column">
                        <div class="divHomeAdmin">
                            <h3>Répartition des comptes</h3>

                            <c:set var="total" value="${0}"/>
                            <c:forEach var="user" items="${mapUser}">
                                <c:set var="total" value="${total + user.value}" />
                            </c:forEach>

                            <div class="ui grid">
                                <div class="sixteen wide column center aligned">
                                    <div class="ui small statistic">
                                        <div class="value">
                                            <p><c:out value="${total}"></c:out></p>
                                        </div>
                                        <div class="label">Comptes ResponsibleFood</div>
                                    </div>
                                </div>
                            </div>

                            <div class="ui grid">
                                <div class="eight wide column center aligned">
                                    <div class="ui tiny horizontal statistic">
                                        <div class="value">
                                            <p><c:out value="${mapUser['client']}"></c:out></p>
                                        </div>
                                        <div class="label">Clients</div>
                                    </div>
                                </div>

                                <div class="eight wide column center aligned">
                                    <div class="ui tiny horizontal statistic">
                                        <div class="value">
                                            <p><c:out value="${mapUser['farmer']}"></c:out></p>
                                        </div>
                                        <div class="label">Agriculteurs</div>
                                    </div>
                                </div>
                            </div>

                            <div class="ui grid">
                                <div class="eight wide column center aligned">
                                    <div class="ui tiny horizontal statistic">
                                        <div class="value">
                                            <p><c:out value="${mapUser['association']}"></c:out></p>
                                        </div>
                                        <div class="label">Associations</div>
                                    </div>
                                </div>

                                <div class="eight wide column center aligned">
                                    <div class="ui tiny horizontal statistic">
                                        <div class="value">
                                            <p><c:out value="${mapUser['retailer']}"></c:out></p>
                                        </div>
                                        <div class="label">Distributeurs</div>
                                    </div>
                                </div>
                            </div>

                            <div class="ui grid">
                                <div class="eight wide column center aligned">
                                    <div class="ui tiny horizontal statistic">
                                        <div class="value">
                                            <p><c:out value="${mapUser['admin']}"></c:out></p>
                                        </div>
                                        <div class="label">Administrateurs</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="eight wide computer sixteen wide column">
                        <div class="divHomeAdmin">
                            <h3>Carte de la france</h3>
                            <p>(Prochainement)</p>
                        </div>
                    </div>

                    <div class="eight wide computer sixteen wide column">
                        <div class="divHomeAdmin">
                            <h3>Création d'un compte administrateur</h3>
                            <p>Vous avez besoin d'un collègue pour administrer le site ? (Prochainement)</p>
                            <p>Ajoutez un nouvel admin !</p>

                            <a class="item">
                                <button class="ui green button">Ajouter un administrateur</button>
                            </a>

                        </div>
                    </div>

                    <div class="eight wide computer sixteen wide column">
                        <div class="divHomeAdmin">
                            <h3>Diverses Métriques</h3>

                            <div class="ui grid">
                                <div class="sixteen wide column">
                                    <div class="ui tiny horizontal statistic">
                                        <div class="value">
                                            <p><c:out value="${nbOffer}"></c:out></p>
                                        </div>
                                        <div class="label">Offres</div>
                                    </div>
                                </div>
                            </div>

                            <div class="ui grid">
                                <div class="eigth wide column">
                                    <div class="ui tiny horizontal statistic">
                                        <div class="value">
                                            <p><c:out value="${nbTag}"></c:out></p>
                                        </div>
                                        <div class="label">Tags</div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

            </sec:authorize>
            <sec:authorize access="!isAuthenticated()">
                <h1>Bonjour et bienvenue sur ResponsibleFood !</h1>
                <p>Vous pouvez vous inscrire ou vous connecter. </p>
            </sec:authorize>
        </div>

    </div>
    <jsp:directive.include file = "../fragments/footer.jspf"></jsp:directive.include>
</div>
</body>
</html>