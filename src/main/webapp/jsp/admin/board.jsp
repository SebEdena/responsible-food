<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Welcome</title>
    <jsp:directive.include file = "../fragments/head.jspf"></jsp:directive.include>
    <link href="/css/admin/adminBoard.css" rel="stylesheet">
</head>
<body>
<div id="rf-wrapper">
    <jsp:directive.include file = "../fragments/admin/navigation.jspf"></jsp:directive.include>
    <div id="rf-content">
        <jsp:directive.include file = "../fragments/admin/adminNavBoard.jspf"></jsp:directive.include>
    </div>
    <jsp:directive.include file = "../fragments/footer.jspf"></jsp:directive.include>
</div>
</body>
</html>