<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>Welcome</title>
    <jsp:directive.include file = "../fragments/head.jspf"></jsp:directive.include>
    <link href="/css/admin/adminBoard.css" rel="stylesheet">
    <script type="text/javascript" src="/js/admin/removeOfferAccount.js"></script>
</head>
<body>
<div id="rf-wrapper">
    <jsp:directive.include file = "../fragments/admin/navigation.jspf"></jsp:directive.include>
    <div id="rf-content">

        <jsp:directive.include file = "../fragments/admin/adminNavBoard.jspf"></jsp:directive.include>

        <div id="divAction" class="ui centered link cards">
            <c:if test="${empty arrayListUser}">
                <div class="ui message">
                    <div class="header">
                        Il n'y a pas encore de comptes !
                    </div>
                    <p>Patientez un peu les premiers comptes arriveront dans les prochains jours.</p>
                    <p>Si ce problème persiste, vous pouvez nous contacter via le lien "Contacter le support" en bas de page</p>
                </div>
            </c:if>

            <div class="ui divided items">
                <c:forEach items="${arrayListUser}" var="user" >

                    <div class="item">
                        <div class="image">
                            <img src="/images/person/${fn:escapeXml(user.imageId)}">
                        </div>
                        <div class="content">
                            <a class="header"><c:out value="${user.getFirstName()} ${user.getLastName()}"></c:out></a>
                            <div class="meta">
                                <span class="cinema">ID : <c:out value="${user.getId()}"></c:out></span>
                            </div>
                            <div class="description">
                                <p>
                                    Email : <c:out value="${user.getEmail()}"></c:out> <br>
                                    Téléphone : <c:out value="${user.getPhone()}"></c:out> <br>
                                    Entreprise : <c:out value="${user.getCompany()}"></c:out> <br>
                                    Adresse : <c:out value="${user.getAddress()},  ${user.getZipCode()}  ${user.getCity()}"></c:out> <br>
                                </p>
                            </div>
                            <div class="extra">
                                <div class="row">
                                    <div class="ui label">
                                        <c:forEach items="${user.getRoles()}" var="roles" >
                                            <c:out value="${roles.getRole()}"></c:out>
                                        </c:forEach>
                                    </div>
                                </div>
                                <div class="row">
                                    <a href="/board-admin/manageAccounts/removeAccount?AccountId=${fn:escapeXml(user.id)}" class="ui red right floated button" id="remove-account">Supprimer
                                        <i class="right chevron icon"></i>
                                    </a>
                                    <a href="/board-admin/manageAccounts" class="ui yellow right floated button">Détail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
    <jsp:directive.include file = "../fragments/footer.jspf"></jsp:directive.include>
</div>
</body>
</html>