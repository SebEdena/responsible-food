<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<html>
    <head>
        <title>Détail de l'offre</title>
        <jsp:directive.include file = "fragments/head.jspf"></jsp:directive.include>
        <link href="/css/offer.css" rel="stylesheet">
        <link href="/css/search.css" rel="stylesheet">
    </head>
    <body>
        <div id="rf-wrapper">
            <jsp:directive.include file = "fragments/navigation.jspf"></jsp:directive.include>
            <div id="rf-content">

                <jsp:directive.include file = "fragments/basket/informationBasket.jspf"></jsp:directive.include>

                <div class="ui divider hidden"></div>

                <div class="offer-special">
                    <div class="offer-detailed-container">
                        <div class="offer-detailed-left">
                            <div class=""><h1><c:out value="${offer.title}"></c:out></h1></div>
                            <h4>Annonce émise le : <fmt:formatDate pattern="dd/MM/yyyy" value="${offer.timeStamp}"/></h4>
                            <div class="">Expire le : <fmt:formatDate pattern="dd/MM/yyyy" value="${offer.expirationDate}"/></div>
                            <img class="img-detailed-offer" src="/images/offer/${fn:escapeXml(offer.imageId)}">
                        </div>
                        <div class="offer-detailed-right">
                            <div class="info-prod">
                                <h2 class="ui header">
                                    <a href="/user/details/${fn:escapeXml(offer.author.id)}">
                                        <c:out value="${offer.author.firstName} ${offer.author.lastName}"></c:out></a></h2>
                                <h3 class="ui header"><c:out value="${offer.author.company}"></c:out></h3>
                            </div>
                            <div class=""><b>Adresse : </b> <c:out value="${offer.address}"></c:out></div>
                            <div class=""><b>Stock : </b> <c:out value="${offer.available}"></c:out></div>
                            <div class=""><b>Quantité par stock : </b> <c:out value="${offer.quantityPerUnit} ${offer.unit == null ? '':offer.unit.symbol}"></c:out></div>
                            <div class=""><b>Prix unitaire : </b><fmt:formatNumber type="number" value="${fn:escapeXml(offer.getPricePerUnit())}" pattern=".00"></fmt:formatNumber> €</div>
                            <sec:authorize access="hasAnyRole('client')">
                                <div class="cart-info">
                                    <form class="ui form" action="/shop/${fn:escapeXml(offer.id)}" method='GET'>
                                        <div class="add-to-cart-container">
                                            <select id="qte" name="qte">
                                                <option value="1" selected>1</option>
                                                <c:forEach var="i" step="1" begin="2" end="${fn:escapeXml(offer.available)}">
                                                    <option value="${i}">${i}</option>
                                                </c:forEach>
                                            </select>
                                            <button class="ui fluid green submit button" type="submit">
                                                Ajouter au panier !
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </sec:authorize>
                            <sec:authorize access="!hasAnyRole('client')">
                                <div class="ui message">
                                    <p>Le panier est réservé pour les clients connectés</p>
                                    <p>Connectez-vous en cliquant : <a href="/auth/login">ici</a></p>
                                    <p>Pas de compte, inscrivez-vous en cliquant : <a href="/auth/signup">ici</a> </p>
                                </div>
                            </sec:authorize>
                        </div>
                    </div>
                    <br>
                    <div class="offer-detailed-container">
                        <c:if test="${offer.description != ''}">
                            <div class="">Le petit mot du producteur : <b><i>"${offer.description}"</i></b></div>
                        </c:if>
                    </div>
                </div>
            </div>
            <jsp:directive.include file = "fragments/footer.jspf"></jsp:directive.include>
        </div>
    </body>
    <script src="/js/search.js"></script>
</html>