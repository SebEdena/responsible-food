<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Validation de l'offre</title>
    <jsp:directive.include file = "../fragments/head.jspf"></jsp:directive.include>
    <link href="../../css/basket.css" rel="stylesheet">
</head>
<body>
<div id="rf-wrapper">
    <jsp:directive.include file = "../fragments/navigation.jspf"></jsp:directive.include>
    <div id="rf-content">
        <div class="ui success message">
            <div class="header">
                Votre panier a été réservé !
            </div>
        </div>
        <a href="/">Revenir à la page d'accueil</a>
    </div>
    <jsp:directive.include file = "../fragments/footer.jspf"></jsp:directive.include>
</div>
</body>
</html>