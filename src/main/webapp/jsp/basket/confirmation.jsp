<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Confirmation de l'offre</title>
    <jsp:directive.include file = "../fragments/head.jspf"></jsp:directive.include>
    <link href="../../css/basket.css" rel="stylesheet">
</head>
<body>
<div id="rf-wrapper">
    <jsp:directive.include file = "../fragments/navigation.jspf"></jsp:directive.include>
    <div id="rf-content">
        <div class="ui container">
            <jsp:directive.include file = "../fragments/basket/informationBasket.jspf"></jsp:directive.include>
            <c:choose>
                <c:when test="${sessionScope.basket != null && sessionScope.basket.size() > 0}">
                    <h1 class="ui header">Voici les détails de votre réservation</h1>
                    <table class="ui celled padded table">
                        <thead>
                        <tr>
                            <th>Image</th>
                            <th>Nom de l'annonce</th>
                            <th>Quantité</th>
                            <th>Prix à l'unité</th>
                            <th>Sous-Total</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:set var="total" value="0" scope="page"/>
                        <c:forEach items="${sessionScope.basket}" var="basketItem" varStatus="loop">
                            <tr>
                                <td class="img-td"><div class="tiny-img"><img src="/images/offer/${fn:escapeXml(basketItem.offer.imageId)}" alt=""></div></td>
                                <td><a href="/offer/${basketItem.offer.id}">${basketItem.offer.title}</a></td>
                                <td>${basketItem.quantity}</td>
                                <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${basketItem.offer.pricePerUnit}" /> €</td>
                                <c:set var="total" value="${total + basketItem.quantity * basketItem.offer.pricePerUnit}" scope="page"/>
                                <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${basketItem.quantity * basketItem.offer.pricePerUnit}" /> €</td>
                                <td><a class="ui red button"  href="/shop/delete/${basketItem.offer.id}" onclick="return confirm('Êtes-vous sûr de vouloir supprimer le produit du panier ?')">Supprimer</a></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="4">
                                <a class="ui green button" href="/shop/validation">Valider</a>
                                <a class="ui red button" href="/shop/deleteAll" onclick="return confirm('Êtes-vous sûr de vouloir supprimer tous les produits du panier ?')">Tout annuler</a>
                            </th>
                            <th colspan="2">
                                <div class="ui small disabled button" id="basket_total">
                                    Total : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${total}" /> €
                                </div>
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </c:when>
                <c:otherwise>
                    <h1 class="ui header">Vous n'avez pas d'offre dans votre panier !</h1>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
    <jsp:directive.include file = "../fragments/footer.jspf"></jsp:directive.include>
</div>
</body>
</html>