<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<html>
<head>
    <title>Welcome</title>
    <jsp:directive.include file = "fragments/head.jspf"></jsp:directive.include>
    <link href="/css/offer.css" rel="stylesheet">
    <link href="/css/search.css" rel="stylesheet">
    <script type="text/javascript" src="/js/search.js"></script>
</head>
<body>
    <div id="rf-wrapper">
        <jsp:directive.include file = "fragments/navigation.jspf"></jsp:directive.include>
        <div id="rf-content">
            <jsp:directive.include file = "fragments/basket/informationBasket.jspf"></jsp:directive.include>
            <div class="banner">
                <sec:authorize access="isAuthenticated()">
                    <c:set var="rolesLogged" value="${sessionScope.roles}"/>
                    <h1>Bonjour <c:out value="${name}"></c:out> !</h1>
                </sec:authorize>
                <sec:authorize access="!isAuthenticated()">
                    <h1>Bonjour et bienvenue sur ResponsibleFood !</h1>
                </sec:authorize>
            </div>

            <div class="ui hidden divider"></div>

            <!-- Navigation Bar !-->
            <jsp:directive.include file = "fragments/searchBar.jspf"></jsp:directive.include>

            <div class="ui hidden divider"></div>

            <!-- Shortcut for research !-->
            <div class="search_categories">
                <p>En manque d'inspiration ? Essayez nos raccourcis !</p>
                <div class="researchContainerImg">
                    <div class="researchShortcut">
                        <a class="category-container" href="/search/family/Légumes">
                            <div class="offer-category">
                                <img src="/img/legumes.jpg" class="figureShortcut img-category">
                                <div class="img-overlay"><div class="text-centered">Légumes</div></div>
                            </div>
                        </a>
                        <a class="category-container" href="/search/family/Fruits">
                            <div class="offer-category">
                                <img src="/img/fruits.jpg" class="figureShortcut img-category">
                                <div class="img-overlay"><div class="text-centered">Fruits</div></div>
                            </div>
                        </a>
                        <a class="category-container" href="/search/family/Viandes">
                            <div class="offer-category">
                                <img src="/img/viandes.png" class="figureShortcut img-category">
                                <div class="img-overlay"><div class="text-centered">Viandes</div></div>
                            </div>
                        </a>
                        <a class="category-container" href="/search/family/Poissons">
                            <div class="offer-category">
                                <img src="/img/poissons.jpg" class="figureShortcut img-category">
                                <div class="img-overlay"><div class="text-centered">Poissons</div></div>
                            </div>
                        </a>
                        <a class="category-container" href="/search/family/Laitages">
                            <div class="offer-category">
                                <img src="/img/laitage.jpg" class="figureShortcut img-category">
                                <div class="img-overlay"><div class="text-centered">Laitages</div></div>
                            </div>
                        </a>
                        <a class="category-container" href="/search/family/Oeufs">
                            <div class="offer-category">
                                <img src="/img/oeuf.jpg" class="figureShortcut img-category">
                                <div class="img-overlay"><div class="text-centered">Oeufs</div></div>
                            </div>
                        </a>
                        <a class="category-container" href="/search/family/Fruits de mer">
                            <div class="offer-category">
                                <img src="/img/fruit_mer.jpg" class="figureShortcut img-category">
                                <div class="img-overlay"><div class="text-centered">Fruits de mer</div></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="ui hidden divider"></div>

            <!-- List of the ten more recent offers !-->
            <div class="offer-content">
                <h1>Nos dernières offres</h1>
                <div class="offers-container limit-size">
                    <c:forEach items="${offers}" var="offer" varStatus="loop">
                        <a class="lien offer-margin" href="/offers/<c:out value="${offer.id}"></c:out>">
                            <div class="offer-container">
                                <div class="sm-img offer-img">
                                    <img src="/images/offer/<c:out value="${offer.imageId}"></c:out>">
                                </div>
                                <div class="offer-text-context">
                                    <h3><c:out value="${offer.title}"></c:out></h3>
                                    <c:choose>
                                        <c:when test="${offer.description != ''}">
                                            <c:choose>
                                                <c:when test="${offer.description.length() > 80}">
                                                    <div><b>"</b><c:out value="${offer.description.substring(0, 77)}..."></c:out><b>"</b></div>
                                                </c:when>
                                                <c:otherwise>
                                                    <div><b>"</b><c:out value="${offer.description}"></c:out><b>"</b></div>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:when>
                                        <c:otherwise>
                                            <div><br></div>
                                        </c:otherwise>
                                    </c:choose>
                                    <div><b>Stock : </b><c:out value="${offer.available}"></c:out></div>
                                    <div><b>Quantité par stock : </b>
                                        <c:out value="${offer.quantityPerUnit}"></c:out> <c:out value="${offer.unit == null ? '': fn:escapeXml(offer.unit.symbol)}"></c:out>
                                    </div>
                                    <div><b>Prix : </b>
                                        <fmt:formatNumber type="number" value="${fn:escapeXml(offer.getPricePerUnit())}" pattern=".00"></fmt:formatNumber> €
                                    </div>
                                    <div><b>Expire le : </b>
                                        <fmt:formatDate pattern="dd/MM/yyyy" value="${offer.expirationDate}"/>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </c:forEach>
                </div>
            </div>
        </div>
        <jsp:directive.include file = "fragments/footer.jspf"></jsp:directive.include>
    </div>
</body>
</html>