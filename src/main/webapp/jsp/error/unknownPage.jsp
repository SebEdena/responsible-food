<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <jsp:directive.include file = "../fragments/head.jspf"></jsp:directive.include>
        <title>Page non trouvée</title>
    </head>
    <body>
        <div id="rf-wrapper">
            <jsp:directive.include file = "../fragments/navigation.jspf"></jsp:directive.include>
            <div id="rf-content">
                <div class="ui red message">
                    <h1>La page que vous recherchiez n'existe pas</h1>
                    <h3>URL : <c:out value="${url}"></c:out></h3>
                    <a href="/">Revenir à la page d'accueil</a>
                </div>
            </div>
            <jsp:directive.include file = "../fragments/footer.jspf"></jsp:directive.include>
        </div>
    </body>
</html>