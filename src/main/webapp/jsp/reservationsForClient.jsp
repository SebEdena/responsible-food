<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<head>
    <jsp:directive.include file = "fragments/head.jspf"></jsp:directive.include>
    <title>Réservations</title>
    <link href="/css/myBoard.css" rel="stylesheet">
</head>

<body>
<div id="rf-wrapper">
    <jsp:directive.include file = "fragments/navigation.jspf"></jsp:directive.include>
    <div id="rf-content">
        <div id="divAction" class="ui centered link cards">
            <c:if test="${empty arrayListTransaction}">
                <div class="ui message">
                    <div class="header">
                        Vous n'avez actuellement aucune reservation
                    </div>
                </div>
            </c:if>
            <c:forEach items="${arrayListTransaction}" var="transaction" >
                <div class="ui green card" >
                    <div class="content">
                        <img class="ui right floated tiny image" src="/images/offer/${fn:escapeXml(offer.getImageId())}">
                        <div class="header">
                            <p>${fn:escapeXml(transaction.buyer.getFirstName())} ${fn:escapeXml(transaction.buyer.getLastName())}
                            </p>
                        </div>
                        <div class="meta">
                            <p>
                                L'offre : ${fn:escapeXml(transaction.offer.title)} <br>
                                Prix : <fmt:formatNumber type="number" value="${transaction.offer.pricePerUnit*transaction.quantity}" pattern=".00"></fmt:formatNumber> €<br>
                                Quantité voulue: ${fn:escapeXml(transaction.quantity*transaction.offer.quantityPerUnit)} ${fn:escapeXml(transaction.offer.unit.symbol)}
                                <br>
                            </p>
                        </div>
                        <div class="description">
                            <p><c:out value="${offer.getDescription()}"></c:out></p>
                        </div>
                        <div class="meta">
                            <p>Réserver le : <fmt:formatDate pattern="dd/MM/yyyy" value="${transaction.date}"/></p>
                        </div>
                    </div>
                    <c:if test="${transaction.validated eq false}">
                        <div class="ui orange message" style="margin: 6px !important;">
                            <div class="header">
                                N'a pas encore été validé
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${transaction.validated eq true}">
                        <div class="ui green message" style="margin: 6px !important;">
                            <div class="header">
                                Validée! Vous pouvez désormais contacter le producteur via son adresse mail :
                                ${transaction.offer.author.email}
                            </div>
                        </div>
                    </c:if>
                </div>
            </c:forEach>
        </div>
    </div>
    <jsp:directive.include file = "fragments/footer.jspf"></jsp:directive.include>
</div>
</body>