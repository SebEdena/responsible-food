function checkBeforeDelete(btn) {
    let choice = confirm("Êtes-vous sûr de vouloir supprimer cette annonce ?");
    console.log($(btn).attr('href'))
    if(choice) {
        window.location.href = $(btn).attr('href');
    }
}

$(document).on('click', '#del-offer-btn', function (e) {
    e.preventDefault();
    checkBeforeDelete(e.target);
});