function checkBeforeDeleteOffer(btn) {
    let choice = confirm("Êtes-vous sûr de vouloir supprimer cette offre ?");
    console.log($(btn).attr('href'))
    if(choice){
        window.location.href = $(btn).attr('href');
    }
}

function checkBeforeDeleteAccount(btn) {
    let choice = confirm("Êtes-vous sûr de vouloir supprimer ce compte ?");
    console.log($(btn).attr('href'))
    if(choice){
        window.location.href = $(btn).attr('href');
    }
}

$(document).on('click', '#remove-offer', function (e) {
    e.preventDefault();
    checkBeforeDeleteOffer(e.target);
});

$(document).on('click', '#remove-account', function (e) {
    e.preventDefault();
    checkBeforeDeleteAccount(e.target);
});