$(() => {
    initImageUpload($('#form-user-details'), false);

    $('.message .close')
        .on('click', function() {
            $(this)
                .closest('.message')
                .transition('fade');
        });
})