function displayCGU() {
    $.ajax({
        url : "http://localhost:8080/cgu",
        type : "GET",
        success: function(data){
            $("#textCGU").html(data.message)
            $("#modalCGU").modal('show');
        },
        error: function(error){
            console.log(error)
            $("#textCGU").html(error.response)
            $("#modalCGU").modal('show');
        }
    });
}