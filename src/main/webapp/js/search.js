$(() => {
    $('#rshTags').dropdown().hide()
    $('.tags').css("display","none")
    $('.search-bar-selector').dropdown()
    $('#searchBtn').popup()

    $('#type').change(function() {
        if ($(this).children('option:selected').val() === "tags"){
            $('#rshTags').dropdown().show()
            $('#rshTags').prop("required",true)
            $('#rshText').css("display", "none")
            $('#rshText').removeAttr("required")
            $('.tags').css("display","block")
        } else {
            $('#rshTags').dropdown().hide()
            $('#rshText').prop("required",true)
            $('#rshTags').removeAttr("required")
            $('.tags').css("display","none")
            $('#rshText').css("display", "block")
        }
    })
})