$(() => {
    $('.ui.sticky').sticky();

    $('#dropdown_user').dropdown();
    $('#dropdown_basket').dropdown();
    $('#pages-menu').dropdown();
    $('#account-menu').dropdown();
});