function initImageUpload(form, overrideSubmit) {
    let url;
    let acceptedFormats = ['image/jpeg', 'image/png'];
    let maxImageSize = 1000000; //1MB
    let currentSrc;

    function toggleImageButtons(form, enable) {
        $(form).find('.img-upload-submit').first().prop("disabled", !enable);
        $(form).find('.img-upload-reset').first().prop("disabled", !enable);
    }

    function openFileDialog(event) {
        event.preventDefault();
        $(form).find('.img-upload-input').first().trigger("click");
    }

    function changeImagePreview(form, src) {
        $(form).find('.img-upload-preview').first().prop("src", src);
    }

    function updateDefaultImage(form) {
        currentSrc = $(form).find('.img-upload-preview').first().prop("src");
        $(form).find('.img-upload-input').val(null);
    }

    function setImageErrorMessage(form, message) {
        if(message == null || message === "") {
            $(form).find(".img-upload-error").first().text("");
            $(form).find(".img-upload-error").first().css("display", "none");
        } else {
            $(form).find(".img-upload-error").first().text(message);
            $(form).find(".img-upload-error").first().css("display", "block");
            $(form).find('.img-upload-input').val(null);
        }
    }

    function resetImage(form) {
        toggleImageButtons(form, false);
        changeImagePreview(form, currentSrc);
        $(form).find('.img-upload-input').val(null);
    }

    function submitImage(event) {
        if (overrideSubmit) {
            event.preventDefault();
            toggleImageButtons(event.target, false);

            let val = $(event.target).find('.img-upload-input').first().prop('files')[0];
            let data = new FormData();
            data.append("image", val);

            $.post({
                url: url,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                enctype: 'multipart/form-data',
                success: response => {
                    updateDefaultImage(event.target);
                },
                error: response => { setImageErrorMessage(form, response.data) }
            })
        }
    }

    function changeImageInput(event) {
        if(event.target.files.length > 0) {
            let file = event.target.files[0];
            if(!acceptedFormats.includes(file.type)) {
                toggleImageButtons(form, false);
                setImageErrorMessage(form, "Le fichier doit être au format JPEG ou PNG.")
                $(event.target())
            } else if(file.size >= maxImageSize) {
                toggleImageButtons(form, false);
                setImageErrorMessage(form, "Le fichier doit être plus petit que 1Mo");
            } else {
                setImageErrorMessage(form, null);
                let reader = new FileReader();
                reader.onload = () => {
                    let image = new Image();
                    image.src = reader.result.toString();
                    setImageErrorMessage(form, null);
                    toggleImageButtons(form, true);
                    changeImagePreview(form, image.src);
                }
                reader.readAsDataURL(file);
            }
        } else {
            resetImage($(form));
        }
    }

    currentSrc = $(form).find('.img-upload-preview').first().prop("src");
    url = $(form).prop("action");
    $(form).on('reset', event => resetImage(form));
    $(form).on('submit', event => submitImage(event));
    $(form).find('.img-upload-input').first().on('change', event => changeImageInput(event));
    $(form).find('.img-upload-proxy-button').first().on('click', event => openFileDialog(event));
    if(!overrideSubmit) {
        $(form).find('.img-upload-reset').first().on('click', event => { event.preventDefault(); resetImage($(form)) });
    }
}