function displayFAQ() {
    $.ajax({
        url : "http://localhost:8080/faq",
        type : "GET",
        success: function(data){
            $("#question1").html(data.question1)
            $("#reponse1").html(data.reponse1)
            $("#question2").html(data.question2)
            $("#reponse2").html(data.reponse2)
            $("#modalFAQ").modal('show');
        },
        error: function(error){
            console.log(error)
            $("#question1").html(error.response)
            $("#modaFAQ").modal('show');
        }
    });
}