window.onload = function() {
    $('.phone').mask("09 99 99 99 99", {
        translation: {
            '0': {pattern: /0/},
            '9': {pattern: /\d/},
        }
    })
    ruleChange();
};

function ruleChange(){
    if($("#rule").val() === "client"){
        $("#company").css('display', "none");
        $("#company input").prop('required',false);
    }else{
        $("#company").css('display', "block");
        $("#company input").prop('required',true);
    }
}

function modalForgottenPwd(){
    $("#modalForgottenPwd").modal("show");
}

